#pragma once

#include "core"
#include "layers"
#include "events"
#include "input"
#include <glad/glad.h>

class SandboxLayer : public MxnLayer
{
public:
	SandboxLayer() : MxnLayer(
		#ifdef BLD_DEBUG
		"Sandbox"
		#endif
	)
	{
		// glGenVertexArrays(1, &_vertexArray);
		// glBindVertexArray(_vertexArray);

		// glGenBuffers(1, &_vertexBuffer);
		// glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);

		// float verticies[3 * 3] = {
		// 	0.3f,  0.0f, 0.0f,
		// 	0.9f, -0.7f, 0.0f,
		// 	0.9f,  0.33f, 0.0f,
		// };

		// glBufferData(GL_ARRAY_BUFFER, sizeof(verticies), verticies, GL_STATIC_DRAW);

		// glEnableVertexAttribArray(0);
		// glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);

		// glGenBuffers(1, &_indexBuffer);
		// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);

		// unsigned int indicies[3] = { 0, 1, 2 };
		// glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);
	}

	void OnUpdate() override
	{
		// glBindVertexArray(_vertexArray);
		// glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	}

	void OnEvent(MxnEvent& e) override
	{
		
	}

private:
	unsigned int _vertexArray, _indexBuffer, _vertexBuffer;
};
