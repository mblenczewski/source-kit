#include "pch.h"
#include "app"
#include "wing_link"
#include "debug"
#include "bridge"

#include "layer.h"
#include <GLFW/glfw3.h>
#include <glad/glad.h>

void Startup()
{
	Log::Init();
	CTWBridgeDock();
	// glfwMakeContextCurrent((GLFWwindow*)App::GetWindow().GetNativeWindow());
	// int status = gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	// ASSERT(status, "Failed to initialise GLAD");

	// App::GetLayerStack().PushLayer(new SandboxLayer());

	LG_INF("Hello from the Sandbox");
}

void Shutdown()
{
	LG_INF("Bye bye from the Sandbox");
}
