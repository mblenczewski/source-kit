#pragma once

#include "core"
#include "layers"
#include "events"
#include "input"
#include <imgui.h>
#include <glad/glad.h>


class PlaygroundLayer : public MxnLayer
{
public:
	PlaygroundLayer() : MxnLayer(
		#ifdef BLD_DEBUG
		"Playground"
		#endif
	) 
	{
		// glGenVertexArrays(1, &_vertexArray);
		// glBindVertexArray(_vertexArray);

		// glGenBuffers(1, &_vertexBuffer);
		// glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);

		// float verticies[3 * 3] = {
		// 	-0.9f,  0.6f, 0.0f,
		// 	-0.9f, -0.2f, 0.0f,
		// 	-0.3f,  0.0f, 0.0f
		// };

		// glBufferData(GL_ARRAY_BUFFER, sizeof(verticies), verticies, GL_STATIC_DRAW);

		// glEnableVertexAttribArray(0);
		// glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);

		// glGenBuffers(1, &_indexBuffer);
		// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);

		// unsigned int indicies[3] = { 0, 1, 2 };
		// glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);
	}

	void OnUpdate() override
	{
		// glBindVertexArray(_vertexArray);
		// glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	}

	void OnImGUIRender() override
	{
		// ImGui::Begin("Playground");
		// ImGui::Text("hello");
		// ImGui::End();
	}

	void OnEvent(MxnEvent& e) override
	{

	}

private:
	unsigned int _vertexArray, _indexBuffer, _vertexBuffer;
};
