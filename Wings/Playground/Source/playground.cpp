#include "pch.h"
#include "app"
#include "wing_link"
#include "debug"
#include "bridge"

#include "layer.h"
#include <GLFW/glfw3.h>
#include <glad/glad.h>

void Startup()
{
	Log::Init();
	LG_INF("Hello from the Playground");
	CTWBridgeDock();
	
	
	// glfwMakeContextCurrent((GLFWwindow*)App::GetWindow().GetNativeWindow());
	// int status = gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	// ASSERT(status, "Failed to initialise GLAD");
	
	// App::GetLayerStack().PushLayer(new PlaygroundLayer());
}

void Shutdown()
{
	LG_INF("Bye bye from the Playground");
}
