#include <imgui.h>

#include "app"
#include "core"
#include "debug"
#include "input"
#include "gameloop"

// #include "Graphics/Data/shader.h"
// #include "Graphics/Data/buffers.h"
// #include "Graphics/Data/vertex_array.h"
// #include "Graphics/Data/texture.h"

#include "Debug/imgui_layer.h"
#include "Assets/a.h"

#include "Graphics/Renderer/renderer.h"
#include "Graphics/Renderer/renderer2d.h"

#include "Components/transform.h"
#include "Components/camera.h"
#include "Components/cam_controller.h"
#include "Debug/profiling.h"

// own<OrthographicCameraController> g_controller;

static bool OnWindowClose(EvtWindowClose e)
{
	App::SetRunning(false);
	return true;
}

static bool OnWindowResize(EvtWindowResize& e)
{
	if (e.GetSize().x == 0 || e.GetSize().y == 0)
	{
		App::SetMinimised(true);
		return false;
	}

	App::SetMinimised(false);
	Renderer::SetWindowSize(e.GetSize().x, e.GetSize().y);
	return false;
}

static void OnEvent(MxnEvent& e)
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<EvtWindowClose>(BIND_EVT_FN(OnWindowClose));
	dispatcher.Dispatch<EvtWindowResize>(BIND_EVT_FN(OnWindowResize));

	// g_controller->OnEvent(e);
	for (auto it = App::GetLayerStack().end(); it != App::GetLayerStack().begin(); )
	{
		(*--it)->OnEvent(e);
		if (e.handled)
			break;
	}
}

void Startup();
void Runtime();
void Shutdown();

int main(void)
{
	PRF_SESSION_BEGIN("Startup", "00Output/profile_startup.json");
	Startup();
	PRF_SESSION_END;

	PRF_SESSION_BEGIN("Runtime", "00Output/profile_runtime.json");
	Runtime();
	PRF_SESSION_END;

	PRF_SESSION_BEGIN("Shutdown", "00Output/profile_shutdown.json");
	Shutdown();
	PRF_SESSION_END;
	
	return 1;
}

void Startup()
{
	PRF_FN;
	LG_INIT;

	LG_INF("Welcome to the Source Kit!");

	// CTWBridgeDock();

	// Init functions from CTWBridgeDock, should move these to an inlined function
	App::Init();
	MngInput::Init();
	Time::Init();

	App::GetWindow().SetEventCallback(BIND_EVT_FN(OnEvent));
	
	Renderer::Init();
	RenderAPI::SetClearColour({ 0.1f, 0.05f, 0.15f, 1 });

	App::SetRunning(true);
}

static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

void Runtime()
{
	PRF_FN;

	while (App::IsRunning())
	{
		PRF_SC("Run Loop");
		Time::Update();

		RenderAPI::Clear();

		if (!App::IsMinimised())
		{
			PRF_SC("Layers Updater");
			for (auto layer : App::GetLayerStack())
				layer->OnUpdate();
		}

		// To go  to render thread
		{
			PRF_SC("ImGUI Update");
			App::GetImGUILayer().Begin();
			for (auto layer : App::GetLayerStack())
				layer->OnImGUIRender();

			static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

    // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
    // because it would be confusing to have two docking targets within each others.
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    // if (opt_fullscreen)
    // {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->WorkPos);
        ImGui::SetNextWindowSize(viewport->WorkSize);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    // }
    // else
    // {
    //     dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
    // }

    // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
    // and handle the pass-thru hole, so we ask Begin() to not render a background.
    if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
        window_flags |= ImGuiWindowFlags_NoBackground;

    // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
    // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
    // all active windows docked into it will lose their parent and become undocked.
    // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
    // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
    // if (!opt_padding)
    //     ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

		static bool open = true;
    ImGui::Begin("DockSpace Demo", &open, window_flags);
    // if (!opt_padding)
    //     ImGui::PopStyleVar();

    // if (opt_fullscreen)
        ImGui::PopStyleVar(2);

    // Submit the DockSpace
    ImGuiIO& io = ImGui::GetIO();
    if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
    {
        ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
    }

    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("Options"))
        {
            // Disabling fullscreen would allow the window to be moved to the front of other windows,
            // which we can't undo at the moment without finer window depth/z control.

            // if (ImGui::MenuItem("Flag: NoSplit",                "", (dockspace_flags & ImGuiDockNodeFlags_NoSplit) != 0))                 { dockspace_flags ^= ImGuiDockNodeFlags_NoSplit; }
            // if (ImGui::MenuItem("Flag: NoResize",               "", (dockspace_flags & ImGuiDockNodeFlags_NoResize) != 0))                { dockspace_flags ^= ImGuiDockNodeFlags_NoResize; }
            // if (ImGui::MenuItem("Flag: NoDockingInCentralNode", "", (dockspace_flags & ImGuiDockNodeFlags_NoDockingInCentralNode) != 0))  { dockspace_flags ^= ImGuiDockNodeFlags_NoDockingInCentralNode; }
            // if (ImGui::MenuItem("Flag: AutoHideTabBar",         "", (dockspace_flags & ImGuiDockNodeFlags_AutoHideTabBar) != 0))          { dockspace_flags ^= ImGuiDockNodeFlags_AutoHideTabBar; }
            // if (ImGui::MenuItem("Flag: PassthruCentralNode",    "", (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode) != 0, opt_fullscreen)) { dockspace_flags ^= ImGuiDockNodeFlags_PassthruCentralNode; }
            ImGui::Separator();

            if (ImGui::MenuItem("Close", NULL, false, true))
							App::SetRunning(false);
            //     *p_open = false;
            ImGui::EndMenu();
        }
        HelpMarker(
            "When docking is enabled, you can ALWAYS dock MOST window into another! Try it now!" "\n"
            "- Drag from window title bar or their tab to dock/undock." "\n"
            "- Drag from window menu button (upper-left button) to undock an entire node (all windows)." "\n"
            "- Hold SHIFT to disable docking." "\n"
            "This demo app has nothing to do with it!" "\n\n"
            "This demo app only demonstrate the use of ImGui::DockSpace() which allows you to manually create a docking node _within_ another window." "\n\n"
            "Read comments in ShowExampleAppDockSpace() for more details.");

        ImGui::EndMenuBar();
    }

    ImGui::End();
			
			ImGui::Begin("Debug");
			// ImGui::ColorEdit4("Flat Colour", &pFlat1.colour.r);
			ImGui::End();
			App::GetImGUILayer().End();

		}

		{
			PRF_SC("Window Update");
			App::GetWindow().OnUpdate();
		}

		// if (IsPressed(MngInput::GetKeyState(KY_ESC)))
		// 	App::SetRunning(false);
	}
}

void Shutdown()
{
	PRF_FN;
	Renderer::Shutdown();

	LG_SHUTDOWN;

	App::Shutdown();
	std::cout << "Fin\n";
}
