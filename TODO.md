# TODO

<del>Create editor project</del>  
<del>Rename OpenGL files and classes to have the ogl prefix</del>  
<del>Remove workbench</del>  
<del>Replace with standalone build scripts</del>  

multiprocessed compilation
cleaner code
Progressive success chime to be played at each success stage of a multi 
stage compilation, e.g. compiling Core for Wings

**Compile Times (Core):   No PCH: 165, PCH: 134**  
**Compile Times (Editor): No PCH: 160, PCH: 160**  
Rework macros: for project name, for the Core and Editor roots  

Create more loggers of differing detail  
Switch to linux to test compatability
Start to use layers more  
Change references/name of Wing to Module  
Use namespaces in different sections of the code  
Move to favour simple structs over classes  
Start custom math library that can be converted easily to glm and DearImGui types +
typedefed replacements for unsigned int, int, etc (i32, u32)  
Seperate the data of the vertex array and buffer from functionality, 
so that the modules don't need to compile glad  
Framebuffers + Scene  

*Later*
Create render queue, it performs the OpenGL operations on the data provided, and gives the data any persistent data
C++ code analysis for serialisation

System to know which files need to be recompiled and which don't
Figure out PCH
- no needless compilation (mainly the pch)
- The PCH only works when pch.h.gch is in the _Includes directory, want it to work from 00PreCompiled
- pch.h needs to be compiled with the same compiler options as the cpp files.
