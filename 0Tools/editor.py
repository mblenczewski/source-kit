import os

import arguments
import shell
import config
import externals as ext
import util

def compile_editor():
	proj_conf = config.read_config_file('project.conf')
	extl_conf = config.read_config_file('2Externals/externals.conf')
	
	profile = config.combine_profiles(proj_conf['def_profile'], proj_conf['profiles'])
	includes = '-I' + ' -I'.join(profile['includes']) + ' '
	libs = '-l' + ' -l'.join(profile['libs']) + ' '
	macros = '-D' + ' -D'.join(profile['macros']) + ' '
	options = ' '.join(profile['options'])

	for name, data in extl_conf.items():
		includes += ext.get_and_format_includes(
			ext.get_externals_folder(name),
			data
			)
		macros += ext.get_and_format_macros(
			ext.get_externals_folder(name),
			data
			)
		
		libs += ext.get_and_format_libs(data)

	if os.path.exists(config.DIR_PC_LIBS):
		l = \
			[ f.replace('lib', '').replace('.a', '')\
			for f in os.listdir(config.DIR_PC_LIBS)\
			if os.path.isfile(f"{config.DIR_PC_LIBS}/{f}") ]
		libs += '-l' + ' -l'.join(l)
	
	libs_temp = []
	for lib in libs.split(' '):
		if not lib in libs_temp:
			libs_temp.append(lib)
	
	libs_temp.reverse()
	libs = ' '.join(libs_temp)

	cpp_files = []
	for root, dir, files in os.walk(config.DIR_CORE_SRC):
		if root == config.DIR_CORE_SRC:
			continue
		
		for f in files:
			if f.endswith('.cpp'):
				cpp_files.append((root + '/' + f).replace('\\', '/'))
	
	for root, dir, files in os.walk(config.DIR_EDITOR_SRC):
		for f in files:
			if f.endswith('.cpp'):
				cpp_files.append((root + '/' + f).replace('\\', '/'))

	exe_dest = config.DIR_BUILDS
	if not os.path.exists(exe_dest):
		os.makedirs(exe_dest)
	
	print("Compiling Editor")
	if arguments.ARG_CONCURRENT:
		cwd = os.getcwd().replace('\\', '/') + '/'

		obj_dir = config.DIR_PC_OBJS + '/Editor'
		if not os.path.exists(obj_dir):
			os.makedirs(obj_dir)
		
		files = []
		for cpp in cpp_files:
			out = cwd + obj_dir +'/' + cpp.replace('/', '+').replace('.cpp', '.o')
			inp = cwd + cpp
			files.append({ 'inp': inp, 'out': out })
		
		play_audio_on_cpp = shell.IF_FAIL if arguments.ARG_AUDIO != shell.NEVER else shell.NEVER
		
		timer = util.Timer()
		if arguments.ARG_TIMING:
			timer.start()
		
		returncode = shell.multi_execute(
			f"{profile['exe_compiler']} -std={profile['c_std']} -o {{out}} " +
			f"-c {{inp}} {includes} {macros} -DBUILD_EDITOR {options}", files,
			arguments.ARG_PRINT, False, play_audio_on_cpp
		)

		if returncode != 0:
			print("Failure")
			exit()
		
		print("Building executable")
		shell.execute(
			f"{profile['exe_compiler']} -o {exe_dest}/editor.exe " +
			f"{obj_dir}/*.o -L{config.DIR_PC_LIBS} {libs}",
			arguments.ARG_PRINT, False, arguments.ARG_AUDIO
		)

		if arguments.ARG_TIMING:
			timer.stop()
			timer.print()
	else:
		shell.execute(
			f"{profile['exe_compiler']} -std={profile['c_std']} -o {exe_dest}/editor.exe " +
			f"{' '.join(cpp_files)} {includes} -L{config.DIR_PC_LIBS} {libs} {macros} " +
			f"-DBUILD_EDITOR {options}",
			arguments.ARG_PRINT, arguments.ARG_TIMING, arguments.ARG_AUDIO
		)
	print("Finish")

if __name__ == '__main__':
	compile_editor()
