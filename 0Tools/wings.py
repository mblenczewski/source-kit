import os

import arguments
import shell
import config
import util
import externals as ext

def precomp_core(profile: dict, includes: str, macros: str, print_commands: bool, timing: bool, audio: int):
	cpp_files = []
	dest_folder = f"{config.DIR_PC_OBJS}/Wings/Core"
	if not os.path.exists(dest_folder):
		os.makedirs(dest_folder)

	for root, dir, files in os.walk(config.DIR_CORE_SRC):
		if root == config.DIR_CORE_SRC:
			continue
		
		for f in files:
			if f.endswith('.cpp'):
				cpp_files.append((root + '/' + f).replace('\\', '/'))
	
	audio = shell.IF_FAIL if audio != shell.NEVER else shell.NEVER
	returncode = 0

	print("Object Compiling Core for Wings")

	timer = util.Timer()
	if timing:
		print("Timing")
		timer.start()

	files = []
	for cpp in cpp_files:
		name = cpp.replace(config.DIR_CORE_SRC, '').replace('/', '+').replace('.cpp', '.o')[1:]
		files.append({ 'inp': cpp, 'out': name })

	command = f"{profile['exe_compiler']} -std={profile['c_std']} -o {dest_folder}/{{out}} " +\
			f"-c {{inp}} {includes} {macros}"

	if arguments.ARG_CONCURRENT:
		returncode = shell.multi_execute(command, files, print_commands, False, audio)
	else:
		for f in files:
			returncode += shell.execute(command.format(inp = f['inp'], out = f['out']), print_commands, False, audio
			)
	
	if timing:
		timer.stop()
		timer.print()
	shell.audio(returncode, audio)
	print("Finished")

def execute():
	play_audio_on_cpp = shell.NEVER if arguments.ARG_AUDIO == shell.NEVER else shell.IF_FAIL

	proj_conf = config.read_config_file('project.conf')
	extl_conf = config.read_config_file('2Externals/externals.conf')
	
	profile = config.combine_profiles(proj_conf['def_profile'] + [ 'wings' ], proj_conf['profiles'])
	includes = '-I' + ' -I'.join(profile['includes']) + ' '
	libs = '-l' + ' -l'.join(profile['libs']) + ' '
	macros = '-D' + ' -D'.join(profile['macros']) + ' '
	options = ' '.join(profile['options'])

	for name, data in extl_conf.items():
		includes += ext.get_and_format_includes(
			ext.get_externals_folder(name),
			data
			)
		macros += ext.get_and_format_macros(
			ext.get_externals_folder(name),
			data
			)
		
		libs += ext.get_and_format_libs(data)

	if os.path.exists(config.DIR_PC_LIBS):
		l = \
			[ f.replace('lib', '').replace('.a', '')\
			for f in os.listdir(config.DIR_PC_LIBS)\
			if os.path.isfile(f"{config.DIR_PC_LIBS}/{f}") ]
		libs += '-l' + ' -l'.join(l)
	
	libs_temp = []
	for lib in libs.split(' '):
		if not lib in libs_temp:
			libs_temp.append(lib)
	
	libs_temp.reverse()
	libs = ' '.join(libs_temp)

	precomp_core(profile, includes, macros, arguments.ARG_PRINT, arguments.ARG_TIMING, arguments.ARG_AUDIO)

	wings = { }
	for root, dir, files in os.walk(config.DIR_WINGS):
		if 'wing' in files:
			wings[root] = []
		
		wing = ''
		for name in wings:
			if root.startswith(name):
				wing = name
				break
		else:
			continue

		for f in files:
			if f.endswith('.cpp'):
				wings[wing].append(root + '/' + f)
	
	output = config.DIR_BUILDS + '/Wings/'
	if not os.path.exists(output):
		os.makedirs(output)
	
	returncode = 0
	for wing, cpp_files in wings.items():
		name = wing[len('Wings/'):]
		dest_folder = f"{config.DIR_PC_OBJS}/Wings/{name}"

		print(f"Building Wing: {name}")

		timer = util.Timer()
		if arguments.ARG_TIMING:
			timer.start()
		
		if not os.path.exists(dest_folder):
			os.makedirs(dest_folder)
		
		# Compile CPP files
		print(f"Object Compiling for {wing}")

		files = []
		for cpp in cpp_files:
			out = cpp.replace(wing + '\\', '').replace('\\', '/').replace('/', '+').replace('.cpp', '.o')
			cpp = cpp.replace('\\', '/')
			files.append({ 'inp': cpp, 'out': out })
		
		cwd = os.getcwd().replace('\\', '/')
		command = f"{profile['exe_compiler']} -std={profile['c_std']} -o {cwd}/{dest_folder}/{{out}} " +\
					f"-c {cwd}/{{inp}} {includes} {macros}"
		if arguments.ARG_CONCURRENT:
			returncode += shell.multi_execute(command, files, arguments.ARG_PRINT, False, play_audio_on_cpp)
		else:
			for f in files:
				returncode += shell.execute(
					command.format(inp = f['inp'], out = f['out']),
					arguments.ARG_PRINT, play_sound = play_audio_on_cpp
				)
		
		print("Building Wing file")

		returncode += shell.execute(
			f"{profile['exe_compiler']} -std={profile['c_std']} -fPIC -shared -o {output + name + '.wng'} " +
			f"{dest_folder}/*.o {config.DIR_PC_OBJS}/Wings/Core/*.o " +
			f"-L{config.DIR_PC_LIBS} {libs}",
			arguments.ARG_PRINT, play_sound = play_audio_on_cpp
		)

		if arguments.ARG_TIMING:
			timer.stop()
			timer.print()
		print("Finished")
	
	shell.audio(returncode, arguments.ARG_AUDIO)

if __name__ == '__main__':
	execute()
