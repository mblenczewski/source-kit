import os

import arguments
import shell
import config
import util
import externals as ext

def execute():
	proj_conf = config.read_config_file('project.conf')
	extl_conf = config.read_config_file('2Externals/externals.conf')
	
	profile = config.combine_profiles(proj_conf['def_profile'], proj_conf['profiles'])
	includes = '-I' + ' -I'.join(profile['includes']) + ' '
	libs = '-l' + ' -l'.join(profile['libs']) + ' '
	macros = '-D' + ' -D'.join(profile['macros']) + ' '
	options = ' '.join(profile['options'])

	for name, data in extl_conf.items():
		includes += ext.get_and_format_includes(
			ext.get_externals_folder(name),
			data
			)
		macros += ext.get_and_format_macros(
			ext.get_externals_folder(name),
			data
			)
		
		libs += ext.get_and_format_libs(data)

	if os.path.exists(config.DIR_PC_LIBS):
		l = \
			[ f.replace('lib', '').replace('.a', '')\
			for f in os.listdir(config.DIR_PC_LIBS)\
			if os.path.isfile(f"{config.DIR_PC_LIBS}/{f}") ]
		libs += '-l' + ' -l'.join(l)
	
	libs_temp = []
	for lib in libs.split(' '):
		if not lib in libs_temp:
			libs_temp.append(lib)
	
	libs_temp.reverse()
	libs = ' '.join(libs_temp)
	
	cpp_files = []
	for root, dir, files in os.walk(config.DIR_CORE_SRC):
		for f in files:
			if f.endswith('.cpp'):
				cpp_files.append((root + '/' + f).replace('\\', '/'))
		

	if not os.path.exists(config.DIR_BUILDS):
		os.makedirs(config.DIR_BUILDS)

	print("Compiling Core")

	if arguments.ARG_CONCURRENT:
		dest_folder = config.DIR_PC_OBJS + "/Core"
		if not os.path.exists(dest_folder):
			os.makedirs(dest_folder)

		cwd = os.getcwd().replace('\\', '/')
		command = f"{profile['exe_compiler']} -std={profile['c_std']} -o {cwd}/{dest_folder}/" +\
		f"{{out}} -c {cwd}/{{inp}} " +\
		f"{includes} {macros} {options}"
		# "{1} -c " + cwd + "{0} " +\

		inputs = []
		for cpp in cpp_files:
			output = cpp.replace('Core/Source/', '').replace('\\', '/').replace('/', '+').replace('.cpp', '.o')
			inputs.append({ 'inp': cpp, 'out': output })

		play_audio_on_cpp = shell.IF_FAIL if arguments.ARG_AUDIO != shell.NEVER else shell.NEVER
		timer = util.Timer()
		if arguments.ARG_TIMING:
			timer.start()
		returncode = shell.multi_execute(command, inputs, arguments.ARG_PRINT, False, play_audio_on_cpp)
		if returncode != 0:
			print("Failure")
			exit()

		print("Building executable")
		shell.execute(
			f"{profile['exe_compiler']} -o {config.DIR_BUILDS}/{proj_conf['executable']} " +
			f"{dest_folder}/*.o -L{config.DIR_PC_LIBS} {libs}",
			arguments.ARG_PRINT, False, arguments.ARG_AUDIO
		)

		if arguments.ARG_TIMING:
			timer.stop()
			timer.print()
	else:
		shell.execute(
			f"{profile['exe_compiler']} -std={profile['c_std']} -o {config.DIR_BUILDS}/{proj_conf['executable']} {' '.join(cpp_files)} " +\
			f"{includes} -L{config.DIR_PC_LIBS} {libs} {macros} {options}"
			, arguments.ARG_PRINT, arguments.ARG_TIMING, arguments.ARG_AUDIO
		)

	print("Finished")

if __name__ == '__main__':
	execute()
