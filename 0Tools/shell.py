import subprocess
import shlex
import shutil
import util
from playsound import playsound

ASSET_PATH = '0Tools/assets/'
ALWAYS = 0
IF_SUCCESS = 1
IF_FAIL = -1
NEVER = 2

AUDIO_FLAG = { 'always': ALWAYS, 'success': IF_SUCCESS, 'fail': IF_FAIL, 'never': NEVER }

sfx_success = "comp_success.wav"
sfx_fail = "comp_fail.wav"

# Runs the passed in terminal command, returns on success
def execute(command, print_command = False, timing = False, play_sound = NEVER):
	if print_command:
		print(f"Command:\n\t{command}\n")
	
	args = []
	if isinstance(command, str):
		args = shlex.split(command.replace('\\', '/'))
	else:
		args = command
	
	timer = util.Timer()
	if timing:
		timer.start()
		print("Shell: Timing execution")
	output = subprocess.run(args)
	if timing:
		timer.stop()
		timer.print()
	
	audio(output.returncode, play_sound)

	return output.returncode

def multi_execute(raw_command: str, files: list, print_command = False, timing = False, play_sound = NEVER):
	processes = []
	command_split = raw_command.split(' ', 1)
	raw_command = shutil.which(command_split[0]) + ' ' + command_split[1]
	raw_command = raw_command.replace('\\', '/')

	timer = util.Timer()
	if timing:
		timer.start()
		print("Multi Shell: Timing Execution")

	for data in files:
		command = raw_command.format(inp = data['inp'], out = data['out'])

		if print_command:
			print(f"Command:\n\t{command}\n")
		
		args = shlex.split(command)
		processes.append(subprocess.Popen(args))
	
	print(f"Sent {len(processes)} process{'es' if len(processes) > 1 else ''}, awaiting completion")

	returncode = 0
	for p in processes:
		p.wait()
		returncode += p.returncode

	if timing:
		timer.stop()
		timer.print()
	
	audio(returncode, play_sound)
	return returncode


def audio(return_code, play_sound=ALWAYS):
	if play_sound == NEVER:
		return
	
	if return_code == 0:
		if play_sound > -1: # ALWAYS and IF_SUCCESS
			playsound(ASSET_PATH + sfx_success)

	if return_code != 0:
		if play_sound < 1: # ALWAYS and IF_FAIL
			playsound(ASSET_PATH + sfx_fail)

