import os

DIR_PC_OBJS = '00PreCompiled'
DIR_PC_LIBS = '00PreCompiled/Libs'

DIR_BUILDS = '00Builds'

DIR_CORE_SRC = 'Core/Source'

DIR_WINGS = 'Wings'

DIR_EDITOR_SRC = '9Editor/Source'

_TOOLS_CONFIG = '0Tools/tools.conf'

def emplace_in_dict(d: dict, path: list, data: dict) -> dict:
	in_d = path[0] in d
	if len(path) == 1:
		if in_d:
			d[path[0]].update(data)
		else:
			d[path[0]] = data
		# print(d)
		# print(data)
		return d

	sub_dict = {}
	if in_d:
		sub_dict = d[path[0]]
	
	sub_dict = emplace_in_dict(sub_dict, path[1:], data)
	
	d[path[0]] = sub_dict
	return d

def write_to_config(parent_config: dict, name: str, config: dict):
	parent_config = emplace_in_dict(parent_config, name.split('/'), config)

def read_config_file(path: str) -> dict:
	config = {}
	with open(path, 'r') as file:
		scope_name = ''
		scope = {}

		for line in file:
			line = line.strip()
			if len(line) == 0 and scope_name != '':
				config = emplace_in_dict(config, scope_name.split('/'), scope)
				scope_name = ''
				scope = {}
				continue
			
			if len(line) == 0 or \
				 line[0] == '#' or line[0] == ';':
				continue

			if line.startswith('$ '): # scope header
				scope_name = line[2:]
				continue
			
			if 'l=' in line: # List variable
				name_data = line.split('l=')
				name = name_data[0].strip()
				data = [ s.strip() for s in name_data[1].split(',') ]

				if scope_name == '':
					config[name] = data
				else:
					scope[name] = data
			
			elif '=' in line: # single variable
				name_data = [ s.strip() for s in line.split('=') ]

				if scope_name == '':
					config[name_data[0]] = name_data[1]
				else:
					scope[name_data[0]] = name_data[1]
			
			else: # flags
				data = [ s.strip() for s in line.split(',') ]

				if scope_name == '':
					if 'flags' in config:
						config['flags'].extend(data)
					else:
						config['flags'] = data
				else:
					if 'flags' in config:
						scope['flags'].extend(data)
					else:
						scope['flags'] = data
	
	config = emplace_in_dict(config, scope_name.split('/'), scope)

	return config

def combine_profiles(profile_names: list, all_profiles: dict) -> dict:
	profile = all_profiles['universal']
	for name in profile_names:
		prf = all_profiles[name]

		for key, value in prf.items():
			is_list = isinstance(value, list)
			if key in profile:
				if isinstance(profile[key], list):
					profile[key].extend(value)
				else:
					profile[key] = value
			else:
				profile[key] = value
	
	return profile

def read_tools_config() -> dict:
	return read_config_file(_TOOLS_CONFIG)
