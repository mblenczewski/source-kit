import sys
import config
import shell

_HELP = """\
	-h | ?  Display this help dialogue
	-timer=(y/n) Time the compilation
	-print=(y/n) Print command
	-audio=(always/success/fail/never) Play a sound upon completion of compilation (Not working)\
	-concurrent=(y/n) Enable concurrent compilation
"""
	# run (Only with editor.py) Runs the related binary

tools_config = config.read_tools_config()
ARG_PRINT = tools_config['print_commands'] == 'y'
ARG_TIMING = tools_config['timing'] == 'y'
ARG_AUDIO = shell.AUDIO_FLAG.get(tools_config['audio'], shell.NEVER)
ARG_CONCURRENT = tools_config['concurrent']
# ARG_RUN = False

if len(sys.argv) != 1:

	args = sys.argv[1:]
	invalid = False
	for arg in args:
		if arg == '-h' or arg == '?':
			print(_HELP)
			exit()
		
		if arg.startswith('-timer='):
			val = arg[len('-timer='):]
			if len(val) > 0:
				ARG_TIMING = val[0] == 'y'
			else:
				print('No value passed to argument -timer')
				invalid = True
		elif arg.startswith('-print='):
			val = arg[len('-print='):]
			if len(val) > 0:
				ARG_PRINT = val[0] == 'y'
			else:
				print('No value passed to argument -print')
				invalid = True
		elif arg.startswith('-audio='):
			val = arg[len('-audio='):]
			if len(val) > 0 or val in shell.AUDIO_FLAG:
				ARG_AUDIO = shell.AUDIO_FLAG.get(val, shell.NEVER)
			else:
				print('No value or an invalid value passed to argument -audio')
				invalid = True
		elif arg.startswith('-concurrent='):
			val = arg[len('-concurrent='):]
			if len(val) > 0:
				ARG_CONCURRENT = val[0] == 'y'
			else:
				print('No value or an invalid value passed to argument -concurrent')
				invalid = True
		# elif arg == 'run':
		# 	ARG_RUN = True
		else:
			print(f'Unrecognised argument: {arg}')

	if invalid:
		exit()
