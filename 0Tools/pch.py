import os

import arguments
import shell
import config

def compile_pch(compiler: str, c_std: str, macros: str, options: str, print_commands: bool, timing: bool, audio: int):
	dest_folder = config.DIR_PC_OBJS	
	if not os.path.exists(dest_folder):
		os.makedirs(dest_folder)
	
	pch_file = 'pch.h'
	shell.execute(
		f"{compiler} -std={c_std} -o {dest_folder}/{pch_file}.gch " +
		f"{config.DIR_CORE_SRC}/_Includes/{pch_file} {macros} {options}",
		print_commands, timing, audio
	)

	with open(f"{dest_folder}/{pch_file}", 'w') as output:
		output.write("#error Not using GCH")

def execute():
	proj_conf = config.read_config_file('project.conf')

	profile = config.combine_profiles(proj_conf['def_profile'], proj_conf['profiles'])
	macros = '-D' + ' -D'.join(profile['macros'])
	options = ' '.join(profile['options'])
	compile_pch(profile['exe_compiler'], profile['c_std'], macros, options, arguments.ARG_PRINT, arguments.ARG_TIMING, arguments.ARG_AUDIO)


if __name__ == '__main__':
	execute()
