import time

class Timer:
	def __init__(self):
		self.start_time = 0.0
		self.elapsed_time = 0.0

	def start(self):
		self.start_time = time.perf_counter()

	def stop(self):
		self.elapsed_time = time.perf_counter() - self.start_time
		self.start_time = 0.0

	def print(self, precision = 4):
		print(f"Total Elapsed Time of {self.elapsed_time:0.{precision}f} secs")
