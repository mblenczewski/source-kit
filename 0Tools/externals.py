import os

import util
import config
import shell
import arguments

def get_externals_folder(name: str) -> str:
	return f'2Externals/{name}'

def get_and_format_includes(ext_folder: str, ext: dict) -> str:
	includes = ''

	if 'includes' in ext:
		for dir in ext['includes']:
			includes += '-I' + ext_folder + '/' + dir + ' '
	else:
		includes = '-I' + ext_folder + ' '

	return includes
	
def get_and_format_macros(ext_folder: str, ext: dict) -> str:
	macros = ''

	if 'macros' in ext:
		for macro in ext['macros']:
			macros += '-D' + macro + ' '

	return macros
	
def get_and_format_libs(ext: dict) -> str:
	libs = ''

	if 'libs' in ext:
		for lib in ext['libs']:
			libs += '-l' + lib + ' '

	return libs

def find_source_files(ext_folder: str, ext: dict) -> list:
	srcs = []
	if 'src' in ext:
			ext_folder += '/' + ext['src']
	
	has_targets = 'target_srcs' in ext
	for root, dir, files in os.walk(ext_folder):
		for f in files:
			if has_targets and not f in ext['target_srcs'] or not f.endswith(('cpp', 'c')):
				continue
			
			src = (root + '/' + f).replace('\\', '/')
			srcs.append(src)
	
	return srcs

def execute():
	conf = config.read_config_file('project.conf')
	conf_externals = config.read_config_file('2Externals/externals.conf')

	play_audio_on_cpp = shell.IF_FAIL if arguments.ARG_AUDIO != shell.NEVER else shell.NEVER
	returncode = 0

	for name in conf_externals:
		ext = conf_externals[name]

		if 'flags' in ext and 'header_only' in ext['flags']:
			print(f"{name} is header only. Skipping")
			continue

		print(f"Building External {name}")
		root = get_externals_folder(name)
		if not os.path.exists(root):
			print(f"Could not find external {name} at root {root}")
			continue

		dest_dir = f"{config.DIR_PC_OBJS}/Externals/{name}"
		if not os.path.exists(dest_dir):
			os.makedirs(dest_dir)

		success = True

		timer = util.Timer()
		if arguments.ARG_TIMING:
			print(f'Timing build of {name}')
			timer.start()

		cpp_files = find_source_files(root, ext)
		compiler = ext['compiler'] if 'compiler' in ext else 'g++'
		c_std = ext['c_std'] if 'c_std' in ext else 'c++1z'
		includes = get_and_format_includes(root, ext)
		macros = get_and_format_macros(root, ext)

		obj_name_begin = len(root) + 1

		print(f"Object Compiling for {name}")

		files = []
		for cpp in cpp_files:
			output = dest_dir + '/' + cpp[obj_name_begin:].replace('/', '+').replace('.cpp', '.o').replace('.c', '.o')
			files.append({ 'inp': cpp, 'out': output })

		command = f"{compiler} -std={c_std} -o {{out}} -c {{inp}} {includes} {macros}"
		if arguments.ARG_CONCURRENT:
			success = 0 == shell.multi_execute(
				command, files, arguments.ARG_PRINT, play_sound = play_audio_on_cpp
			)
		else:
			for f in files:
				# obj = dest_dir + '/' + f[obj_name_begin:].replace('/', '+').replace('.cpp', '.o').replace('.c', '.o')

				success = 0 == shell.execute(
					command.format(inp = f['inp'], out = f['out']),
					arguments.ARG_PRINT, play_sound = play_audio_on_cpp
				)

				if not success:
					break
		
		if not success:
			print(f"Error with compiling {name}. Skipping")
			returncode = 1
			continue
		
		if not os.path.exists(config.DIR_PC_LIBS):
			os.makedirs(config.DIR_PC_LIBS)
		
		print("Building Library file")
		returncode += shell.execute(
			f"ar rvs {config.DIR_PC_LIBS}/lib{name}.a {dest_dir}/*.o",
			arguments.ARG_PRINT, play_sound = play_audio_on_cpp
		)
		if arguments.ARG_TIMING:
			timer.stop()
			timer.print()

	print("Finished")
	shell.audio(returncode, arguments.ARG_AUDIO)

if __name__ == '__main__':
		execute()
