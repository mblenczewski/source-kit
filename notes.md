### C++ R strings

Declare a string with R"( ... )" and it will allow you to type on multiple lines
R"(
	Hello
	Line 2
)"

The newline char after 'Hello' is preserved