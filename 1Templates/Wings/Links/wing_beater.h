#pragma once
#ifndef BLD_WING

#include "core"

#include "Util/singleton.h"
#include "wing_funcs.h"
#include "dllHandle.h"

#ifdef WING_EXTENSION
	#define TEMP "." WING_EXTENSION
	#undef WING_EXTENSION
	#define WING_EXTENSION TEMP
#else
	#define WING_EXTENSION ".wng"
#endif

class WingBeater : public MxnSingleton<WingBeater>
{
public:
	~WingBeater();
	
	static void GatherWings();
	static unsigned int WingCount() { return Get()->_wingCount; }

$$
$# $if 'ret' in link
$# 	static void Call{link_name}(std::vector<{ret}>& returns);
$# $else
	static void Call{link_name}();
$# $endif
$x
$# Should produce
$# static void CallStartup();
$#/ Kept around so that they remain in memory

private:
	DLLHandle **_wings;
	unsigned int _wingCount;

$$
	Wng{link_name} *WLinks{link_name};
$x

	// Can't forgo this
	WingBeater(DLLHandle **wings, unsigned int count
$$
	, Wng{link_name} *{link_name}
$x
);
};
#endif // not BLD_WING
