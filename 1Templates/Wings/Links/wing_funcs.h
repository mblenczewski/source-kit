#pragma once

#ifdef BLD_WING

	#ifdef PLT_WINDOWS
		#define WING_API __declspec(dllexport)
		#define WING_FUNC(func) func
	#else
		#error Can only build to Windows
	#endif
	

	#ifdef __cplusplus
	// Because C++ can handle overloading, name mangling occurs during compilation
	// declaring these functions as C functions prevents the name mangling
	// although they cannot be overloaded
extern "C" {
	#endif // __cplusplus
#else
	#define WING_API typedef
	#define WING_FUNC(func) (*Wng##func)
#endif

$$
WING_API {ret} WING_FUNC({link_name})();
$X

$# Should produce:
$# WING_API void WING_FUNC(Startup);

#ifdef BLD_WING
	#ifdef __cplusplus
}
	#endif // __cplusplus
#endif

#undef WING_API
#undef WING_FUNC
