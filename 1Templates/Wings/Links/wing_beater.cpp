#ifndef BLD_WING
#include "pch.h"
#include "debug"
#include "core"

#include "wing_beater.h"
#include "wing_funcs.h"

WingBeater::WingBeater(DLLHandle **wings, unsigned int count
$$
		, Wng{link_name} *{link_name}
$x
)
		: _wings(wings), _wingCount(count), MxnSingleton<WingBeater>(this)
$$
		, WLinks{link_name}({link_name})
$x
{ }

WingBeater::~WingBeater()
{
	// Sometimes it will crash here, other times it will not, completely random

	for (unsigned int i = 0; i < _wingCount; i--)
		delete _wings[i];
	
	delete _wings;
}

void WingBeater::GatherWings()
{
	PRF_FN;
	std::vector<DLLHandle*> wings;

	std::string exe_dir;

#ifdef PLT_WINDOWS
	{
		char pBuf[256];
		size_t len = sizeof(pBuf); 
		int bytes = GetModuleFileName(NULL, pBuf, len);
		std::string path = std::string(pBuf, bytes ? bytes : -1);
		exe_dir = path.substr(0, path.find_last_of('\\'));
	}
#else
	#error Can only build to windows
#endif

	std::string wing_dir = exe_dir + "/Wings";

	if (!fs::exists(wing_dir))
	{
		new WingBeater(
			nullptr, 0
			, nullptr
			, nullptr
		);
		return;
	}

	for (auto p : fs::recursive_directory_iterator(wing_dir))
	{
		if (p.is_directory() || p.path().extension().string().compare(WING_EXTENSION) != 0)
			continue;

		std::string path = p.path().string();

		// When DLLHandle goes out of scope, it frees the library
		DLLHandle *h = new DLLHandle(path.c_str());

		if (h->Get())
			wings.push_back(h);
		else
			LG_ERR("File {0} is not a hook", path);
	}

	int size = wings.size();

$$
	Wng{link_name} *wLinks{link_name} = (Wng{link_name}*)malloc(sizeof(Wng{link_name}*) * size);
	for (unsigned int i = 0; i < size; i++)
	{{
		Wng{link_name} pointer
				= reinterpret_cast<Wng{link_name}>(GetProcAddress(wings[i]->Get(), "{link_name}"));

		if (pointer)
		{{
			wLinks{link_name}[i] = pointer;
		}}
		else
		{{
			LG_ERR("Loading {link_name} from {{0}} failed", wings[i]->GetPath());
$# $if has_ret
$# 			wLinks{link_name}[i] = []() -> {ret} {{}};
$# $else
			wLinks{link_name}[i] = [](){{}};
$# $endif
		}}
	}}

$x
	new WingBeater(
		&wings[0], size
$$
		, wLinks{link_name}
$x
	);
}

// #############################
// Calls to wing functions
// #############################

$$
$# $if 'ret' in link
$# void WingBeater::Call{link_name}(std::vector<{ret}>& returns)
$# {{
$#  	for (unsigned int i = 0; i < sInstance->WingCount; i++)
$#  	{{
$#  		returns.push_back(Get()->WLinks{link_name}[i]());
$#  	}}
$# }}
$# $else
void WingBeater::Call{link_name}()
{{
	for (unsigned int i = 0; i < Get()->WingCount(); i++)
	{{
		Get()->WLinks{link_name}[i]();
	}}
}}
$# $endif
$x
#endif // not BLD_WING