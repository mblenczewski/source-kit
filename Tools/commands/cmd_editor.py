from .command import Command
import globals
import build_profiles
import os
from compilation import pch
from compilation import compiler

import compilation as cmp

class CmdEditor(Command):
	def __init__(self):
		super().__init__()
		self.name = "Editor Utility"
		self.desc = "Commands for using and compiling the editor"
		self.keys = [ 'editor' ]
		self.help = self.desc + f':\n {self.keys[0]} (run): Boots up the editor\n {self.keys[0]} build: Compiles the editor'
	
	def execute(self, args):
		if len(args) == 1:
			self.run()
			return
		
		if args[1] == 'build':
			self.build()


	def run(self):
		os.system(f".\\{globals.DIR_BUILDS}\\editor.exe")
	
	def build(self):
		print("Building editor")
		profile = globals.BLD_DEFAULT

		for ext in globals.EXTERNALS_DATA.values():
			profile['libs'] = ext['libs'] + profile['libs']
			profile['includes'] = profile['includes'] + ext['includes']

		if os.path.exists(globals.DIR_PC_LIBS):
			profile['libs'] = \
				[ f.replace('lib', '').replace('.a', '')\
				for f in os.listdir(globals.DIR_PC_LIBS)\
				if os.path.isfile(f"{globals.DIR_PC_LIBS}/{f}") ]\
				+ profile['libs']

		cpp_files = []
		first = True # To skip over Core's main.cpp file
		for root, dir, files in os.walk(globals.DIR_CORE_CODE):
			if first:
				first = False
				continue

			to_include = False
			for f in files:
				if f.endswith('.cpp'):
					to_include = True
					break
			else:
				continue
			
			cpp_files.append(f"{root}/*.cpp")
		
		for root, dir, files in os.walk('9Editor/Source'):
			to_include = False
			for f in files:
				if f.endswith('.cpp'):
					to_include = True
					break
			else:
				continue
			
			cpp_files.append(f"{root}/*.cpp")
		
		output = f"{globals.DIR_BUILDS}"
		if not os.path.exists(output):
			os.makedirs(output)

		profile = globals.to_compiler_format(profile)
		
		pch.pre_compile_headers(profile)
		print("Compiling Core")
		compiler.execute(\
			f"{profile['compiler']} {profile['c_std']} -o {output}/editor.exe {' '.join(cpp_files)} " +\
			f"{profile['includes']} -L{globals.DIR_PC_LIBS} {profile['libs']} {profile['macros']} -DBUILD_EDITOR {profile['options']}"
		)
		print("Finished")