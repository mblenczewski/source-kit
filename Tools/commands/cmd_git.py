from workbench.command import Command
import subprocess
import globals

def grun(*args):
	command = [ 'git', *args]
	# print(command)
	subprocess.run(command)

def gcommit(message = '', add = True, amend = False):
	if amend:
		grun('commit', '--amend')
		return

	if add:
		grun('add', '.')

	if isinstance(message, str) and message != '':
		grun('commit', '-m', message)
	else:
		grun('commit')
	

class CmGCommit(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Relay to `git commit`'
		self.keys = [ 'gcom' ]
		self.desc = 'A relay to the git command `git commit`'
	
	def execute(self, args):
		add = True
		if '--no-a' in args:
			add = False
			args.remove('--no-a')

		amend = False
		if '--amend' in args:
			args.remove('--amend')
			amend = True
		
		message = ''
		if len(args) > 1:
			message = ' '.join(args[1:])
		
		gcommit(message, add, amend)

class CmGMV(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Relay to `git mv`'
		self.keys = [ 'gmv' ]
		self.desc = 'A relay to the git command `git mv`'
	
	def execute(self, args):
		args = args[1:]
		formatted = []
		for arg in args:
			formatted.append(f"{globals.CURRENT_DIR}/{arg}" if len(globals.CURRENT_DIR) > 0 else arg)
		
		grun('mv', *formatted)

class CmGPush(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Relay to `git push`'
		self.keys = [ 'gpush' ]
		self.desc = 'A relay to the git command `git push`'
	
	def execute(self, args):
		args = args[1:]
		grun('push', *args)

# class CmGBranch(Command):
# 	def __init__(self):
# 		super().__init__()
# 		self.name = 'Relay to `git checkout` and `git branch`'
# 		self.keys = [ 'gbr' ]
# 		self.desc = 'A relay to the git command `git checkout` and `git branch`'
	
# 	def execute(self, args):
# 		args = args[1:]
# 		grun('branch', *args)

class CmGit(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Relay to `git checkout` and `git branch`'
		self.keys = [ 'git' ]
		self.desc = 'A relay to the git command `git`'
	
	def execute(self, args):
		args = args[1:]
		grun(*args)
