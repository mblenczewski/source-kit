from workbench.command import Command
from code_gen.wings import *

class CmWingLinksCodeGen(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Wing Links Files Generator'
		self.keys = [ 'wing-gen' ]
		self.desc = '(Re)Generates the files for linking against Wings from the Core'
	
	def execute(self, args):
		gen_wing_funcs()
		gen_wing_beater()
