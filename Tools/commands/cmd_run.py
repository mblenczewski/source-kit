from .command import Command
import globals
import os

class CmRun(Command):
	def __init__(self):
		super().__init__()
		self.name = "Run Program"
		self.desc = "Runs the current available build of the project"
		self.keys = [ 'run' ]
	
	def execute(self, args):
		if not os.path.exists(globals.DIR_BUILDS):
			print("No builds are available")
			return

		# os.chdir(globals.DIR_BUILDS)
		os.system(f".\\{globals.DIR_BUILDS}\\{globals.CORE_EXE_NAME}")
		# os.chdir('..')

class CmDebugRun(Command):
	def __init__(self):
		super().__init__()
		self.name = "Debug Run Program"
		self.desc = "Runs the current available build of the project to debug it"
		self.keys = [ 'debug', 'dbg', 'gdb' ]
	
	def execute(self, args):
		if not os.path.exists(globals.DIR_BUILDS):
			print("No builds are available")
			return
		
		if len(args) == 1:
			os.system(f"gdb .\\{globals.DIR_BUILDS}\\{globals.CORE_EXE_NAME}")
		elif args[1] == 'editor':
			os.system(f"gdb .\\{globals.DIR_BUILDS}\\editor.exe")


