import os
import sys

class Command:
	def __init__(self):
		self.name = 'No Title'
		self.keys = [ '' ]
		self.desc = 'Overwrite default command description'
		self.help = 'Overwrite default help description'
	
	# Will eventually need to extend this to include the distance from the name
	# and return that value, too
	# This will be used to help with autocomplete and to suggest what the user meant
	# if no matching command is found
	def compare(self, command):
		for n in self.keys:
			if n == command:
				return True
		
		return False
	
	def execute(self, args):
		print('Executing {} with args {}'.format(self.name, str(args)))
