from .command import Command
import globals
import build_profiles

import compilation as cmp
import compilation.wings
import compilation.externals
import compilation.core
import compilation.pch

# from _Tools.compile import *
# import _Tools.compile.externals
# import _Tools.compile.core
# import _Tools.compile.wings

# from _Tools._config import *

class CmdPreCompile(Command):
	def __init__(self):
		super().__init__()
		self.name = "Pre Compile Code"
		self.desc = f"Creates the obj files and static libraries that would be placed in {globals.DIR_PRE_COMP}"
		self.keys = [ 'pre-compile', 'precompile', 'precomp', 'pc' ]
	
	def execute(self, args):
		
		build_profile_names = [ arg.replace('-bp', '', 1) for arg in args if arg.startswith('-bp') ]
		
		profiles = []
		for name in build_profile_names:
			if name in globals.BUILD_PROFILES:
				profiles.append(globals.BUILD_PROFILES[name])

		bp = {}
		if profiles == []:
			bp = globals.BLD_DEFAULT
		else:
			bp = build_profiles.combine_build_profiles(profiles)
		
		if 'cfw' in args or 'core-for-wings' in args:
			if 'wings' in globals.BUILD_PROFILES:
				profiles = [ bp, globals.BUILD_PROFILES['wings'] ]
				bp = build_profiles.combine_build_profiles(profiles)
			
			cmp.wings.precomp_core(bp)
		
		elif 'ext' in args or 'externals' in args:
			externals = {}
			for name in globals.EXTERNALS_DATA:
				if name in args:
					externals[name] = globals.EXTERNALS_DATA[name]
				
			if len(externals) == 0:
				externals = globals.EXTERNALS_DATA

			cmp.externals.precomp_externals(externals)

		else:
			cmp.pch.pre_compile_headers(bp)


class CmBuild(Command):
	def __init__(self):
		super().__init__()
		self.name = "Builds Code"
		self.desc = "Builds the core and wings"
		self.keys = [ 'build' ]

	def execute(self, args):
		args = args[1:]
		timing = False
		if '-t' in args:
			timing = True
			args.remove('-t')
		
		
		bp_names = []
		for i in range(len(args) - 1, 0, -1):
			arg = args[i]
			if arg.startswith('-bp'):
				args.remove(arg)
				bp_names.append(arg)

		profiles = []
		for name in bp_names:
			if name in globals.BUILD_PROFILES:
				profiles.append(globals.BUILD_PROFILES[name])
		
		if profiles == []:
			profiles = [ globals.BLD_DEFAULT ]

		core = False
		precomp_core = True
		comp_wings = False
		wings = {}

		if len(args) == 0:
			core = True
			comp_wings = True
		else:
			if 'core' in args:
				core = True
				args.remove('core')
			
			if '--no-pc' in args:
				precomp_core = False
				args.remove('--no-pc')
			
			if 'wings' in args:
				comp_wings = True
				args.remove('wings')
			else:
				for arg in args:
					if arg in globals.WINGS_DATA.keys():
						wings[arg] = globals.WINGS_DATA[arg]
		
		if comp_wings:
			wings = globals.WINGS_DATA
		elif wings != {}:
			comp_wings = True
		

		build_profile = build_profiles.combine_build_profiles(profiles)

		if core:
			cmp.core.build_core(build_profile)
			print('Post core')
			if precomp_core:
				if 'wings' in globals.BUILD_PROFILES:
					build_profile = build_profiles.combine_build_profiles([ build_profile, globals.BUILD_PROFILES['wings'] ])
				cmp.wings.precomp_core(build_profile)
				print('Post pc core')
		
		if comp_wings:
			if 'wings' in globals.BUILD_PROFILES:
				build_profile = build_profiles.combine_build_profiles([ build_profile, globals.BUILD_PROFILES['wings'] ])
			
			cmp.wings.build_wings(build_profile, wings)
			print('Post wings')


		# args = args[1:]


		# build_profile_names = []
		# no_bp_args = []

		# for arg in args:
		# 	if arg.startswith('-bp'):
		# 		build_profile_names.append(arg.replace('-bp', ''))
		# 	else:
		# 		no_bp_args.append(arg)

		# profiles = []
		# for name in build_profile_names:
		# 	if name in globals.BUILD_PROFILES:
		# 		profiles.append(globals.BUILD_PROFILES[name])

		# bp = {}
		# if profiles == []:
		# 	bp = globals.BLD_DEFAULT
		# else:
		# 	bp = build_profiles.combine_build_profiles(profiles)



		# if 'core' in args:
		# 	cmp.core.build_core(bp)
		
		# if 'wings' in args:
		# 	no_bp_args.remove('wings')
			
		# 	if 'wings' in globals.BUILD_PROFILES:
		# 		profiles = [ bp, globals.BUILD_PROFILES['wings'] ]
		# 		bp = build_profiles.combine_build_profiles(profiles)
			
		# 	wings = {}
		# 	for arg in no_bp_args:
		# 		for name in globals.WINGS_DATA:
		# 			if arg == name:
		# 				wings[name] = globals.WINGS_DATA[name]
			
		# 	cmp.wings.build_wings(bp, wings)
