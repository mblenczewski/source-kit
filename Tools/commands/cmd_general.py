from .command import Command
import globals
import subprocess

import os
import time

def evaluate_new_dir(path: str) -> str:
	curr = globals.CURRENT_DIR

	curr_dirs = curr.split('/') if curr != '' else []

	split_path = path.replace('\\', '/').split('/')
	for p in split_path:
		if p == '..':
			if len(curr_dirs) > 0:
				curr_dirs = curr_dirs[:-1]
		elif p == '.':
			continue
		else:
			curr_dirs.append(p)

	dest = '/'.join([d for d in curr_dirs if d != ''])
	if os.path.exists(dest) or len(dest) == 0:
		return dest
	else:
		print(f"Path {path} not found")
		return curr


class CmQuit(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Quit'
		self.desc = 'Quits the workbench'
		self.keys = [ 'quit', 'q' ]

	def execute(self, args):
		exit()

class CmChangeDir(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Change Directory'
		self.desc = 'Changes the current working directory'
		self.keys = [ 'cd' ]
	
	def execute(self, args):
		if len(args) == 1:
			print('No destination given')
			return

		globals.CURRENT_DIR = evaluate_new_dir(args[1])

class CmListDirContents(Command):
	def __init__(self):
		super().__init__()
		self.name = 'List Directory Contents'
		self.desc = 'Lists the files and folders in the current directory'
		self.keys = [ 'ls' ]
	
	def execute(self, args):
		dir = globals.CURRENT_DIR
		if len(args) > 1:
			dir = evaluate_new_dir(args[1])

		root = os.getcwd() + '/' + dir
		dir_items = os.listdir(root)
		dirs = []
		files = []
		longest_file_name = 0
		for i in dir_items:
			fullname = root + '/' + i
			if os.path.isdir(fullname):
				dirs.append(i)
			else:
				files.append(i)
				if len(i) > longest_file_name:
					longest_file_name = len(i)
		
		for d in dirs:
			print(d)
		
		for f in files:
			fullname = root + '/' + f
			stats = os.stat(fullname)
			print(f"{f: <{longest_file_name + 3}} {time.ctime(stats.st_mtime)}    {stats.st_size}")

class CmListCommands(Command):
	def __init__(self):
		super().__init__()
		self.name = 'List Commands'
		self.desc = 'Lists all available commands'
		self.keys = [ 'lc', 'list-cmds', 'help', '?' ]
		self.info_string = ''
	
	def get_info_string(self):
		if self.info_string != '':
			return
		
		longest_title = 20
		for c in globals.ALL_COMMANDS:
			if len(c.name) > longest_title:
				longest_title = len(c.name)
		
		self.info_string = ''
		for c in globals.ALL_COMMANDS:
			self.info_string += f"   {c.name: <{longest_title + 5}} {' '.join(c.keys)}\n     {c.desc}\n"

	def execute(self, args):
		if len(args) > 1:
			for c in globals.ALL_COMMANDS:
				if args[1] in c.keys:
					print(c.help)
					return

		self.get_info_string()
		print(self.info_string)

class CmClear(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Clear Console'
		self.desc = ''
		self.keys = [ 'clear', 'cls' ]
		self.info_string = ''
	
	def execute(self, args):
		# If Machine is running on Windows, use cls
		os.system(('cls' if os.name in ('nt', 'dos') else 'clear'))

class CmReload(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Reload Configuration'
		self.desc = ''
		self.keys = [ 'reload' ]
		self.info_string = ''
	
	def execute(self, args):
		globals.load_configs()

class CmdToggle(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Toggle Global Variables'
		self.keys = [ 'toggle' ]
	
	def execute(self, args):
		if len(args) == 1:
			print("No globals specified")
			return
		
		if 'print-commands' in args or 'print' in args:
			globals.PRINT_COMPILER_COMMANDS = not globals.PRINT_COMPILER_COMMANDS
			print(f"Print Compiler Commands is set to {globals.PRINT_COMPILER_COMMANDS}")

		if 'time' in args or 'time-comp' in args:
			globals.TIME_COMPILATION = not globals.TIME_COMPILATION
			print(f"Time Compilation is set to {globals.TIME_COMPILATION}")

class CmPrintData(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Print Raw Data'
		self.keys = [ 'print-data', 'pdata' ]

	def execute(self, args):
		l_args = len(args)
		if l_args == 1:
			return
		
		if args[1] == 'ext':
			print_ext = lambda key : print(f"{key}:\n	{globals.EXTERNALS_DATA[key]}")

			if l_args > 2:
				exts = args[2:]
				[ print_ext(name) for name in exts if name in globals.EXTERNALS_DATA ]
			else:
				[ print_ext(key) for key in globals.EXTERNALS_DATA ]

class CmRaw(Command):
	def __init__(self):
		super().__init__()
		self.name = 'Executes a shell command'
		self.keys = [ 'raw' ]
	
	def execute(self, args):
		if len(args) < 1:
			print("No command passed")
			return
		
		args = args[1:]
		# Throws an error for some reason
		subprocess.check_call(args)
		# compilation.compiler.execute(args, play_sound=compilation.compiler.NEVER)
