import os

RAWS = {}

def emplace_in_dict(d: dict, path: list, data: dict) -> dict:
	in_d = path[0] in d
	if len(path) == 1:
		if in_d:
			d[path[0]].update(data)
		else:
			d[path[0]] = data
		# print(d)
		# print(data)
		return d

	sub_dict = {}
	if in_d:
		sub_dict = d[path[0]]
	
	sub_dict = emplace_in_dict(sub_dict, path[1:], data)
	
	d[path[0]] = sub_dict
	return d

def read_config_file(path: str):
	config = {}
	with open(path) as file:
		scope_name = ''
		scope = {}

		for line in file:
			line = line.strip()
			if len(line) == 0 and scope_name != '':
				config = emplace_in_dict(config, scope_name.split('/'), scope)
				# config[scope_name] = scope
				scope_name = ''
				scope = {}
				continue

			if len(line) == 0 or \
				 line[0] == '#' or line[0] == ';':
				continue
			
			if line.startswith('$ '):
				scope_name = line[2:]
				continue
			
			if 'l=' in line: # List variable
				name_data = line.split('l=')
				name = name_data[0].strip()
				data = [ s.strip() for s in name_data[1].split(',') ]

				if scope_name == '':
					config[name] = data
				else:
					scope[name] = data
			
			elif '=' in line: # single variable
				name_data = [ s.strip() for s in line.split('=') ]

				if scope_name == '':
					config[name_data[0]] = name_data[1]
				else:
					scope[name_data[0]] = name_data[1]
			
			else: # flags
				data = [ s.strip() for s in line.split(',') ]

				if scope_name == '':
					if 'flags' in config:
						config['flags'].extend(data)
					else:
						config['flags'] = data
				else:
					if 'flags' in config:
						scope['flags'].extend(data)
					else:
						scope['flags'] = data
	
	return config

def combine_profiles(profiles:list) -> dict:
	global PROFILE_VARS
	global PRF_UNIVERSAL
	profiles.append(PRF_UNIVERSAL)
	
	result = {}
	for key in PROFILE_VARS:
		is_list = any([type(profile[key]) is list for profile in profiles if key in profile])

		if is_list:
			all = []
			[ all.extend(profile[key] if type(profile[key]) is list else [ profile[key] ]) for profile in profiles if key in profile ] 
			result[key] = list(dict.fromkeys(all))
		else:
			for profile in profiles:
				if key in profile:
					result[key] = profile[key]

	return result

PRJ_NAME = ''
EXE_NAME = ''
DIR_CODE = ''

# Core
DIR_CORE = ''

# Modules
DIR_MODULES = ''
MOD_EXTENSION = ''
MOD_LINKS = []

# Editor
DIR_EDITOR = ''

# Externals
DIR_EXTERNALS = ''
EXT_DATA = {}

# Profiles
PRF_DEFAULT = {}
PRF_UNIVERSAL = {}
ALL_PROFILES = {}
PROFILE_VARS = set()

def populate_base():
	global RAWS
	global PRJ_NAME
	global EXE_NAME
	global DIR_CODE

	if 'name' in RAWS:
		PRJ_NAME = RAWS['name']
	
	if 'executable' in RAWS:
		EXE_NAME = RAWS['executable']
	
	if 'dir_code' in RAWS:
		DIR_CODE = RAWS['dir_code']

def populate_core():
	global RAWS
	global DIR_CORE

	if 'core' in RAWS:
		core = RAWS['core']

		if 'dir' in core:
			DIR_CORE = core['dir']

def populate_modules():
	global RAWS
	global DIR_MODULES
	global MOD_EXTENSION
	global MOD_LINKS

	if 'modules' in RAWS:
		modules = RAWS['modules']

		if 'dir' in modules:
			DIR_MODULES = modules['dir']
		
		if 'extension' in modules:
			MOD_EXTENSION = modules['extension']
		
		if 'links' in modules:
			MOD_LINKS = modules['links']

def populate_editor():
	global RAWS
	global DIR_EDITOR

	if 'editor' in RAWS:
		editor = RAWS['editor']

		if 'dir' in editor:
			DIR_EDITOR = editor['dir']

def populate_externals():
	global RAWS
	global DIR_EXTERNALS
	global EXT_DATA

	if 'externals' in RAWS:
		externals = RAWS['externals']

		if 'dir' in externals:
			DIR_EXTERNALS = externals['dir']
		
		if 'config_path' in externals:
			config_path = DIR_EXTERNALS + '/' + externals['config_path']

			config = read_config_file(config_path)
			EXT_DATA = {}

			prop_names = [ 'compiler', 'src', 'includes', 'macros', 'options', 'c_std', 'libs', 'target_srcs', 'header_only' ]

			for name, ext in config.items():
				new_ext = {}

				for key in prop_names:
					data = ext[key] if key in ext else ''
					
					if data == '':
						if key == 'compiler':
							data = PRF_DEFAULT['exe_compiler']
						elif key == 'c_std':
							data = PRF_DEFAULT['c_std']
					
					if key == 'src' or key == 'includes' or key == 'libs' or key == 'macros' or key == 'target_srcs':
						if type(data) is not list:
							data = [ data ]
						
						if '' in data:
							data.remove('')
						
						if key == 'src' or key == 'includes':
							if data == []:
								data = [ f"{DIR_EXTERNALS}/{name}" ]
							else:
								data = [ f"{DIR_EXTERNALS}/{name}{'/' + i if i !=  '.' else ''}" for i in data ]

					new_ext[key] = data
				
				EXT_DATA[name] = new_ext

def populate_profiles():
	global RAWS
	global PRF_DEFAULT
	global PRF_UNIVERSAL
	global ALL_PROFILES
	global PROFILE_VARS

	if 'profiles' not in RAWS:
		return
	
	profiles = RAWS['profiles']

	if 'universal' in profiles:
		PRF_UNIVERSAL = profiles['universal']
	
	profiles.pop('universal')

	for key, _ in PRF_UNIVERSAL.items():
		PROFILE_VARS.add(key)
	
	for key, profile in profiles.items():
		ALL_PROFILES[key] = combine_profiles([ PRF_UNIVERSAL, profile ])
	
	PRF_DEFAULT = combine_profiles([ ALL_PROFILES[prf_name] for prf_name in RAWS['def_profile'] ])

def populate_globals():
	populate_base()
	populate_core()
	populate_modules()
	populate_editor()
	populate_profiles()
	populate_externals()


RAWS = read_config_file('project.conf')
populate_globals()
