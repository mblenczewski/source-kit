import os
import globals
# import datetime
from . import pch
from . import compiler

def build_core(profile):
	for ext in globals.EXTERNALS_DATA.values():
		profile['libs'] = ext['libs'] + profile['libs']
		profile['includes'] = profile['includes'] + ext['includes']

	if os.path.exists(globals.DIR_PC_LIBS):
		profile['libs'] = \
			[ f.replace('lib', '').replace('.a', '')\
			for f in os.listdir(globals.DIR_PC_LIBS)\
			if os.path.isfile(f"{globals.DIR_PC_LIBS}/{f}") ]\
			+ profile['libs']

	cpp_files = []
	for root, dir, files in os.walk(globals.DIR_CORE_CODE):
		to_include = False
		for f in files:
			if f.endswith('.cpp'):
				to_include = True
				break
		else:
			continue
		
		cpp_files.append(f"{root}/*.cpp")
	
	output = f"{globals.DIR_BUILDS}"
	if not os.path.exists(output):
		os.makedirs(output)

	profile = globals.to_compiler_format(profile)
	
	pch.pre_compile_headers(profile)
	print("Compiling Core")
	compiler.execute(\
		f"{profile['compiler']} {profile['c_std']} -o {output}/{globals.CORE_EXE_NAME} {' '.join(cpp_files)} " +\
		f"{profile['includes']} -L{globals.DIR_PC_LIBS} {profile['libs']} {profile['macros']} {profile['options']}"
	)
	print("Finished")