import os
import globals
from . import compiler

def precomp_externals(externals):
	for name in externals:
		ext = externals[name]
		if ext['header_only'] == '---':
			print(f"{name} is header only. Skipping")
			continue

		print(f"Building External {name}")
		root = f"{globals.DIR_EXTERNALS}/{name}"
		if not os.path.exists(root):
			print(f"Could not find external {name} at root {root}")
			continue

		ext = globals.to_compiler_format(ext)

		dest_dir = f"{globals.DIR_PC_OBJS}/Externals/{name}"
		if not os.path.exists(dest_dir):
			os.makedirs(dest_dir)

		objs = []
		success = True

		for root, dir, files in os.walk(ext['src']):
			for f in files:
				if (not f.endswith(('cpp', 'c'))) or not globals.check_if_in_target_srcs(ext, f):
					continue
				
				cpp_path = root + '/' + f
				obj_path = dest_dir + '/' + f.replace('.cpp', '.o').replace('.c', '.o')

				success = success and compiler.execute(f"{ext['compiler']} {ext['c_std']} -o {obj_path} -c {cpp_path} {ext['includes']} {ext['macros']}", play_sound=compiler.IF_FAIL)
				if not success:
					break
				objs.append(obj_path)
			
			if not success:
				break
		
		if not success:
			print(f"Error with compiling {name}. Skipping")
			continue
		
		if len(objs) == 0:
			print('No source files used. Skipping')
			continue

		# Build the static lib
		if not os.path.exists(globals.DIR_PC_LIBS):
			os.makedirs(globals.DIR_PC_LIBS)
		lib_dest = f"{globals.DIR_PC_LIBS}/lib{name}.a"
		
		compiler.execute(f"{globals.STATIC_LIB_COMPILER} {lib_dest} {' '.join(objs)}", play_sound=compiler.IF_FAIL)
		print("Finished")
	
	compiler.audio(0)
