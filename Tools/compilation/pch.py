import globals
import os
from . import compiler

def pre_compile_headers(build_profile):
	dest_folder = f"{globals.DIR_PRE_COMP}"
	if not os.path.exists(dest_folder):
		os.makedirs(dest_folder)

	input_file = 'pch.h'
	
	compiler.execute(f"{globals.COMPILER} {build_profile['c_std']} {globals.DIR_CORE_CODE}/_Includes/{input_file} -o {dest_folder}/{input_file}.gch {build_profile['macros']} -municode", play_sound=compiler.IF_FAIL) # -Wall -municode {build_profile['options']}")

	with open(f"{dest_folder}/{input_file}", 'w') as output:
		output.write("#error Not using GCH")
