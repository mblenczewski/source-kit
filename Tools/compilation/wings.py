import os
import globals
from . import pch
from . import compiler

def precomp_core(profile):
	for ext in globals.EXTERNALS_DATA.values():
		profile['libs'] = ext['libs'] + profile['libs']
		profile['includes'] = profile['includes'] + ext['includes']
	
	dest_folder = f"{globals.DIR_PC_OBJS}/Wings/Core"
	if not os.path.exists(dest_folder):
		os.makedirs(dest_folder)

	profile = globals.to_compiler_format(profile)

	for root, dir, files in os.walk(globals.DIR_CORE_CODE):
		if root == globals.DIR_CORE_CODE:
			continue
		
		for f in files:
			if not f.endswith(('.cpp')):
				continue
			
			name = root.replace(globals.DIR_CORE_CODE, '')[1:]
			name = name.replace('\\', '+').replace('/', '+') + '-'
			name += f.replace('.cpp', '.o')

			compiler.execute(\
				f"{profile['compiler']} {profile['c_std']} -o {dest_folder}/{name} " +\
				f"-c {root}/{f} {profile['includes']} {profile['macros']}", play_sound=compiler.IF_FAIL
			)

def build_wings(profile, wings = {}):
	if wings == {}:
		wings = globals.WINGS_DATA
	
	for ext in globals.EXTERNALS_DATA.values():
		profile['libs'] = ext['libs'] + profile['libs']
		profile['includes'] = profile['includes'] + ext['includes']

	if os.path.exists(globals.DIR_PC_LIBS):
		profile['libs'] = \
			[ f.replace('lib', '').replace('.a', '')\
			for f in os.listdir(globals.DIR_PC_LIBS)\
			if os.path.isfile(f"{globals.DIR_PC_LIBS}/{f}") ]\
			+ profile['libs']

	profile = globals.to_compiler_format(profile)
	
	pch.pre_compile_headers(profile)
	for name in wings:
		dest_folder = f"{globals.DIR_PC_OBJS}/Wings/{name}"
		if not os.path.exists(dest_folder):
			os.makedirs(dest_folder)
		
		start_dir = wings[name]['src_path']
		print(f"Compiling files in {name} to object files")
		for root, dir, files in os.walk(start_dir):
			for f in files:
				if not f.endswith('.cpp'):
					continue
				
				name = root.replace(start_dir, '')
				name = name.replace('\\', '+').replace('/', '+')
				if len(name) > 0:
					name += '-'
				name += f.replace('.cpp', '')

				compiler.execute(
					f"{profile['compiler']} {profile['c_std']} -o {dest_folder}/{name}.o " +\
					f"-c {root}/{f} {profile['includes']} {profile['macros']}"
				)
		

		output = f"{globals.DIR_BUILDS}/Wings"
		if not os.path.exists(output):
			os.makedirs(output)
		
		output += f"/{name}.{globals.EXT_WINGS}"
		print(f"Compiling Wing {name}")
		compiler.execute(
			f"{profile['compiler']} {profile['c_std']} -fPIC -shared -o {output} " +\
			f"{dest_folder}/*.o {globals.DIR_PC_OBJS}/Wings/Core/*.o " +\
			f"-L{globals.DIR_PC_LIBS} {profile['libs']}", play_sound=compiler.IF_FAIL
		)
		print("Finished")
