import os
import workbench
import state
from pynput import keyboard


def run():
	state.ALL_COMMANDS = [ cls() for cls in commands.command.Command.__subclasses__() ]

	while True:
		command = input(f"$ {'/' if state.CURRENT_DIR == '' else state.CURRENT_DIR}: ")
		args = [ a for a in command.split(' ') if a != '' ]

		if len(args) != 0:
			for cmd in state.ALL_COMMANDS:
				if cmd.compare(args[0]):
					cmd.execute(args)
					break
			else:
				print(f"  Command '{args[0]}' not found")

def on_press(key):
	if key == keyboard.Key.esc:
		return False

def on_esc():
	print('Esc')
	exit()

def on_ctrl_c():
	print(2)
	return False

if __name__ == '__main__':
	with keyboard.GlobalHotKeys({
		'<esc>': on_esc, '<ctrl>+c': on_ctrl_c
	}) as h:
		with keyboard.Listener(on_press = on_press) as l:
			# run()
			l.join()


	pass
	# l.join()
