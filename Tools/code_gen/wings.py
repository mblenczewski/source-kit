import os
import globals

DIR_WING_LINKS = globals.DIR_TEMPLATES + '/Wings/Links/'
DIR_OUT = globals.DIR_CORE_CODE + '/Wings/'

WING_FUNCS = 'wing_funcs.h'
WING_BEATER_H ='wing_beater.h'
WING_BEATER_CPP = 'wing_beater.cpp'


def generate_from_template(template_dest: str, output_dest: str):
	output = open(output_dest, 'w')

	with open(template_dest) as template:
		# 0 - relay lines from template to output
		# 1 - copying the generation lines
		# 2 - formating and exporting the gen lines into the output
		generate_mode = 0
		string_to_format = ""

		for line in template:
			if len(line) > 1:
				if line[:2] == '$#':
					continue
				elif line[0:2] == '$$':
					generate_mode = 1
					string_to_format = ""
					continue
				elif line.lower()[0:2] == '$x':
					generate_mode = 2
			
			if generate_mode == 1:
				string_to_format += line
			elif generate_mode == 2:
				for link in globals.WING_LINKS:
					output.write(string_to_format.format(link_name = link['name'], ret = link['ret']))
			
				generate_mode = 0
			else:
				output.write(line)

	output.close()

def gen_wing_funcs():
	generate_from_template(DIR_WING_LINKS + WING_FUNCS, DIR_OUT + WING_FUNCS)

def gen_wing_beater():
	generate_from_template(DIR_WING_LINKS + WING_BEATER_H, DIR_OUT + WING_BEATER_H)
	generate_from_template(DIR_WING_LINKS + WING_BEATER_CPP, DIR_OUT + WING_BEATER_CPP)
