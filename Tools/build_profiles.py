
def combine_build_profiles(profiles: list) -> dict:
	result = {}
	all_keys = []
	[all_keys.extend(profile.keys()) for profile in profiles]
	all_keys = set(all_keys)
	
	for key in all_keys:
		is_list = any([type(profile[key]) is list for profile in profiles if key in profile])

		if is_list:
			all = []
			[ all.extend(profile[key] if type(profile[key]) is list else [ profile[key] ]) for profile in profiles if key in profile ] 
			result[key] = list(dict.fromkeys(all))
		else:
			for profile in profiles:
				if key in profile:
					result[key] = profile[key]

	return result