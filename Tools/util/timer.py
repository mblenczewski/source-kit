import time

class Timer:
	start = 0.0
	elapsed_tiem = 0.0

	def start(self):
		self.start = time.perf_counter()

	def stop(self):
		self.elapsed_time = time.perf_counter() - self.start
		self.start = 0.0

	def print(self, precision = 4):
		print(f"Total Elapsed Time {self.elapsed_time:0.{precision}f}")

