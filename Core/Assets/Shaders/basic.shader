#shader vertex
#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;

uniform mat4 uMVP;

out vec3 vPosition;
out vec2 vTexCoord;

void main()
{
	vPosition = aPosition;
	vTexCoord = aTexCoord;

	gl_Position = uMVP * vec4(aPosition, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 colour;

in vec3 vPosition;
in vec2 vTexCoord;

uniform sampler2D tex;

void main()
{
	// colour = vTexCoord;
	// colour = vec4(vPosition * 0.5 + 0.5, 1.0) * vec4(vColour.zx, 0, 1.0);
	// colour = vec4(texture(tex, vTexCoord).xyz, 1.0);
	colour = texture(tex, vTexCoord);
}
