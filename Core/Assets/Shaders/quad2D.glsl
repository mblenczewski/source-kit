#shader vertex
#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;

uniform mat4 uViewProjection;
uniform mat4 uModel;

out vec3 vPosition;
out vec2 vTexCoord;

void main()
{
	vPosition = aPosition;
	vTexCoord = aTexCoord;
	
	gl_Position = uViewProjection * uModel * vec4(aPosition, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 colour;

in vec3 vPosition;
in vec2 vTexCoord;

uniform vec4 uColour;
uniform float uTiling;
uniform sampler2D uTexture;

void main()
{
	colour = texture(uTexture, vTexCoord * uTiling) * uColour;
	// colour = vec4(vTexCoord, 0.0, 1.0);
}
