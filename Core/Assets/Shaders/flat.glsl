#shader vertex
#version 330 core

layout(location = 0) in vec3 aPosition;

uniform mat4 uViewProjection;
uniform mat4 uModel;

out vec3 vPosition;

void main()
{
	vPosition = aPosition;
	
	gl_Position = uViewProjection * uModel * vec4(aPosition, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 colour;

in vec3 vPosition;

uniform vec4 uColour;

void main()
{
	colour = uColour;
}
