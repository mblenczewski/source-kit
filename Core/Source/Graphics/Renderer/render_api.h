#pragma once

#include <glm/glm.hpp>
#include "core"
#include "debug"

#include "Graphics/Data/vertex_array.h"
#include "Util/singleton.h"

enum class RenderAPIType { None = -1, OpenGL = 0 };

class MxnRenderAPI
{
public:
	virtual void Init() = 0;

	virtual void SetClearColour(const glm::vec4& colour) = 0;
	virtual void SetViewport(unsigned int x, unsigned int y, unsigned int width, unsigned int height) = 0;

	virtual void DrawIndexed(const ref<MxnVertexArray>& vertexArray) = 0;
	inline RenderAPIType GetAPI() const { return _API; }
	
	virtual void Clear() = 0;

#ifdef BLD_DEBUG
	virtual void CheckForErrors() const = 0;
#endif

protected:
	MxnRenderAPI(RenderAPIType apiType) : _API(apiType) {}
	RenderAPIType _API;
};

class RenderAPI : public MxnSingleton<MxnRenderAPI>
{
public:
	static void Init(MxnRenderAPI *ref) { new RenderAPI(ref); Get()->Init(); }

	static inline void SetClearColour(const glm::vec4& colour) { Get()->SetClearColour(colour); }
	static inline void SetViewport(unsigned int x, unsigned int y, unsigned int width, unsigned int height) 
		{ Get()->SetViewport(x, y, width, height); }

	static inline void DrawIndexed(const ref<MxnVertexArray>& vertexArray) { Get()->DrawIndexed(vertexArray); }
	static inline RenderAPIType GetAPI() { return Get()->GetAPI(); }
	
	static inline void Clear() { Get()->Clear(); }

#ifdef BLD_DEBUG
	static inline void CheckForErrors() { Get()->CheckForErrors(); }
#endif

private:
	RenderAPI(MxnRenderAPI *ref) : MxnSingleton<MxnRenderAPI>(ref) {}
};
