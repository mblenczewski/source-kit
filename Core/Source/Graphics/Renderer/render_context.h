#pragma once

class MxnRenderContext
{
public:
	virtual void Init() = 0;
	virtual void SwapBuffers() = 0;

protected:
	MxnRenderContext() {}
};
