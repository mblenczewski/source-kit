#include "pch.h"
#include "renderer.h"
#include "debug"

#include "renderer2d.h"
#include "Platform/OpenGL/ogl_render_api.h"

Renderer::~Renderer()
{
	PRF_FN;
	Renderer2D::Shutdown();
}

void Renderer::Init()
{
	PRF_FN;
	new Renderer;
	RenderAPI::Init(new OGLRenderAPI);
	Renderer2D::Init();
}

void Renderer::BeginScene(const ref<OrthographicCamera>& camera, const ref<Transform>& transform)
{
	Get()->_currViewProjection = camera->GetMatProjection() * ModelMat4(*transform);
}

void Renderer::EndScene() {}


void Renderer::SetWindowSize(unsigned int width, unsigned int height)
{
	RenderAPI::SetViewport(0, 0, width, height);
}

void Renderer::Submit(ref<MxnShader>& shader, const ref<MxnVertexArray>& vertexArray, const Transform& transform)
{
	glm::mat4 mvp = Get()->_currViewProjection * ModelMat4(transform);
	shader->Bind();
	shader->UploadMat4("uMVP", mvp);

	RenderAPI::DrawIndexed(vertexArray);
}
