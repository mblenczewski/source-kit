#pragma once

#include "core"
#include "render_api.h"
#include "Components/camera.h"
#include "Util/singleton.h"
#include "Graphics/Data/shader.h"
#include "Components/transform.h"


class Renderer : public MxnSingleton<Renderer>
{
public:
	~Renderer();

	static void BeginScene(const ref<OrthographicCamera>& camera, const ref<Transform>& transform);
	static void EndScene();

	static void Submit(ref<MxnShader>& shader, const ref<MxnVertexArray>& vertexArray, const Transform& transform);
	static void SetWindowSize(unsigned int width, unsigned int height);

	inline static RenderAPIType GetAPI() { return RenderAPI::GetAPI(); }

	static void Init();

private:
	Renderer() : _currViewProjection(1.f), MxnSingleton<Renderer>(this) {}

	glm::mat4 _currViewProjection;
};
