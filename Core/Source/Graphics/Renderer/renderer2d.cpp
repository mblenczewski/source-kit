#include "pch.h"
#include "renderer2d.h"

#include "render_api.h"
#include "Assets/a.h"
#include "Graphics/Data/vertex_array.h"
#include "Graphics/Data/buffers.h"
#include "Graphics/Data/shader.h"
#include "Graphics/Data/texture.h"

struct Render2DData
{
	ref<MxnVertexArray> vaQuad;
	ref<MxnShader> shd2D;

	ref<MxnTexture> texBlank;
};

static Render2DData *s_Data;

void Renderer2D::Init()
{
	s_Data = new Render2DData;

	s_Data->vaQuad = ref<MxnVertexArray>(CreateVertexArray());
	s_Data->shd2D = ref<MxnShader>(LoadShader(AssetPath(Shaders/quad2D.glsl), "Composite 2D"));
	s_Data->shd2D->Bind();
	s_Data->shd2D->UploadInt("uTexture", 0);

	float vertices[4 * 5] = {
		-0.5f, -0.5f, 0.0f,   0.0f, 0.0f,
		-0.5f,  0.5f, 0.0f,   0.0f, 1.0f,
		 0.5f,  0.5f, 0.0f,   1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f,   1.0f, 0.0f
	};

	ref<MxnBfrVertex> vertex(CreateBfrVertex(vertices, sizeof(vertices)));
	vertex->SetBufferLayout({ { ShaderDataType::Float3, "aPosition" }, { ShaderDataType::Float2, "aTexCoord" } });

	unsigned int indicies[3 * 2] = { 0, 1, 2,    0, 2, 3 };
	ref<MxnBfrIndex> index(CreateBfrIndex(indicies, 3 * 2));

	s_Data->vaQuad->AddVertexBuffer(vertex);
	s_Data->vaQuad->SetIndexBuffer(index);

	s_Data->texBlank = ref<MxnTexture>(CreateTex2D(1, 1));
	
	unsigned int colourData = 0xffffffff;
	s_Data->texBlank->SetTexData(&colourData, sizeof(unsigned int));
}

void Renderer2D::Shutdown()
{
	delete s_Data;
}


static glm::mat4 identity(1.0f);

void Renderer2D::BeginScene(const ref<OrthographicCamera>& camera, const ref<Transform2D>& transform)
{
	// Scale then Rotate then Translate
	glm::mat4 model = glm::translate(identity, { transform->position.x, transform->position.y, transform->z }) * transform->GetMatRotation();

	s_Data->shd2D->Bind();
	s_Data->shd2D->UploadMat4("uViewProjection", camera->GetMatProjection() * model);
}

void Renderer2D::EndScene() { }

void Renderer2D::DrawQuad(const ref<Transform2D>& transform, const RenderProps& properties)
{
	static glm::mat4 identity(1.0f);
	// Scale then Rotate then Translate
	glm::mat4 model = glm::translate(identity, { transform->position.x, transform->position.y, transform->z }) * 
		transform->GetMatRotation() * 
		glm::scale(identity, { transform->size.x, transform->size.y, 1.0f });
	
	s_Data->shd2D->Bind();
	s_Data->shd2D->UploadMat4("uModel", model);
	s_Data->shd2D->UploadFloat4("uColour", properties.colour);
	s_Data->shd2D->UploadFloat("uTiling", properties.tilingFactor);

	const ref<MxnTexture>& tex = properties.texture ? properties.texture : s_Data->texBlank;
	tex->Bind();

	RenderAPI::DrawIndexed(s_Data->vaQuad);
}


// void Renderer2D::DrawFlatQuad(const glm::vec2& position, const glm::vec2& size, const glm::vec4& colour)
// {
// 	DrawFlatQuad({ position.x, position.y, 0.f }, size, colour);
// }

// void Renderer2D::DrawFlatQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& colour)
// {
// 	s_Data->shd2D->Bind();
// 	s_Data->shd2D->UploadMat4("uModel", glm::mat4(1.0f));
// 	s_Data->shd2D->UploadFloat4("uColour", colour);

// 	s_Data->texBlank->Bind();

// 	RenderAPI::DrawIndexed(s_Data->vaQuad);
// }

// void Renderer2D::DrawTexQuad(const glm::vec2& position, const glm::vec2& size, const ref<MxnTexture>& texture, const glm::vec4& colour)
// {
// 	DrawTexQuad({ position.x, position.y, 0.f }, size, texture, colour);
// }

// void Renderer2D::DrawTexQuad(const glm::vec3& position, const glm::vec2& size, const ref<MxnTexture>& texture, const glm::vec4& colour)
// {
// 	s_Data->shd2D->Bind();

// 	glm::mat4 model = glm::translate(glm::mat4(1.0f), position) * glm::scale(glm::mat4(1.0f), { size.x, size.y, 1.0f });
// 	s_Data->shd2D->UploadMat4("uModel", model);
// 	s_Data->shd2D->UploadFloat4("uColour", colour);

// 	texture->Bind();

// 	RenderAPI::DrawIndexed(s_Data->vaQuad);
// }
