#pragma once

#include <glm/glm.hpp>
#include "core"

#include "Components/transform_2d.h"
#include "Components/camera.h"

#include "Graphics/Data/texture.h"

struct RenderProps
{
	// RenderProps() : texture(nullptr), colour(1.0f), tilingFactor(1.0f) {}

	ref<MxnTexture> texture;
	glm::vec4 colour = glm::vec4(1.0f);
	float tilingFactor = 1.0f;
};

class Renderer2D
{
public:
	static void Init();
	static void Shutdown();

	static void BeginScene(const ref<OrthographicCamera>& camera, const ref<Transform2D>& transform);
	static void EndScene();

	static void DrawQuad(const ref<Transform2D>& transform, const RenderProps& properties);

	// static void DrawFlatQuad(const glm::vec2& position, const glm::vec2& size, const glm::vec4& colour = glm::vec4(1.0f));
	// static void DrawFlatQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& colour = glm::vec4(1.0f));

	// static void DrawTexQuad(const glm::vec2& position, const glm::vec2& size, const ref<MxnTexture>& texture, const glm::vec4& colour = glm::vec4(1.0f));
	// static void DrawTexQuad(const glm::vec3& position, const glm::vec2& size, const ref<MxnTexture>& texture, const glm::vec4& colour = glm::vec4(1.0f));
};
