#pragma once
#include "core"
#include "buffers.h"

class MxnVertexArray
{
public:
	virtual ~MxnVertexArray() {}

	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;

	virtual void AddVertexBuffer(const ref<MxnBfrVertex>& bfrVertex) = 0;
	virtual const std::vector<ref<MxnBfrVertex>>& GetVertexBuffers() const = 0;

	virtual void SetIndexBuffer(const ref<MxnBfrIndex>& bfrIndex) = 0;
	virtual const ref<MxnBfrIndex>& GetIndexBuffer() const = 0;
};

MxnVertexArray* CreateVertexArray();
