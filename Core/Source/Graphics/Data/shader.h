#pragma once

#include "core"
#include "math"

// To be made into a mixin and have one made for each render API
class MxnShader
{
public:
	virtual ~MxnShader() = default;

	virtual const std::string& GetName() const = 0;
	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;
	
	virtual void UploadInt(const char *uniform, int value) = 0;

	virtual void UploadFloat(const char *uniform, float value) = 0;
	virtual void UploadFloat2(const char *uniform, const glm::vec2& value) = 0;
	virtual void UploadFloat3(const char *uniform, const glm::vec3& value) = 0;
	virtual void UploadFloat4(const char *uniform, const glm::vec4& value) = 0;
	
	virtual void UploadMat4(const char *uniform, const glm::mat4& matrix) = 0;
	virtual void UploadMat3(const char *uniform, const glm::mat3& matrix) = 0;
};

MxnShader* LoadShader(const fs::path& filepath, std::string name = "");
MxnShader* LoadShader(const std::string& srcVertex, const std::string& srcPixel, std::string name = "");

class LibShader
{
public:
	void Add(const std::string& name, const ref<MxnShader>& shader);

	ref<MxnShader> Load(const fs::path& shaderPath, std::string name = "");
	ref<MxnShader> Load(const std::string& srcVertex, const std::string& srcPixel, std::string name = "");

	ref<MxnShader>& Get(const std::string& name);
	bool Exists(const std::string& name) const;

private:
	std::unordered_map<std::string, ref<MxnShader>> _shaders;
};
