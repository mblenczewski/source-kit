#include "pch.h"
#include "shader.h"

#include "debug"

void LibShader::Add(const std::string& name, const ref<MxnShader>& shader)
{
	ASSERT(!Exists(name), "Tried to add shader " + name + " to library it is already in");
	_shaders.emplace(name, shader);
}

ref<MxnShader> LibShader::Load(const fs::path& filepath, std::string name)
{
	ASSERT(!Exists(name), "Tried to load shader " + name + " to library it is already in");
	ref<MxnShader> shader(LoadShader(filepath, name));
	_shaders.emplace(name, shader);
	return shader;
}

ref<MxnShader> LibShader::Load(const std::string& srcVertex, const std::string& srcPixel, std::string name)
{
	ASSERT(!Exists(name), "Tried to load shader " + name + " to library it is already in");
	ref<MxnShader> shader(LoadShader(srcVertex, srcPixel, name));
	_shaders.emplace(name, shader);
	return shader;
}

ref<MxnShader>& LibShader::Get(const std::string& name)
{
	ASSERT(Exists(name), "Tried to load shader " + name + " to library it is already in");
	return _shaders[name];
}

bool LibShader::Exists(const std::string& name) const
{
	return _shaders.find(name) != _shaders.end();
}
