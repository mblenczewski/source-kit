#pragma once

#include <glad/glad.h>
#include "core"
#include "debug"

enum class ShaderDataType { None = 0, Bool, Float1, Float2, Float3, Float4, Int1, Int2, Int3, Int4, Mat3x3, Mat4x4 };

inline unsigned int ShaderDataToSize(ShaderDataType type)
{
	switch (type)
	{
		case ShaderDataType::Bool:      return 1;
		case ShaderDataType::Float1:    return sizeof(float) * 1;
		case ShaderDataType::Float2:    return sizeof(float) * 2;
		case ShaderDataType::Float3:    return sizeof(float) * 3;
		case ShaderDataType::Float4:    return sizeof(float) * 4;
		case ShaderDataType::Int1:      return sizeof(int) * 1;
		case ShaderDataType::Int2:      return sizeof(int) * 2;
		case ShaderDataType::Int3:      return sizeof(int) * 3;
		case ShaderDataType::Int4:      return sizeof(int) * 4;
		case ShaderDataType::Mat3x3:    return sizeof(float) * 3 * 3;
		case ShaderDataType::Mat4x4:    return sizeof(float) * 4 * 4;
	};

	ASSERT(false, "Unknown ShaderDataType");
	return 0;
}

struct BfrElement
{
	const char* name;
	ShaderDataType type;
	unsigned int size;
	unsigned int offset;
	bool normalised;

	BfrElement() : name(""), type(ShaderDataType::None), size(0), offset(0), normalised(false)
	{} 

	BfrElement(ShaderDataType type, const char* name, bool normalised = false)
		: name(name), type(type), size(ShaderDataToSize(type)), offset(0), normalised(normalised)
		{}
	
	unsigned int GetComponentCount() const
	{
		switch (type)
		{
			case ShaderDataType::Bool:      return 1;
			case ShaderDataType::Float1:    return 1;
			case ShaderDataType::Float2:    return 2;
			case ShaderDataType::Float3:    return 3;
			case ShaderDataType::Float4:    return 4;
			case ShaderDataType::Int1:      return 1;
			case ShaderDataType::Int2:      return 2;
			case ShaderDataType::Int3:      return 3;
			case ShaderDataType::Int4:      return 4;
			case ShaderDataType::Mat3x3:    return 3 * 3;
			case ShaderDataType::Mat4x4:    return 4 * 4;
		};

		ASSERT(false, "Unknown ShaderDataType");
		return 0;
	}
};

class BfrLayout
{
public:
	BfrLayout() : BfrLayout({}) { }

	BfrLayout(std::initializer_list<BfrElement> list) : _elements(list), _stride(0)
	{
		unsigned int offset = 0;
		for (auto& element : _elements)
		{
			element.offset = offset;
			offset += element.size;
			_stride += element.size;
		}
	}

	inline const std::vector<BfrElement>& GetElements() const { return _elements; }
	inline unsigned int GetStride() const { return _stride; }

	std::vector<BfrElement>::iterator begin() { return _elements.begin(); }
	std::vector<BfrElement>::iterator end() { return _elements.end(); }
	std::vector<BfrElement>::const_iterator begin() const { return _elements.begin(); }
	std::vector<BfrElement>::const_iterator end() const { return _elements.end(); }
private:
	std::vector<BfrElement> _elements;
	unsigned int _stride;
};

class MxnBfrVertex
{
public:
	virtual ~MxnBfrVertex() {}
	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;
	virtual void SetBufferLayout(const BfrLayout& layout) = 0;
	virtual const BfrLayout& GetLayout() const = 0;
};

class MxnBfrIndex
{
public:
	virtual ~MxnBfrIndex() {}
	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;
	virtual unsigned int GetCount() const = 0;
};

MxnBfrVertex* CreateBfrVertex(float verticies[], unsigned int size);
MxnBfrIndex*  CreateBfrIndex (unsigned int indicies[], unsigned int count); 
