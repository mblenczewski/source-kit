#pragma once

#include "core"
#include "math"

class MxnTexture
{
public:
	virtual ~MxnTexture() {}

	virtual void Bind(unsigned int slot = 0) const = 0;

	virtual void SetTexData(void *data, unsigned int size) = 0;

	virtual glm::uvec2 GetDimensions() const = 0;
};

MxnTexture* CreateTex2D(unsigned int width, unsigned int height);
MxnTexture* CreateTex2D(const fs::path& path);
