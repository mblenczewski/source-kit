#pragma once

#include "layers"

class ImGUILayer : public MxnLayer
{
public:
	ImGUILayer();
	~ImGUILayer();

	void OnAttach() override;
	void OnDetach() override;
	void OnImGUIRender() override;

	void Begin();
	void End();
	// void OnEvent(MxnEvent& event) override;

	// bool OnWindowClose(EvtWindowClose& event);
	// bool OnWindowLostFocus(EvtWindowLostFocus& event);
	// bool OnWindowResize(EvtWindowResize& event);
	// bool OnWindowMoved(EvtWindowMoved& event);

	// bool OnKeyPressed(EvtKeyPressed& event);
	// bool OnKeyReleased(EvtKeyReleased& event);
	// bool OnKeyTyped(EvtKeyTyped& event);

	// bool OnMouseMoved(EvtMouseMoved& event);
	// bool OnMouseScroll(EvtMouseScroll& event);
	// bool OnMouseButtonPressed(EvtMouseButtonPressed& event);
	// bool OnMouseButtonReleased(EvtMouseButtonReleased& event);

private:
	float _time = 0.0f;
};
