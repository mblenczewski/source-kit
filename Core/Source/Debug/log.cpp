#include "pch.h"
#include "log.h"
#include <spdlog/sinks/stdout_color_sinks.h>

#include "core"

#ifdef BLD_DEBUG
	#define LG_PATTERN "[%T] [%g:%#] %! || %^%v%$"
#else
	// This shouldn't be used where ever BLD_DEBUG isn't defined, but just in case
	#define LG_PATTERN
#endif

spdlog::logger* Log::sGeneralLogger;

void Log::Init()
{
	spdlog::set_level(spdlog::level::trace);
#ifdef BLD_WING
	auto general = spdlog::get("GENERAL");
	if (!general)
	{
		general = spdlog::stdout_color_mt("GENERAL");
		general->set_pattern(LG_PATTERN);
	}
	// spdlog::register_logger(general);
	sGeneralLogger = general.get();
#else
	sGeneralLogger = spdlog::stdout_color_mt("GENERAL").get();
	sGeneralLogger->set_pattern(LG_PATTERN);
	// spdlog::register_logger(sGeneralLogger);
	
	// sGeneralLogger->info("Setup Logger");
#endif
}

void Log::Shutdown()
{
	sGeneralLogger->info("Logging Over");
}
