#include "pch.h"
#include "profiling.h"

#include "debug"


static unsigned int  g_profileCount = 0;
static std::string   g_currentSessionName;
static std::ofstream g_outputStream;
static std::mutex    g_lock;

void Instrumentor::BeginSession(const std::string& sessionName, const std::string& filepath)
{
	if (!g_currentSessionName.empty())
	{
		LG_WRN("Tried to start a profiling seesion before the session {} had ended", g_currentSessionName);
		return;
	}

	g_currentSessionName = sessionName;
	g_outputStream.open(filepath);
	WriteHeader();
}

void Instrumentor::EndSession()
{
	WriteFooter();
	g_outputStream.close();
	g_currentSessionName.clear();
	g_profileCount = 0;
}

void Instrumentor::WriteProfile(const ProfileResults& result)
{
	std::lock_guard<std::mutex> lock(g_lock);

	std::ofstream& out = g_outputStream;
	// LG_INF("{}", g_profileCount);
	if (g_profileCount++ > 0)
		out << ",";
	
	std::string name = result.name;
	std::replace(name.begin(), name.end(), '"', '\'');

	out << "{";
	out << "\"cat\":\"function\",";
	out << "\"dur\":" << (result.end - result.start) << ',';
	out << "\"name\":\"" << name << "\",";
	out << "\"ph\":\"X\",";
	out << "\"pid\":0,";
	out << "\"tid\":" << result.threadID << ",";
	out << "\"ts\":" << result.start;
	out << "}";

	// out.flush();
}

void Instrumentor::WriteHeader()
{
	g_outputStream << "{\"otherData\": {},\"traceEvents\":[";
	// g_outputStream.flush();
}

void Instrumentor::WriteFooter()
{
	g_outputStream << "]}";
	// g_outputStream.flush();
}
