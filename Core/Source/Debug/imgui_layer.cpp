#include "pch.h"
#include "imgui_layer.h"

// Temporary
#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include "core"
#include "app"
#include "input"
#include "debug"

#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_opengl3.h>


#ifdef BLD_DEBUG
	#define LAYER_NAME "IMGUI Layer"
#else
	#define LAYER_NAME
#endif

ImGUILayer::ImGUILayer() : MxnLayer(LAYER_NAME) { }

ImGUILayer::~ImGUILayer() { }

void ImGUILayer::OnAttach()
{
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	ImGuiIO& io = ImGui::GetIO(); (void)io;
	// io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	// io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
	// io.ConfigViewportsNoAutoMerge = true;
	// io.ConfigViewportsNoTaskBarIcon = true;

	ImGui::StyleColorsDark();
	// ImGui::StyleColorsClassic();

	// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
	ImGuiStyle& style = ImGui::GetStyle();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
			style.WindowRounding = 0.0f;
			style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	}
	
	ImGui_ImplGlfw_InitForOpenGL(static_cast<GLFWwindow*>(App::GetWindow().GetNativeWindow()), true);
	ImGui_ImplOpenGL3_Init("#version 410");
}

void ImGUILayer::OnDetach()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

void ImGUILayer::OnImGUIRender()
{
	// static bool show = true;
	// ImGui::ShowDemoWindow(&show);
}

void ImGUILayer::Begin()
{
	MxnWindow& window = App::GetWindow();
	ImGui::GetIO().DisplaySize = ImVec2((float)window.GetWidth(), (float)window.GetHeight());

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void ImGUILayer::End()
{
	// Rendering
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		GLFWwindow* backup_current_context = glfwGetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		glfwMakeContextCurrent(backup_current_context);
	}
}

// void ImGUILayer::OnEvent(MxnEvent& event)
// {
// 	EventDispatcher dispatcher(event);
// 	dispatcher.Dispatch<EvtWindowClose>(BIND_EVT_MFN(OnWindowClose));
// 	dispatcher.Dispatch<EvtWindowLostFocus>(BIND_EVT_MFN(OnWindowLostFocus));
// 	dispatcher.Dispatch<EvtWindowResize>(BIND_EVT_MFN(OnWindowResize));
// 	dispatcher.Dispatch<EvtWindowMoved>(BIND_EVT_MFN(OnWindowMoved));

// 	dispatcher.Dispatch<EvtKeyPressed>(BIND_EVT_MFN(OnKeyPressed));
// 	dispatcher.Dispatch<EvtKeyReleased>(BIND_EVT_MFN(OnKeyReleased));
// 	dispatcher.Dispatch<EvtKeyTyped>(BIND_EVT_MFN(OnKeyTyped));

// 	dispatcher.Dispatch<EvtMouseMoved>(BIND_EVT_MFN(OnMouseMoved));
// 	dispatcher.Dispatch<EvtMouseScroll>(BIND_EVT_MFN(OnMouseScroll));
// 	dispatcher.Dispatch<EvtMouseButtonPressed>(BIND_EVT_MFN(OnMouseButtonPressed));
// 	dispatcher.Dispatch<EvtMouseButtonReleased>(BIND_EVT_MFN(OnMouseButtonReleased));
// }

// bool ImGUILayer::OnWindowClose(EvtWindowClose& event)
// {
// 	return false;
// }

// bool ImGUILayer::OnWindowLostFocus(EvtWindowLostFocus& event)
// {
// 	return false;
// }

// bool ImGUILayer::OnWindowResize(EvtWindowResize& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.DisplaySize = ImVec2(event.GetWidth(), event.GetHeight());
// 	io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);
// 	glViewport(0, 0, event.GetWidth(), event.GetHeight());
// 	return false;
// }

// bool ImGUILayer::OnWindowMoved(EvtWindowMoved& event)
// {
// 	return false;
// }

// bool ImGUILayer::OnKeyPressed(EvtKeyPressed& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.KeysDown[event.GetKeyCode()] = true;
// 	io.KeyCtrl = io.KeysDown[KY_LCONTROL] || io.KeysDown[KY_RCONTROL];
// 	io.KeyShift = io.KeysDown[KY_LSHIFT] || io.KeysDown[KY_RSHIFT];
// 	io.KeyAlt = io.KeysDown[KY_L_ALT] || io.KeysDown[KY_R_ALT];
// 	io.KeySuper = io.KeysDown[KY_LSUPER] || io.KeysDown[KY_RSUPER];
// 	return false;
// }

// bool ImGUILayer::OnKeyReleased(EvtKeyReleased& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.KeysDown[event.GetKeyCode()] = false;
// 	io.KeyCtrl = io.KeysDown[KY_LCONTROL] || io.KeysDown[KY_RCONTROL];
// 	io.KeyCtrl = io.KeysDown[KY_LSHIFT] || io.KeysDown[KY_RSHIFT];
// 	io.KeyCtrl = io.KeysDown[KY_L_ALT] || io.KeysDown[KY_R_ALT];
// 	io.KeyCtrl = io.KeysDown[KY_LSUPER] || io.KeysDown[KY_RSUPER];
// 	return false;
// }

// bool ImGUILayer::OnKeyTyped(EvtKeyTyped& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.AddInputCharacter(event.GetKeyCode());
// 	return false;
// }

// bool ImGUILayer::OnMouseMoved(EvtMouseMoved& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.MousePos = ImVec2(event.GetPosition().x, event.GetPosition().y);
// 	return false;
// }

// bool ImGUILayer::OnMouseScroll(EvtMouseScroll& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.MouseWheel += event.GetScroll().y;
// 	io.MouseWheelH += event.GetScroll().x;
// 	return false;
// }

// bool ImGUILayer::OnMouseButtonPressed(EvtMouseButtonPressed& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.MouseDown[event.GetMouseButton()] = true;
// 	return false;
// }

// bool ImGUILayer::OnMouseButtonReleased(EvtMouseButtonReleased& event)
// {
// 	ImGuiIO& io = ImGui::GetIO();
// 	io.MouseDown[event.GetMouseButton()] = false;
// 	return false;
// }
