#pragma once

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

class Log
{
public:
	static void Init();
	static void Shutdown();

	inline static spdlog::logger* GetGeneral() { return sGeneralLogger; }

private:
	// TODO: Make multiple different loggers of different detail
	// Update, Detail, Time, etc
	static spdlog::logger *sGeneralLogger;
};
