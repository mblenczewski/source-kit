#pragma once

#include <chrono>
#include <thread>
#include "core"

namespace chr = std::chrono;

// {"pid":59904,"tid":6,"ts":9026289822,"ph":"E","cat":"v8","name":"V8.Execute","args":{"runtime-call-stats":{}},"tts":81317},
// {"pid":59904,"tid":6,"ts":9026323208,"ph":"B","cat":"v8","name":"V8.Execute","args":{},"tts":81443},
struct ProfileResults
{
	std::string name;
	unsigned long long threadID;
	long long start, end;
};

class Instrumentor
{
public:
	static void BeginSession(const std::string& sessionName, const std::string& filepath = "results.json");
	static void EndSession();
	static void WriteProfile(const ProfileResults& result);
	static void WriteHeader();
	static void WriteFooter();
};

struct InstrumentationTimer
{
	InstrumentationTimer(const char* name) 
		: _result(
			{ name, std::hash<std::thread::id>{}(std::this_thread::get_id()),
				chr::time_point_cast<chr::microseconds>(chr::high_resolution_clock::now()).time_since_epoch().count(), 0 }
		), _stopped(false) { }

	~InstrumentationTimer()
	{
		if (!_stopped) Stop();
	}

	void Stop()
	{
		_result.end = chr::time_point_cast<chr::microseconds>(chr::high_resolution_clock::now()).time_since_epoch().count();
		Instrumentor::WriteProfile(_result);
		_stopped = true;
	}

private:
	ProfileResults _result;
	bool _stopped;
};
