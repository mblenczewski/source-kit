#include "pch.h"
#include "core"
#include <glad/glad.h>

#define IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#define IMGUI_DISABLE_WIN32_FUNCTIONS
#include <backends/imgui_impl_opengl3.cpp>
#include <backends/imgui_impl_glfw.cpp>
