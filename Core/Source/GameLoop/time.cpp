#include "pch.h"
#include "time.h"

// TODO To be removed, perhaps move this functionality to platform
#include <GLFW/glfw3.h>

void Time::UpdateImpl()
{
	_lastFrameTime = _currentFrameTime;
	_currentFrameTime = (float)glfwGetTime();
	_deltaTime = _currentFrameTime - _lastFrameTime;
}
