#pragma once
#include "ctw"
#include "debug"
#include "util/singleton"

class Time : public MxnSingleton<Time>
{
public:
	inline static void Update() { Get()->UpdateImpl(); }

	inline static float GetDeltaTime() { return Get()->_deltaTime; }

	SiINIT(Time)

private:
	Time(IF_WING(Time* ref)) : MxnSingleton<Time>(IF_WING_ELSE(ref, this)) {}

	void UpdateImpl();

	float _deltaTime = 0.0f;

	float _currentFrameTime = 0.0f;
	float _lastFrameTime = 0.0f;
};
