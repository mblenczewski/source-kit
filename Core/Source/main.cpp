// Temporary
// #include <glad/glad.h>
// #include <GLFW/glfw3.h>
#include "pch.h"
#include <imgui.h>

#include "app"
#include "bridge"
#include "core"
#include "debug"
#include "wing_link"
#include "input"
#include "gameloop"

#include "Graphics/Data/shader.h"
#include "Graphics/Data/buffers.h"
#include "Graphics/Data/vertex_array.h"
#include "Graphics/Data/texture.h"

#include "Wings/wing_beater.h"
#include "Debug/imgui_layer.h"
#include "Assets/a.h"

#include "Graphics/Renderer/renderer.h"
#include "Graphics/Renderer/renderer2d.h"

#include "Components/transform.h"
#include "Components/camera.h"
#include "Components/cam_controller.h"
#include "Debug/profiling.h"

own<OrthographicCameraController> g_controller;

static bool OnWindowClose(EvtWindowClose e)
{
	App::SetRunning(false);
	return true;
}

static bool OnWindowResize(EvtWindowResize& e)
{
	if (e.GetSize().x == 0 || e.GetSize().y == 0)
	{
		App::SetMinimised(true);
		return false;
	}

	App::SetMinimised(false);
	Renderer::SetWindowSize(e.GetSize().x, e.GetSize().y);
	return false;
}

static void OnEvent(MxnEvent& e)
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<EvtWindowClose>(BIND_EVT_FN(OnWindowClose));
	dispatcher.Dispatch<EvtWindowResize>(BIND_EVT_FN(OnWindowResize));

	g_controller->OnEvent(e);
	for (auto it = App::GetLayerStack().end(); it != App::GetLayerStack().begin(); )
	{
		(*--it)->OnEvent(e);
		if (e.handled)
			break;
	}
}

void Startup();
void Runtime();
void Shutdown();

int main(void)
{
	PRF_SESSION_BEGIN("Startup", "00Output/profile_startup.json");
	Startup();
	PRF_SESSION_END;

	PRF_SESSION_BEGIN("Runtime", "00Output/profile_runtime.json");
	Runtime();
	PRF_SESSION_END;

	PRF_SESSION_BEGIN("Shutdown", "00Output/profile_shutdown.json");
	Shutdown();
	PRF_SESSION_END;
	
	return 1;
}

void Startup()
{
	PRF_FN;
	LG_INIT;

	LG_INF("Welcome to the Source Kit!");

	CTWBridgeDock();

	App::GetWindow().SetEventCallback(BIND_EVT_FN(OnEvent));
	
	WingBeater::GatherWings();
	WingBeater::CallStartup();

	// All data making this necessary should have been passed to the wings
	CTWBridge::Shutdown();

	Renderer::Init();
	RenderAPI::SetClearColour({ 0.1f, 0.05f, 0.15f, 1 });

	App::SetRunning(true);
}

static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

void Runtime()
{
	PRF_FN;

	ref<MxnTexture> tex(CreateTex2D(AssetPath(Textures/checkerboard.png)));
	ref<MxnTexture> tex2(CreateTex2D(AssetPath(Textures/logo_render.png)));

	ref<OrthographicCamera> camera(new OrthographicCamera(-1.6f,1.6f, -0.9f, 0.9f));
	ref<Transform2D> camTransform(new Transform2D);

	g_controller <= new OrthographicCameraController(camera, camTransform, 1280.0f / 720.f, 1.f);

	glm::vec4 colour(1.0f, 0.0f, 1.0f, 1.0f);

	ref<Transform2D> tFlat1(new Transform2D);
	RenderProps pFlat1{};
	pFlat1.colour = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);

	ref<Transform2D> tTex1(new Transform2D(glm::vec2(1.0f, -3.0f), glm::vec2(0.8f, 0.6f), glm::radians(30.f)));
	RenderProps pTex1{ tex2 };
	
	ref<Transform2D> tTex2(new Transform2D(glm::vec2(), glm::vec2(10.f, 10.f), 0, -0.5f));
	RenderProps pTex2{ tex, glm::vec4(1.0f, .6f, .6f, 1.0f), 10.0f };

	while (App::IsRunning())
	{
		PRF_SC("Run Loop");
		Time::Update();

		RenderAPI::Clear();
		MngInput::Update();

		g_controller->Update();
		{
			PRF_SC("Drawing");
			Renderer2D::BeginScene(camera, camTransform);

			Renderer2D::DrawQuad(tFlat1, pFlat1);
			Renderer2D::DrawQuad(tTex1, pTex1);
			Renderer2D::DrawQuad(tTex2, pTex2);

			// Renderer2D::DrawFlatQuad(glm::vec2(), glm::vec2(1.0f, 1.0f), colour);
			// Renderer2D::DrawTexQuad(glm::vec2(1.0f, -3.0f), glm::vec2(0.8f, .6f), tex2);
			// Renderer2D::DrawTexQuad(glm::vec3(0.0f, 0.0f, -0.5f), glm::vec2(10.0f), tex, glm::vec4(1.0f, .6f, .6f, 1.0f));

			Renderer2D::EndScene();
		}

		if (!App::IsMinimised())
		{
			PRF_SC("Layers Updater");
			for (auto layer : App::GetLayerStack())
				layer->OnUpdate();
		}

		// To go  to render thread
		{
			PRF_SC("ImGUI Update");
			App::GetImGUILayer().Begin();
			for (auto layer : App::GetLayerStack())
				layer->OnImGUIRender();
			
			ImGui::ColorEdit4("Flat Colour", &pFlat1.colour.r);
			
			App::GetImGUILayer().End();
		}

		{
			PRF_SC("Window Update");
			App::GetWindow().OnUpdate();
		}

		if (IsPressed(MngInput::GetKeyState(KY_ESC)))
			App::SetRunning(false);
	}
}

void Shutdown()
{
	PRF_FN;
	Renderer::Shutdown();

	WingBeater::CallShutdown();
	WingBeater::Shutdown();

	LG_SHUTDOWN;

	App::Shutdown();
	std::cout << "Fin\n";
}
