#include "pch.h"
#include "transform.h"
#include "transform_2d.h"

#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>

const static glm::mat4 identity(1.0f);

glm::mat4 ModelMat4(const Transform& transform)
{
	return ModelMat4(transform.position, transform.rotation, transform.scale);
}

glm::mat4 ModelMat4(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale)
{
	glm::mat4 transform = 
		glm::rotate(identity, glm::radians(rotation.x), glm::vec3(1, 0, 0)) *
		glm::rotate(identity, glm::radians(rotation.y), glm::vec3(0, 1, 0)) *
		glm::rotate(identity, glm::radians(rotation.z), glm::vec3(0, 0, 1)) *
		glm::translate(identity, position) *
		glm::scale(identity, 1.0f / scale);
	return glm::inverse(transform);
}

Transform2D::Transform2D() : position(0.f), z(0.f), size(1.f), rotation(0.f), matRotation(1.0f) {}

Transform2D::Transform2D(float z) : position(0.f), z(z), size(1.f), rotation(0.f), matRotation(1.0f) {}

Transform2D::Transform2D(const glm::vec2& position, const glm::vec2& size, float rotation, float z)
	: position(position), z(z), size(size), rotation(rotation), matRotation(1.0f)
{
	// to be replaced with an approximately function
	if (glm::abs(rotation) > 0.002f)
		RecalculateMatRotation();
}

void Transform2D::RecalculateMatRotation()
{
	static glm::vec3 axis(0, 0, 1);
	static glm::mat4 matRotXY = glm::rotate(identity, 0.f, glm::vec3(1, 0, 0)) * glm::rotate(identity, 0.f, glm::vec3(0, 1, 0));
	matRotation = matRotXY * glm::rotate(identity, rotation, axis);
}
