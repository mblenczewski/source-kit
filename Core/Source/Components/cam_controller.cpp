#include "pch.h"
#include "cam_controller.h"

#include "gameloop"
#include "input"

OrthographicCameraController::OrthographicCameraController(const ref<OrthographicCamera>& camera, const ref<Transform2D>& transform, float aspectRatio, float zoom)
	: _camera(camera), _transform(transform), _aspectRatio(aspectRatio), _zoom(zoom)
{
	_camera->SetProjection(-_zoom * _aspectRatio, _zoom * _aspectRatio, -_zoom, _zoom);
}

void OrthographicCameraController::Update()
{
	glm::vec2& pos = _transform->position;
	float& rot = _transform->rotation;

	if (IsPressed(MngInput::GetKeyState(KY_A)))
		pos.x -= _spdTranslation * Time::GetDeltaTime();
	else if (IsPressed(MngInput::GetKeyState(KY_D)))
		pos.x += _spdTranslation * Time::GetDeltaTime();
	
	if (IsPressed(MngInput::GetKeyState(KY_W)))
		pos.y += _spdTranslation * Time::GetDeltaTime();
	else if (IsPressed(MngInput::GetKeyState(KY_S)))
		pos.y -= _spdTranslation * Time::GetDeltaTime();
	
	if (IsPressed(MngInput::GetKeyState(KY_Q)))
		rot += _spdRotation * Time::GetDeltaTime();
	else if (IsPressed(MngInput::GetKeyState(KY_E)))
		rot -= _spdRotation * Time::GetDeltaTime();
}

void OrthographicCameraController::OnEvent(MxnEvent& e)
{
	EventDispatcher dispatcher(e);
	dispatcher.Dispatch<EvtMouseScroll>(BIND_EVT_MFN(OrthographicCameraController::OnMouseScroll));
	dispatcher.Dispatch<EvtWindowResize>(BIND_EVT_MFN(OrthographicCameraController::OnWindowResize));
}

bool OrthographicCameraController::OnMouseScroll(EvtMouseScroll& e)
{
	_zoom -= e.GetScroll().y * 0.25f;
	_zoom = std::max(_zoom, 0.25f);
	_camera->SetProjection(-_zoom * _aspectRatio, _zoom * _aspectRatio, -_zoom, _zoom);
	_spdTranslation = _zoom;
	return false;
}

bool OrthographicCameraController::OnWindowResize(EvtWindowResize& e)
{
	_aspectRatio = (float)e.GetSize().x / (float)e.GetSize().y;
	_camera->SetProjection(-_zoom * _aspectRatio, _zoom * _aspectRatio, -_zoom, _zoom);
	return false;
}