#pragma once

#include "events"
#include "camera.h"
#include "transform_2d.h"

class OrthographicCameraController
{
public:
	OrthographicCameraController(const ref<OrthographicCamera>& camera, const ref<Transform2D>& transform, float aspectRatio, float zoom = 1.0f);

	void Update();

	void OnEvent(MxnEvent& e);

private:
	bool OnMouseScroll(EvtMouseScroll& e);
	bool OnWindowResize(EvtWindowResize& e);

	ref<OrthographicCamera> _camera;
	ref<Transform2D> _transform;

	float _aspectRatio;
	float _zoom;
	float _spdTranslation = 5.0f, _spdRotation = 180.0f;
};
