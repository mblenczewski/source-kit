#include "pch.h"
#include "camera.h"


OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top)
	: _matProjection(glm::ortho(left, right, bottom, top, -1.0f, 1.0f))
{
	// _matViewProjection = _matProjection * _matView;
}

void OrthographicCamera::SetProjection(float left, float right, float bottom, float top)
{
	_matProjection = glm::ortho(left, right, bottom, top);
}

// void OrthographicCamera::RecalculateViewMatrix()
// {
// 	glm::mat4 transform =  glm::rotate(glm::mat4(1.0f), glm::radians(_rotation), glm::vec3(0, 0, 1)) * glm::translate(glm::mat4(1.0f), _position);

// 	// _matView = transform;
// 	_matView = glm::inverse(transform);
// 	_matViewProjection = _matProjection * _matView;
// }
