#pragma once

#include "math"

class Transform2D
{
public:
	Transform2D();
	Transform2D(float z);
	Transform2D(const glm::vec2& position, const glm::vec2& size = glm::vec2(0.f), float rotation = 0.f, float z = 0.f);

	const glm::mat4& GetMatRotation() const { return matRotation; }

	void RecalculateMatRotation();

	///// Not allowed to do this as glm::vec2 has a constructor
	// union
	// {
	// 	glm::vec3 position3D;
	// 	struct
	// 	{
			glm::vec2 position;
			float z;
	// 	};
	// };

	glm::vec2 size;
	float rotation; // radians

private:
	glm::mat4 matRotation;
};
