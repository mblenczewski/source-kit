#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "core"

class OrthographicCamera
{
public:
	OrthographicCamera(float left, float right, float bottom, float top);
	void SetProjection(float left, float right, float bottom, float top);

	inline const glm::mat4& GetMatProjection() const { return _matProjection; }

private:
	glm::mat4 _matProjection;
};
