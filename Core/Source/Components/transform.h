#pragma once

#include <glm/glm.hpp>

class Transform
{
public:
	Transform() : position(0.f), rotation(0.f), scale(1.f) {}

// 	inline void SetPosition(glm::vec3 position) { _position = position; }
// 	inline glm::vec3 GetPosition() const { return _position; }

// 	inline void SetRotation(glm::vec3 rotation) { _rotation = rotation; }
// 	inline glm::vec3 GetRotation() const { return _rotation; }

// 	inline void SetScale(glm::vec3 scale) { _scale = scale; }
// 	inline glm::vec3 GetScale() const { return _scale; }

// private:
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;
};

glm::mat4 ModelMat4(const Transform& transform);
glm::mat4 ModelMat4(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale);
