#pragma once

#include <fstream>
#include <iostream>
#include <functional>
#include <thread>
#include <algorithm>
#include <filesystem>
namespace fs = std::filesystem;

// Data structures
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#ifdef PLT_WINDOWS
	#include <windows.h>
#endif
