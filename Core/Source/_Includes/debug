#pragma once


#ifdef BLD_DEBUG
	#include "Debug/breakpoints.h"
	#include "Debug/log.h"

	#define LG_INIT Log::Init()
	#define LG_SHUTDOWN Log::Shutdown()

	#define LG_TRC(...)    SPDLOG_LOGGER_TRACE(Log::GetGeneral(), __VA_ARGS__)
	#define LG_DBG(...)    SPDLOG_LOGGER_DEBUG(Log::GetGeneral(), __VA_ARGS__)
	#define LG_INF(...)    SPDLOG_LOGGER_INFO(Log::GetGeneral(), __VA_ARGS__)
	#define LG_WRN(...)    SPDLOG_LOGGER_WARN(Log::GetGeneral(), __VA_ARGS__)
	#define LG_ERR(...)    SPDLOG_LOGGER_ERROR(Log::GetGeneral(), __VA_ARGS__)
	#define LG_CRT(...)    SPDLOG_LOGGER_CRITICAL(Log::GetGeneral(), __VA_ARGS__)

	#define ASSERT(condition, ...) { if(!(condition)) { LG_ERR("Assertion Failed: {0}", __VA_ARGS__); __dbgbreak(); } }

	#define IF_DEBUG(x) x
	#define IF_DEBUG_ELSE(x, y) x
	#define IFN_DEBUG(x)

	#ifdef NO_PROFILING
		#define PRF_SESSION_BEGIN(name, filepath)
		#define PRF_SESSION_END
		#define PRF_SC(name)
		#define PRF_FN
	#else
		#include "Debug/profiling.h"

		#define PRF_SESSION_BEGIN(name, filepath) Instrumentor::BeginSession(name, filepath)
		#define PRF_SESSION_END Instrumentor::EndSession()
		#define PRF_SC(name) InstrumentationTimer time_##__LINE__(name)
		#define PRF_FN InstrumentationTimer time_##__LINE__(__PRETTY_FUNCTION__)
	#endif
#else
	#define LG_INIT
	#define LG_SHUTDOWN

	#define LG_TRC(...)
	#define LG_DBG(...)
	#define LG_INF(...)
	#define LG_WRN(...)
	#define LG_ERR(...)
	#define LG_CRT(...)

	#define ASSERT(condition, ...)

	#define IF_DEBUG(x)
	#define IF_DEBUG_ELSE(x, y) y
	#define IFN_DEBUG(x) x
	
	#define PRF_SESSION_BEGIN(name, filepath)
	#define PRF_SESSION_END
	#define PRF_SC(name)
	#define PRF_FN
#endif
