#include "pch.h"
#include "Graphics/Data/texture.h"
#include "core"

#include "Graphics/Renderer/renderer.h"

// ###############   OPEN GL   ###############
#include "OpenGL/ogl_texture.h"

MxnTexture* CreateTex2D(unsigned int width, unsigned int height)
{
	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:
			return (MxnTexture*)new OGLTex2D(width, height);
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}

MxnTexture* CreateTex2D(const fs::path& path)
{
	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:
			return (MxnTexture*)new OGLTex2D(path);
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}
