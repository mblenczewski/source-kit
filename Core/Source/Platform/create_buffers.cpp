#include "pch.h"
#include "core"
#include "debug"

#include "Graphics/Renderer/renderer.h"
#include "Graphics/Data/buffers.h"
#include "Graphics/Data/vertex_array.h"

// ###############   OPEN GL   ###############
#include "OpenGL/ogl_buffers.h"
#include "OpenGL/ogl_vertex_array.h"

#ifdef PLT_WINDOWS
	// Windows specific buffers (DirectX)
#endif

MxnBfrVertex* CreateBfrVertex(float verticies[], unsigned int size)
{
	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:
			return (MxnBfrVertex*)new OGLVertexBuffer(verticies, size);
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}

MxnBfrIndex* CreateBfrIndex (unsigned int indicies[], unsigned int count)
{
	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:    
			return (MxnBfrIndex*)new OGLIndexBuffer(indicies, count);
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}

MxnVertexArray* CreateVertexArray()
{
	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:    
			return (MxnVertexArray*)new OGLVertexArray();
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}
