#include "pch.h"
#include "windows_window.h"

#include "debug"
#include "math"
#include "events"

#include "Platform/OpenGL/ogl_context.h"

// In this context, static means scoped to this file
static bool SGLFWInitialised = false;

static void GLFWErrorCallback(int error, const char *description)
{
	LG_ERR("GLFW Error ({0}): {1}", error, description);
}

MxnWindow* MxnWindow::Create(const WindowProps& props) { return new WindowsWindow(props); }

WindowsWindow::WindowsWindow(const WindowProps& props) { Init(props); }

WindowsWindow::~WindowsWindow() { Shutdown(); }

void WindowsWindow::Init(const WindowProps& props)
{
	_data.title = props.title;
	_data.width = props.width;
	_data.height = props.height;

	LG_INF("Creating window {0} ({1}, {2})", props.title, props.width, props.height);

	if (!SGLFWInitialised)
	{
		// TODO glfwTerminate on system shutdown
		int success = glfwInit(); // Termination here
		ASSERT(success, "Could not initialize GLFW");
		glfwSetErrorCallback(GLFWErrorCallback);
		SGLFWInitialised = true;
	}
	
	{
		PRF_SC("Creste Window");
		_window = glfwCreateWindow((int)props.width, (int)props.height, _data.title.c_str(), nullptr, nullptr);
	}

	{
		PRF_SC("Render Context Init");
		_renderContext = new OGLContext(_window);
		_renderContext->Init();
	}

	PRF_SC("Set Callbacks");
	glfwSetWindowUserPointer(_window, &_data);
	SetVSync(true);

	// Set GLFW callbacks
	glfwSetWindowSizeCallback(_window, [](GLFWwindow *window, int width, int height)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		data.width = width;
		data.height = height;

		EvtWindowResize e(width, height);
		data.callback(e);
	});

	glfwSetWindowCloseCallback(_window, [](GLFWwindow *window)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		EvtWindowClose e;
		data.callback(e);
	});

	glfwSetKeyCallback(_window, [](GLFWwindow *window, int key, int scancode, int action, int mods)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

		switch (action)
		{
			case GLFW_PRESS:
			{
				EvtKeyPressed e(key, 0);
				data.callback(e);
				break;
			}
			case GLFW_RELEASE:
			{
				EvtKeyReleased e(key);
				data.callback(e);
				break;
			}
			case GLFW_REPEAT:
			{
				EvtKeyPressed e(key, 1);
				data.callback(e);
				break;
			}
		}
	});

	glfwSetCharCallback(_window, [](GLFWwindow *window, unsigned int keycode)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		EvtKeyTyped e(keycode);
		data.callback(e);
	});

	glfwSetMouseButtonCallback(_window, [](GLFWwindow *window, int button, int action, int mods)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
		
		switch (action)
		{
			case GLFW_PRESS:
			{
				EvtMouseButtonPressed e(button);
				data.callback(e);
				break;
			}
			case GLFW_RELEASE:
			{
				EvtMouseButtonReleased e(button);
				data.callback(e);
				break;
			}
		}
	});

	glfwSetScrollCallback(_window, [](GLFWwindow *window, double xOffset, double yOffset)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

		EvtMouseScroll e(glm::vec2(xOffset, yOffset));
		data.callback(e);
	});

	glfwSetCursorPosCallback(_window, [](GLFWwindow *window, double xPos, double yPos)
	{
		WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

		EvtMouseMoved e(glm::vec2(xPos, yPos));
		data.callback(e);
	});
}

void WindowsWindow::Shutdown()
{
	glfwDestroyWindow(_window);
	delete _renderContext;
}

void WindowsWindow::OnUpdate()
{
	{
		PRF_SC("Poll Events");
		glfwPollEvents();
	}
	PRF_SC("Swap Buffers");
	_renderContext->SwapBuffers();
}

void WindowsWindow::SetVSync(bool enabled)
{
	glfwSwapInterval(enabled ? 1 : 0);
	_data.vSync = enabled;
}

bool WindowsWindow::IsVSync() const { return _data.vSync; }


void* WindowsWindow::GetNativeWindow() const { return (void*)_window; }
