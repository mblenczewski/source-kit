#include "pch.h"
#include <GLFW/glfw3.h>
#include "app"
#include "math"
#include "core"
#include "input"

// EInputState ToNextState(EInputState current, bool isDown)
// {
// 	switch (current)
// 	{
// 		case EInputState::RELEASED:
// 			return isDown ? EInputState::DOWN : current;
// 		case EInputState::DOWN:
// 			return isDown ? EInputState::HELD	: EInputState::UP;
// 		case EInputState::HELD:
// 			return isDown ? EInputState::HELD : EInputState::UP;
// 		case EInputState::UP:
// 			return isDown ? EInputState::DOWN : EInputState::RELEASED;
// 	}
// 	return EInputState::RELEASED;
// }

static std::vector<unsigned int> SKeys = {
	KY_SPACE, KY_APOSTROPHE, KY_COMMA, KY_MINUS, KY_PERIOD, KY_FSLASH, KY_SEMICOLON,
	KY_EQUAL, KY_LBRACKET, KY_BSLASH, KY_RBRACKET, KY_GRAVE,

	KY_0, KY_1, KY_2, KY_3, KY_4, KY_5, KY_6, KY_7, KY_8, KY_9,

	KY_A, KY_B, KY_C, KY_D, KY_E, KY_F, KY_G, KY_H, KY_I, KY_J,
	KY_K, KY_L, KY_M, KY_N, KY_O, KY_P, KY_Q, KY_R, KY_S, KY_T,
	KY_U, KY_V, KY_W, KY_X, KY_Y, KY_Z,

	// Function Keys
	KY_ESC, KY_ENTER, KY_TAB, KY_BACKSPACE, KY_INSERT, KY_DELETE,
	KY_RIGHT, KY_LEFT, KY_DOWN, KY_UP, KY_PAGE_UP, KY_PAGE_DOWN, KY_HOME, KY_END,
	KY_CAPS, KY_SCROLL, KY_NUM_LOCK, KY_PRINT_SCREEN, KY_PAUSE,
	KY_F1, KY_F2, KY_F3, KY_F4, KY_F5, KY_F6, KY_F7, KY_F8, KY_F9, KY_F10, KY_F11, KY_F12, KY_F13,
	KY_F14, KY_F15, KY_F16, KY_F17, KY_F18, KY_F19, KY_F20, KY_F21, KY_F22, KY_F23, KY_F24, KY_F25,

	KY_KP_0, KY_KP_1, KY_KP_2, KY_KP_3, KY_KP_4, KY_KP_5, KY_KP_6, KY_KP_7, KY_KP_8, KY_KP_9,

	KY_KP_DECIMAL, KY_KP_DIVIDE, KY_KP_MULTIPLY, KY_KP_SUBTRACT, KY_KP_ADD, KY_KP_ENTER,KY_KP_EQUAL,

	KY_LSHIFT, KY_LCONTROL, KY_L_ALT, KY_LSUPER,
	KY_RSHIFT, KY_RCONTROL, KY_R_ALT, KY_RSUPER,
	KY_MENU
};

static std::vector<unsigned int> SMouseButtons = {
	MBC_1, MBC_2, MBC_3, MBC_4,
	MBC_5, MBC_6, MBC_7, MBC_8
};

// Members
MngInput::MngInput(IF_WING(MngInput* ref)) :
	_mousePos(0.0f, 0.0f), _keyStates(40), _mouseButtonStates(8), MxnSingleton<MngInput>(IF_WING_ELSE(ref, this))
{
	PRF_FN;
	for (auto it = SKeys.begin(); it != SKeys.end(); ++it)
	{
		_keyStates.emplace(*it, EInputState::RELEASED);
	}

	for (auto it = SMouseButtons.begin(); it != SMouseButtons.end(); ++it)
	{
		_mouseButtonStates.emplace(*it, EInputState::RELEASED);
	}
}

void MngInput::UpdateImpl()
{
	PRF_FN;
	GLFWwindow *window = static_cast<GLFWwindow*>(App::GetWindow().GetNativeWindow());
	for (auto it = _keyStates.begin(); it != _keyStates.end(); ++it)
	{
		auto state = glfwGetKey(window, it->first);
		// it->second = ToNextState(it->second, state == GLFW_PRESS || state == GLFW_REPEAT);
		ToNextState(it->second, state == GLFW_PRESS || state == GLFW_REPEAT);
	}

	for (auto it = _mouseButtonStates.begin(); it != _mouseButtonStates.end(); ++it)
	{
		auto state = glfwGetMouseButton(window, it->first);
		// it->second = ToNextState(it->second, state == GLFW_PRESS || state == GLFW_REPEAT);
		ToNextState(it->second, state == GLFW_PRESS || state == GLFW_REPEAT);
	}
	
	double x = 0, y = 0;
	glfwGetCursorPos((GLFWwindow*)App::GetWindow().GetNativeWindow(), &x, &y);
	_mousePos.x = (float)x;
	_mousePos.y = (float)y;
}

EInputState MngInput::GetKeyStateImpl(unsigned int keycode)
{
#ifdef BLD_DEBUG
	if (_keyStates.find(keycode) == _keyStates.end())
	{
		LG_ERR("Key {} is not in the _keyStates map", keycode);
		return EInputState::RELEASED;
	}
#endif
	return _keyStates[keycode];
	// return glfwGetKey((GLFWwindow*)App::GetWindow().GetNativeWindow(), keycode);
}

EInputState MngInput::GetMouseButtonStateImpl(unsigned int mouseButton)
{
#ifdef BLD_DEBUG
	if (_mouseButtonStates.find(mouseButton) == _mouseButtonStates.end())
	{
		LG_ERR("Mouse Button {} is not in the _mouseButtonStates map", mouseButton);
		return EInputState::RELEASED;
	}
#endif
	return _mouseButtonStates[mouseButton];
	// return glfwGetMouseButton((GLFWwindow*)App::GetWindow().GetNativeWindow(), mouseButton);
}

// inline glm::vec2 MngInput::GetMousePositionImpl() const { return _mousePos; }
// glm::vec2 MngInput::GetMousePositionImpl()
// {
// 	double x = 0, y = 0;
// 	glfwGetCursorPos((GLFWwindow*)App::GetWindow().GetNativeWindow(), &x, &y);
// 	return glm::vec2((float)x, (float)y);
// }
