#pragma once

#include <GLFW/glfw3.h>
#include "core"
#include "Application/window.h"
#include "Graphics/Renderer/render_context.h"

class WindowsWindow : public MxnWindow
{
public:
	WindowsWindow(const WindowProps& props);
	virtual ~WindowsWindow();

	void OnUpdate() override;

	inline unsigned int GetWidth() const override { return _data.width; }
	inline unsigned int GetHeight() const override { return _data.height; }

	// Window Attributes
	inline void SetEventCallback(const FnEventCallback& callback) override { _data.callback = callback; }
	void SetVSync(bool enabled) override;
	bool IsVSync() const override;
	void* GetNativeWindow() const override;

private:
	void Init(const WindowProps& props);
	void Shutdown();

private:
	GLFWwindow *_window;
	MxnRenderContext *_renderContext;

	// We have this struct so we can send this data to GLFW
	// without sending the whole class
	struct WindowData
	{
		std::string title;
		unsigned int width, height;
		bool vSync;

		FnEventCallback callback;
	};

	WindowData _data;
};
