#include "pch.h"
#include "ogl_shader_compilation.h"

#include <glad/glad.h>
#include "core"
#include "debug"

static GLenum ShaderTypeFromString(const std::string& type)
{
	if (type == "vertex")
		return GL_VERTEX_SHADER;
	if (type == "fragment" || type == "pixel")
		return GL_FRAGMENT_SHADER;

	ASSERT(false, "Unknown shader type!");
	return 0;
}

ShaderSrcMap ParseShader(const fs::path& shaderPath)
{
	std::ifstream srcFile(shaderPath.string(), std::ios::in | std::ios::binary);
	std::string src;
	if (srcFile)
	{
		srcFile.seekg(0, std::ios::end);
		size_t size = srcFile.tellg();
		if (size != -1)
		{
			src.resize(size);
			srcFile.seekg(0, std::ios::beg);
			srcFile.read(&src[0], size);
		}
		else
			LG_ERR("Could not read from file {0}", shaderPath.string());
	}
	else
		LG_ERR("Could not open file {0}", shaderPath.string());
	
	const char *tknType = "#shader";
	size_t tknTypeLen = strlen(tknType);
	size_t pos = src.find(tknType, 0);
	ShaderSrcMap shaderSrcs;

	while (pos != std::string::npos)
	{
		size_t eol = src.find_first_of("\r\n", pos); //End of shader type declaration line
		ASSERT(eol != std::string::npos, "Syntax error");
		size_t begin = pos + tknTypeLen + 1; //Start of shader type name (after "#type " keyword)
		std::string type = src.substr(begin, eol - begin);
		ASSERT(ShaderTypeFromString(type), "Invalid shader type specified");

		size_t nextLinePos = src.find_first_not_of("\r\n", eol); //Start of shader code after shader type declaration line
		ASSERT(nextLinePos != std::string::npos, "Syntax error");
		pos = src.find(tknType, nextLinePos); //Start of next shader type declaration line

		shaderSrcs[ShaderTypeFromString(type)] = (pos == std::string::npos) ? src.substr(nextLinePos) : src.substr(nextLinePos, pos - nextLinePos);
	}

	// std::string line;
	// std::stringstream ss[2];
	// TyShader type = TyShader::NONE;

	// while(getline(src, line))
	// {
	// 	if (line.find("#shader") != std::string::npos)
	// 	{
	// 		if (line.find("vertex") != std::string::npos)
	// 			type = TyShader::VERTEX;
	// 		else if (line.find("pixel") != std::string::npos)
	// 			type = TyShader::PIXEL;
			
	// 		continue;
	// 	}

	// 	if (type != TyShader::NONE)
	// 		ss[(int) type] << line << "\n";
	// }

	return shaderSrcs;
}

#define MAX_SHADERS 2
unsigned int CreateProgram(const ShaderSrcMap& mapShaders)
{
	unsigned int iShaderIDs = 0;
	unsigned int shaderIDs[MAX_SHADERS];
	ASSERT(mapShaders.size() < 3, "Can only support up to 2 shaders");

	for (auto&& [type, source] : mapShaders)
	{
		// Send shader source code to GL
		unsigned int shader = glCreateShader(type);
		const char *cStrSource = (const char *)source.c_str();
		glShaderSource(shader, 1, &cStrSource, 0);

		glCompileShader(shader);

		GLint isCompiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);

		if (isCompiled == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> infoLog(maxLength);
			glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);
			
			// We don't need the shader anymore.
			glDeleteShader(shader);

			const char *shaderName;
			switch (type)
			{
				case GL_VERTEX_SHADER:
				{
					shaderName = "Vertex";
					break;
				}
				case GL_FRAGMENT_SHADER:
				{
					shaderName = "Pixel";
					break;
				}
			}

			// Use the infoLog as you see fit.
			LG_ERR("{} Shader compilation failure: {}", shaderName, &infoLog[0]);
			return 0;
		}

		shaderIDs[iShaderIDs++] = shader;
	}

	// Vertex and fragment shaders are successfully compiled.
	// Now time to link them together into a program.
	// Get a program object
	GLuint program = glCreateProgram();
	
	// Attach our shaders to our program
	for (unsigned int i = 0; i < MAX_SHADERS; i++)
		glAttachShader(program, shaderIDs[i]);
	
	// Link our program
	glLinkProgram(program);

	// Note the different functions here: glGetProgram* instead of glGetShader*.
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
		
		// We don't need the program anymore.
		glDeleteProgram(program);
		// Don't leak shaders either.
		for (unsigned int i = 0; i < MAX_SHADERS; i++)
			glDeleteShader(shaderIDs[i]);
		// glDeleteShader(idPixel);

		// Use the infoLog as you see fit.
		LG_ERR("Shader linking failure: {}", &infoLog[0]);
		
		// In this simple program, we'll just leave
		return 0;
	}

	for (unsigned int i = 0; i < MAX_SHADERS; i++)
		glDetachShader(program, shaderIDs[i]);
	
	return program;
}

// unsigned int CompileShader(TyShader type, const std::string& source)
// {
// 	unsigned int glType = 0;
// 	const char* shaderName;
// 	switch (type)
// 	{
// 		case TyShader::VERTEX:
// 			glType = GL_VERTEX_SHADER;
// 			shaderName = "Vertex";
// 			break;
// 		case TyShader::PIXEL:
// 			glType = GL_FRAGMENT_SHADER;
// 			shaderName = "Pixel";
// 			break;
// 		default:
// 			LG_ERR("Shader type not accounted for {}", (int)type);
// 			return 0;
// 	}

// 	// Send shader source code to GL
// 	unsigned int shader = glCreateShader(glType);
// 	const char *cStrSource = (const char *)source.c_str();
// 	glShaderSource(shader, 1, &cStrSource, 0);

// 	glCompileShader(shader);

// 	GLint isCompiled = 0;
// 	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);

// 	if (isCompiled == GL_FALSE)
// 	{
// 		GLint maxLength = 0;
// 		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

// 		// The maxLength includes the NULL character
// 		std::vector<GLchar> infoLog(maxLength);
// 		glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);
		
// 		// We don't need the shader anymore.
// 		glDeleteShader(shader);

// 		// Use the infoLog as you see fit.
// 		LG_ERR("{} Shader compilation failure: {}", shaderName, &infoLog[0]);
// 		return 0;
// 	}

// 	return shader;
// }

// unsigned int CreateProgram(unsigned int idVertex, unsigned int idPixel)
// {
// 	// Vertex and fragment shaders are successfully compiled.
// 	// Now time to link them together into a program.
// 	// Get a program object.
// 	GLuint program = glCreateProgram();

// 	// Attach our shaders to our program
// 	glAttachShader(program, idVertex);
// 	glAttachShader(program, idPixel);

// 	// Link our program
// 	glLinkProgram(program);

// 	// Note the different functions here: glGetProgram* instead of glGetShader*.
// 	GLint isLinked = 0;
// 	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
// 	if (isLinked == GL_FALSE)
// 	{
// 		GLint maxLength = 0;
// 		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

// 		// The maxLength includes the NULL character
// 		std::vector<GLchar> infoLog(maxLength);
// 		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
		
// 		// We don't need the program anymore.
// 		glDeleteProgram(program);
// 		// Don't leak shaders either.
// 		glDeleteShader(idVertex);
// 		glDeleteShader(idPixel);

// 		// Use the infoLog as you see fit.
// 		LG_ERR("Shader linking failure: {}", &infoLog[0]);
		
// 		// In this simple program, we'll just leave
// 		return 0;
// 	}

// 	// Always detach shaders after a successful link.
// 	glDetachShader(program, idVertex);
// 	glDetachShader(program, idPixel);
// 	return program;
// }
