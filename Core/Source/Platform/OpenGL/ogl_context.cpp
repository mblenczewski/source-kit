#include "pch.h"
#include "ogl_context.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "debug"

OGLContext::OGLContext(GLFWwindow *windowHandle) : _windowHandle(windowHandle)
{
	ASSERT(windowHandle, "Window Handle is null!");
}

void OGLContext::Init()
{
	glfwMakeContextCurrent(_windowHandle);
	int status = gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	ASSERT(status, "Failed to initialise GLAD");

	LG_INF("OpenGL Renderer: {} {} {}", glGetString(GL_VENDOR), glGetString(GL_RENDERER), glGetString(GL_VERSION));
	
	// glEnable(GL_CULL_FACE);
}

void OGLContext::SwapBuffers()
{
	glfwSwapBuffers(_windowHandle);
}
