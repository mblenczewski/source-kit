#pragma once

#include <GLFW/glfw3.h>
#include "Graphics/Renderer/render_context.h"

class OGLContext : public MxnRenderContext
{
public:
	OGLContext(GLFWwindow *window);
	// ~OGLContext();

	void Init() override;
	void SwapBuffers() override;

private:
	GLFWwindow *_windowHandle;
};
