#include "pch.h"
#include "ogl_render_api.h"
#include <glad/glad.h>
#include "debug"

void OGLRenderAPI::Init()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glEnable(GL_DEPTH_TEST);
}

void OGLRenderAPI::SetViewport(unsigned int x, unsigned int y, unsigned int width, unsigned int height)
{
	glViewport(x, y, width, height);
}

void OGLRenderAPI::SetClearColour(const glm::vec4& colour)
{
	glClearColor(colour.r, colour.g, colour.b, colour.a);
}

void OGLRenderAPI::DrawIndexed(const ref<MxnVertexArray>& vertexArray)
{
	vertexArray->Bind();
	glDrawElements(GL_TRIANGLES, vertexArray->GetIndexBuffer()->GetCount(), GL_UNSIGNED_INT, nullptr);
}

void OGLRenderAPI::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

#ifdef BLD_DEBUG
void OGLRenderAPI::CheckForErrors() const
{
	while (GLenum error = glGetError())
	{
		LG_TRC("[OpenGL Error] ({})", error);
	}
}
#endif
