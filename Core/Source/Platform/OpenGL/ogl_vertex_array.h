#pragma once
#include "core"
#include "ogl_buffers.h"
#include "Graphics/Data/vertex_array.h"

class OGLVertexArray : public MxnVertexArray
{
public:
	OGLVertexArray();
	virtual ~OGLVertexArray();
	
	void Bind() const override;
	void UnBind() const override;
	
	void AddVertexBuffer(const ref<MxnBfrVertex>& bfrVertex) override;	
	const std::vector<ref<MxnBfrVertex>>& GetVertexBuffers() const override { return _bfrVerticies; }

	void SetIndexBuffer(const ref<MxnBfrIndex>& bfrIndex) override;
	const ref<MxnBfrIndex>& GetIndexBuffer() const override { return _bfrIndex; }

private:
	unsigned int _ID;
	std::vector<ref<MxnBfrVertex>> _bfrVerticies;
	ref<MxnBfrIndex> _bfrIndex;
};
