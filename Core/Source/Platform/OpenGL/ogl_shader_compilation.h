#pragma once
#include "core"

#include <glad/glad.h>

// struct SrcShader
// {
// 	std::string vertex;
// 	std::string pixel;
// };

// enum class TyShader { NONE = -1, VERTEX = 0, PIXEL = 1 };

using ShaderSrcMap = std::unordered_map<GLuint, std::string>;

ShaderSrcMap ParseShader(const fs::path& shaderPath);
unsigned int CreateProgram(const ShaderSrcMap& mapShaders);

// unsigned int CompileShader(GLenum type, const std::string& source);
// unsigned int CreateProgram(unsigned int[] shaderIDs, unsigned int count);
// unsigned int CreateProgram(unsigned int idVertex, unsigned int idPixel);
