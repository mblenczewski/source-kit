#include "pch.h"
#include "ogl_texture.h"

#include <stb_image.h>
#include "debug"

OGLTex2D::OGLTex2D(unsigned int width, unsigned int height) : _ID(0), _dimensions(width, height), _path(), _fmtInternal(GL_RGBA8), _fmtData(GL_RGBA)
{
	glCreateTextures(GL_TEXTURE_2D, 1, &_ID);
	glTextureStorage2D(_ID, 1, _fmtInternal, width, height);

	glTextureParameteri(_ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(_ID, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTextureParameteri(_ID, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(_ID, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

OGLTex2D::OGLTex2D(const fs::path& path) : _ID(0), _dimensions(0, 0), _path(path)
{
	int width, height, nChannels;
	// Clear needs to be moved somewhere, not sure where
	stbi_set_flip_vertically_on_load(1);

	unsigned char* data = stbi_load(path.string().c_str(), &width, &height, &nChannels, 0);

	GLenum fmtInternal, fmtData;
	switch (nChannels)
	{
		case 3:
		{
			fmtInternal = GL_RGB8;
			fmtData = GL_RGB;
			break;
		}
		case 4:
		{
			fmtInternal = GL_RGBA8;
			fmtData = GL_RGBA;
			break;
		}
	}

	ASSERT(fmtInternal && fmtData, "Texture formats not set properly");	

	_dimensions.x = width;
	_dimensions.y = height;
	
	glCreateTextures(GL_TEXTURE_2D, 1, &_ID);
	glTextureStorage2D(_ID, 1, fmtInternal, width, height);

	glTextureParameteri(_ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(_ID, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTextureParameteri(_ID, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(_ID, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTextureSubImage2D(_ID, 0, 0, 0, width, height, fmtData, GL_UNSIGNED_BYTE, data);

	stbi_image_free(data);
}

OGLTex2D::~OGLTex2D()
{
	glDeleteTextures(1, &_ID);
}

void OGLTex2D::Bind(unsigned int slot) const
{
	glBindTextureUnit(slot, _ID);
}

void OGLTex2D::SetTexData(void *data, unsigned int size)
{
	unsigned int bytesPerPixel = _fmtData == GL_RGBA ? 4 : 3;
	ASSERT(size == _dimensions.x * _dimensions.y * bytesPerPixel, "The size of the passed in data does match the alloted space for this texture");
	glTextureSubImage2D(_ID, 0, 0, 0, _dimensions.x, _dimensions.y, _fmtData, GL_UNSIGNED_BYTE, data);
}
