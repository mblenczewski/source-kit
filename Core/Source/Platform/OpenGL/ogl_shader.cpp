#include "pch.h"
#include "ogl_shader.h"

#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>
#include "core"
#include "debug"

#include "ogl_shader_compilation.h"

OGLShader::OGLShader(const std::string& name, const fs::path& filepath) : _name(name), _ID(0), _filepath(filepath), _uniformMap{}
{
	ShaderSrcMap srcs = ParseShader(filepath);
	CreateShader(srcs);
}

OGLShader::OGLShader(const std::string& name, const std::string& srcVertex, const std::string& srcPixel) : _name(name), _ID(0), _filepath(), _uniformMap{}
{
	ShaderSrcMap mapShaders = 
		{ { GL_VERTEX_SHADER, srcVertex }, { GL_FRAGMENT_SHADER, srcPixel } };

	CreateShader(mapShaders);
}

OGLShader::~OGLShader()
{
	glDeleteProgram(_ID);
}

void OGLShader::Bind() const
{
	glUseProgram(_ID);
}

void OGLShader::UnBind() const
{
	glUseProgram(0);
}

void OGLShader::UploadInt(const char *uniform, int value)
{
	glUniform1i(GetUniformLocation(uniform), value);
}

void OGLShader::UploadFloat(const char *uniform, float value)
{
	glUniform1fv(GetUniformLocation(uniform), 1, &value);
}

void OGLShader::UploadFloat2(const char *uniform, const glm::vec2& value)
{
	glUniform2fv(GetUniformLocation(uniform), 1, glm::value_ptr(value));
}

void OGLShader::UploadFloat3(const char *uniform, const glm::vec3& value)
{
	glUniform3fv(GetUniformLocation(uniform), 1, glm::value_ptr(value));
}

void OGLShader::UploadFloat4(const char *uniform, const glm::vec4& value)
{
	glUniform4fv(GetUniformLocation(uniform), 1, glm::value_ptr(value));
}

void OGLShader::UploadMat4(const char *uniform, const glm::mat4& matrix)
{
	glUniformMatrix4fv(GetUniformLocation(uniform), 1, GL_FALSE, glm::value_ptr(matrix));
}

void OGLShader::UploadMat3(const char *uniform, const glm::mat3& matrix)
{
	glUniformMatrix3fv(GetUniformLocation(uniform), 1, GL_FALSE, glm::value_ptr(matrix));
}

void OGLShader::CreateShader(const ShaderSrcMap& sources)
{
	// unsigned int idVertex = CompileShader(TyShader::VERTEX, srcVertex);
	// unsigned int idPixel = CompileShader(TyShader::PIXEL, srcPixel);
	_ID = CreateProgram(sources);
}

int OGLShader::GetUniformLocation(const char *uniform)
{
	if (_uniformMap.find(uniform) != _uniformMap.end())
		return _uniformMap[uniform];
	
	int id = glGetUniformLocation(_ID, uniform);
	_uniformMap[uniform] = id;

	if (id == -1)
		LG_ERR("Uniform {} does not exist in shader {}", uniform, _filepath);
	
	return id;
}
