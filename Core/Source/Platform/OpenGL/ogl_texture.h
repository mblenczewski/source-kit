#pragma once

#include <glad/glad.h>

#include "core"
#include "math"
#include "Graphics/Data/texture.h"

class OGLTex2D : public MxnTexture
{
public:
	OGLTex2D(unsigned int width, unsigned int height);
	OGLTex2D(const fs::path& path);
	~OGLTex2D() override;

	void Bind(unsigned int slot = 0) const override;

	void SetTexData(void *data, unsigned int size) override;

	glm::uvec2 GetDimensions() const override { return _dimensions; }

private:
	unsigned int _ID;
	glm::uvec2 _dimensions;
	fs::path _path;

	GLenum _fmtInternal, _fmtData;
};

