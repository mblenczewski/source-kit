#pragma once

#include <glm/glm.hpp>
#include "Graphics/Data/shader.h"

#include "ogl_shader_compilation.h"

// To be made into a mixin and have one made for each render API
class OGLShader : public MxnShader
{
public:
	OGLShader(const std::string& name, const fs::path& filepath);
	OGLShader(const std::string& name, const std::string& srcVertex, const std::string& srcPixel);
	~OGLShader() override;

	const std::string& GetName() const override { return _name; }
	void Bind() const override;
	void UnBind() const override;

	void UploadInt(const char *uniform, int value) override;
	void UploadFloat(const char *uniform, float value) override;
	void UploadFloat2(const char *uniform, const glm::vec2& value) override;
	void UploadFloat3(const char *uniform, const glm::vec3& value) override;
	void UploadFloat4(const char *uniform, const glm::vec4& value) override;
	void UploadMat4(const char *uniform, const glm::mat4& matrix) override;
	void UploadMat3(const char *uniform, const glm::mat3& matrix) override;

private:
	void CreateShader(const ShaderSrcMap& mpShaderSrcs);
	int GetUniformLocation(const char *uniform);

	std::string _name;
	unsigned int _ID;
	fs::path _filepath;
	std::unordered_map<std::string, int> _uniformMap;
};
