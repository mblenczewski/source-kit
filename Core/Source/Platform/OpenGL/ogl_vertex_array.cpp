#include "pch.h"
#include "ogl_vertex_array.h"

#include <glad/glad.h>
#include "core"

GLenum ShaderDataTypeToGLType(ShaderDataType type)
{
	switch (type)
	{
		case ShaderDataType::Bool:      return GL_BOOL;
		case ShaderDataType::Float1:    return GL_FLOAT;
		case ShaderDataType::Float2:    return GL_FLOAT;
		case ShaderDataType::Float3:    return GL_FLOAT;
		case ShaderDataType::Float4:    return GL_FLOAT;
		case ShaderDataType::Int1:      return GL_INT;
		case ShaderDataType::Int2:      return GL_INT;
		case ShaderDataType::Int3:      return GL_INT;
		case ShaderDataType::Int4:      return GL_INT;
		case ShaderDataType::Mat3x3:    return GL_FLOAT;
		case ShaderDataType::Mat4x4:    return GL_FLOAT;
	}

	ASSERT(false, "Unknown ShaderDataType");
	return 0;
}

OGLVertexArray::OGLVertexArray()
{
	glCreateVertexArrays(1, &_ID);
}

OGLVertexArray::~OGLVertexArray()
{
	glDeleteVertexArrays(1, &_ID);
}

void OGLVertexArray::Bind() const { glBindVertexArray(_ID); }

void OGLVertexArray::UnBind() const { glBindVertexArray(0); }

void OGLVertexArray::AddVertexBuffer(const ref<MxnBfrVertex>& bfrVertex)
{
	_bfrVerticies.push_back(bfrVertex);

	glBindVertexArray(_ID);
	bfrVertex->Bind();
	
	unsigned int index = 0;
	unsigned int stride = bfrVertex->GetLayout().GetStride();
	for (const auto& element : bfrVertex->GetLayout())
	{
		glEnableVertexAttribArray(index);
		glVertexAttribPointer(
			index, 
			element.GetComponentCount(), 
			ShaderDataTypeToGLType(element.type), 
			element.normalised ? GL_TRUE : GL_FALSE, 
			stride, 
			(const void*)(long long)element.offset
		);
		index++;
	}
}
void OGLVertexArray::SetIndexBuffer(const ref<MxnBfrIndex>& bfrIndex)
{
	glBindVertexArray(_ID);
	bfrIndex->Bind();
	_bfrIndex = bfrIndex;
}
