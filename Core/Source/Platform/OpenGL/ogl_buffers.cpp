#include "pch.h"
#include "ogl_buffers.h"
#include <glad/glad.h>
#include "core"
#include "debug"

OGLVertexBuffer::OGLVertexBuffer(float verticies[], unsigned int size)
{
	glCreateBuffers(1, &_ID);
	glBindBuffer(GL_ARRAY_BUFFER, _ID);
	glBufferData(GL_ARRAY_BUFFER, size, verticies, GL_STATIC_DRAW);
}

OGLVertexBuffer::~OGLVertexBuffer()
{
	glDeleteBuffers(1, &_ID);
}

void OGLVertexBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, _ID);
}

void OGLVertexBuffer::UnBind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void OGLVertexBuffer::SetBufferLayout(const BfrLayout& layout) { _layout = layout; }

const BfrLayout& OGLVertexBuffer::GetLayout() const { return _layout; }



//////////// INDEX BUFFER

OGLIndexBuffer::OGLIndexBuffer(unsigned int indicies[], unsigned int count) : _count(count)
{
	glCreateBuffers(1, &_ID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * count, indicies, GL_STATIC_DRAW);
}

OGLIndexBuffer::~OGLIndexBuffer()
{
	glDeleteBuffers(1, &_ID);
}

void OGLIndexBuffer::Bind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ID);
}

void OGLIndexBuffer::UnBind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
