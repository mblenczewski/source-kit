#include <glm/glm.hpp>

#include "Graphics/Renderer/render_api.h"

class OGLRenderAPI : public MxnRenderAPI
{
public:
	OGLRenderAPI() : MxnRenderAPI(RenderAPIType::OpenGL) {}

	void Init();

	void SetViewport(unsigned int x, unsigned int y, unsigned int width, unsigned int height) override;
	void SetClearColour(const glm::vec4& colour) override;

	void DrawIndexed(const ref<MxnVertexArray>& vertexArray) override;
	
	void Clear() override;

#ifdef BLD_DEBUG
	void CheckForErrors() const override;
#endif
};
