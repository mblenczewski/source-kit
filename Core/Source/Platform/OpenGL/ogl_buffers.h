#pragma once
#include "core"

#include "Graphics/Data/buffers.h"

class OGLVertexBuffer : public MxnBfrVertex
{
public:
	OGLVertexBuffer(float verticies[], unsigned int size);
	~OGLVertexBuffer();

	void Bind() const override;
	void UnBind() const override;

	void SetBufferLayout(const BfrLayout& layout) override;
	const BfrLayout& GetLayout() const override;

private:
	unsigned int _ID;
	BfrLayout _layout;
};

class OGLIndexBuffer : public MxnBfrIndex
{
public:
	OGLIndexBuffer(unsigned int indicies[], unsigned int count);
	~OGLIndexBuffer();

	void Bind() const override;
	void UnBind() const override;
	unsigned int GetCount() const override { return _count; }
	
private:
	unsigned int _ID;
	unsigned int _count;
};
