#include "pch.h"
#include "core"
#include "debug"

#include "Graphics/Renderer/renderer.h"
#include "Graphics/Data/shader.h"

#include "OpenGL/ogl_shader.h"

MxnShader* LoadShader(const fs::path& filepath, std::string name)
{
	if (name.empty())
	{
		const std::string& path = filepath.string();
		size_t start = path.find_last_of("/\\") + 1;
		size_t lastDot = path.find_last_of('.');

		size_t lName = lastDot - start;
		name = name.substr(start, lName);
	}

	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:
			return (MxnShader*)new OGLShader(name, filepath);
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}

static unsigned int s_shaderIndex = 0;

MxnShader* LoadShader(const std::string& srcVertex, const std::string& srcPixel, std::string name)
{
	if (name.empty())
		name = "Shader " + std::to_string(s_shaderIndex++);

	switch (Renderer::GetAPI())
	{
		case RenderAPIType::None:
			LG_ERR("Don't have a null API");
			return nullptr;
		case RenderAPIType::OpenGL:
			return (MxnShader*)new OGLShader(name, srcVertex, srcPixel);
	}

	LG_ERR("API ... is not implemented");
	return nullptr;
}
