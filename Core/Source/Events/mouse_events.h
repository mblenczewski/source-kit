#pragma once

#include "math"
#include "events.h"

#define MOUSE_CATS EVENT_CLASS_CATEGORY(EventCategory::Mouse | EventCategory::Input)

class EvtMouseMoved : public MxnEvent
{
public:
	EvtMouseMoved(glm::vec2 position) : _position(position) {}
	EvtMouseMoved(float xPosition, float yPosition) : _position{xPosition, yPosition} {}

	MOUSE_CATS
	EVENT_CLASS_TYPE(MouseMoved)

	inline glm::vec2 GetPosition() const { return _position; }

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtMouseMoved: " << _position;
		return ss.str();
	}
#endif

private:
	glm::vec2 _position;
};

class EvtMouseScroll : public MxnEvent
{
public:
	EvtMouseScroll(glm::vec2 offset) : _offset(offset) {}
	EvtMouseScroll(float xOffset, float yOffset) : _offset(xOffset, yOffset) {}

	EVENT_CLASS_TYPE(MouseScrolled)
	MOUSE_CATS

	inline glm::vec2 GetScroll() const { return _offset; }

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtMouseScroll: " << _offset;
		return ss.str();
	}
#endif

private:
	glm::vec2 _offset;
};

class MxnEvtMouseButton
{
public:
	inline int GetMouseButton() const { return _button; }

protected:
	MxnEvtMouseButton(int button) : _button(button) {}

	int _button;
};

class EvtMouseButtonPressed : public MxnEvent, public MxnEvtMouseButton
{
public:
	EvtMouseButtonPressed(int button) : MxnEvtMouseButton(button) {}

	MOUSE_CATS
	EVENT_CLASS_TYPE(MouseButtonPressed)

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "MoustButtonPressedEvent: " << _button;
		return ss.str();
	}
#endif
};

class EvtMouseButtonReleased : public MxnEvent, public MxnEvtMouseButton
{
public:
	EvtMouseButtonReleased(int button) : MxnEvtMouseButton(button) {}

	MOUSE_CATS
	EVENT_CLASS_TYPE(MouseButtonReleased)

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "MxnEvtMouseButtonReleased: " << _button;
		return ss.str();
	}
#endif
};

#undef MOUSE_CATS
