#pragma once

#include "core"

#define BIND_EVT_FN(fn) std::bind(&fn, std::placeholders::_1)
#define BIND_EVT_MFN(fn) std::bind(&fn, this, std::placeholders::_1)

// The current implementation of events is blocking,
// meaning that when the event is received it is immediately dispatched
// and must be dealt with then and there
// In the future, it will be better to store all the events
// and then dispatch them all at once in an event stage on the update stage

enum class EventType
{
	None = 0,
	WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
	AppTick, AppUpdate, AppRender,
	KeyPressed, KeyReleased, KeyTyped,
	MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
};

enum EventCategory
{
	None          = 0,
	Application   = BIT(0),
	Input         = BIT(1),
	Keyboard      = BIT(2),
	Mouse         = BIT(3),
	MouseButton   = BIT(4)
};

#ifdef BLD_DEBUG
	#define EVENT_CLASS_TYPE(type)\
		static EventType GetStaticType() { return EventType::type; }\
		virtual EventType GetEventType() const override { return GetStaticType(); }\
		virtual const char* GetName() const override { return #type; }
#else
	#define EVENT_CLASS_TYPE(type)\
		static EventType GetStaticType() { return EventType::type; }\
		virtual EventType GetEventType() const override { return GetStaticType(); }
#endif

#define EVENT_CLASS_CATEGORY(category) virtual int GetCategoryFlags() const override { return category; }

class MxnEvent
{
	friend class EventDispatcher;
public:

	virtual EventType GetEventType() const = 0;
	virtual int GetCategoryFlags() const = 0;
#ifdef BLD_DEBUG
	virtual const char* GetName() const = 0;
	virtual std::string ToString() const { return GetName(); }
#endif

	inline bool IsInCategory(EventCategory category) { return GetCategoryFlags() & category; }

	bool handled = false;
};

inline std::ostream& operator<<(std::ostream& out, const MxnEvent& event)
{
	out << event.ToString();
	return out;
}

class EventDispatcher
{
	template<typename	T>
	using FnEvent = std::function<bool(T&)>;

public:
	EventDispatcher(MxnEvent& event) : _event(event) {}

	template<typename T, typename F>
	bool Dispatch(const F& func)
	{
		if (_event.GetEventType() == T::GetStaticType())
		{
			_event.handled = func(static_cast<T&>(_event));
			return true;
		}
		return false;
	}

private:
	MxnEvent& _event;
};
