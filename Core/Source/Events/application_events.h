#pragma once

#include "math"
#include "events.h"

class EvtWindowClose : public MxnEvent
{
public:
	EvtWindowClose() {}

	EVENT_CLASS_TYPE(WindowClose)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
};

class EvtWindowFocus : public MxnEvent
{
public:
	EvtWindowFocus() {}

	EVENT_CLASS_TYPE(WindowFocus)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
};

class EvtWindowLostFocus : public MxnEvent
{
public:
	EvtWindowLostFocus() {}

	EVENT_CLASS_TYPE(WindowLostFocus)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
};

class EvtWindowResize : public MxnEvent
{
public:
	EvtWindowResize(unsigned int width, unsigned int height) : _size(width, height) {}

	EVENT_CLASS_TYPE(WindowResize)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
	
	inline const glm::uvec2& GetSize() const { return _size; }

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtWindowResize: (" << _size.x << ", " << _size.y << ")";
		return ss.str();
	}
#endif

private:
	glm::uvec2 _size;
};

class EvtWindowMoved : public MxnEvent
{
public:
	EvtWindowMoved(glm::vec2 position) : _position(position) {}

	EVENT_CLASS_TYPE(WindowMoved)
	EVENT_CLASS_CATEGORY(EventCategory::Application)

	glm::vec2 GetPosition() const { return _position; }

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtWindowMoved: " << _position;
		return ss.str();
	}
#endif

private:
	glm::vec2 _position;
};


class EvtAppTick : public MxnEvent
{
public:
	EvtAppTick() {}

	EVENT_CLASS_TYPE(AppTick)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
};

class EvtAppUpdate : public MxnEvent
{
public:
	EvtAppUpdate() {}

	EVENT_CLASS_TYPE(AppUpdate)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
};

class EvtAppRender : public MxnEvent
{
public:
	EvtAppRender() {}

	EVENT_CLASS_TYPE(AppRender)
	EVENT_CLASS_CATEGORY(EventCategory::Application)
};
