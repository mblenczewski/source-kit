#pragma once

#include "events.h"

#define KEY_CATS EVENT_CLASS_CATEGORY(EventCategory::Keyboard | EventCategory::Input);

class MxnKeyCode
{
public:
	inline int GetKeyCode() const { return _keycode; }

protected:
	MxnKeyCode(int keycode) : _keycode(keycode) {}

	int _keycode;
};

class EvtKeyPressed : public MxnEvent, public MxnKeyCode
{
public:
	EvtKeyPressed(int keycode, int repeatCount) :
		MxnKeyCode(keycode), _repeatCount(repeatCount)
	{}

	KEY_CATS
	EVENT_CLASS_TYPE(KeyPressed)
	inline int GetRepeatCount() const { return _repeatCount; }

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtKeyPressed: " << _keycode << " (" << _repeatCount << " repeats)";
		return ss.str();
	}
#endif

private:
	int _repeatCount;
};

class EvtKeyReleased : public MxnEvent, public MxnKeyCode
{
public:
	EvtKeyReleased(int keycode) :
		MxnKeyCode(keycode)
	{}

	KEY_CATS;
	EVENT_CLASS_TYPE(KeyReleased)

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtKeyReleased : " << _keycode;
		return ss.str();
	}
#endif
};

class EvtKeyTyped : public MxnEvent, public MxnKeyCode
{
public:
	EvtKeyTyped(int keycode) : MxnKeyCode(keycode) {}

	KEY_CATS
	EVENT_CLASS_TYPE(KeyTyped)

#ifdef BLD_DEBUG
	std::string ToString() const override
	{
		std::stringstream ss;
		ss << "EvtKeyTyped: " << _keycode;
		return ss.str();
	}
#endif
};

#undef KEY_CATS
