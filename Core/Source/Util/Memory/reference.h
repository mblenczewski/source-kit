#pragma once

#include "pch.h"
#include "owned_ptr.h"

template<typename T>
struct ref
{
public:
	ref() : _pointer(nullptr), _count(nullptr) {}
	// ref(T *pointer) : _pointer(&pointer), _count(new unsigned int(1)) {}
	ref(T *pointer) : _pointer(pointer), _count(new unsigned int(1)) {}

	// ref(const own<T>& obj) : _pointer(&obj._pointer), _count(nullptr) {}

	// ref(const ref& obj) : _pointer(obj._pointer), _count(obj._count)
	// { if (_pointer && _count) (*_count)++; }
	ref(const ref& obj) : _pointer(obj._pointer), _count(obj._count) { if (_count) (*_count)++; }

	ref(ref&& obj) : _pointer(obj._pointer), _count(obj._count) { obj._pointer = nullptr; obj._count = nullptr; }

	~ref() { _cleanup(); }

	std::string pstr() const 
	{ return std::to_string(reinterpret_cast<long long>(_pointer)); }

	unsigned int count() const { return _count ? *_count : 0; }

	// void reset(const own<T>& obj) { _pointer = &obj._pointer; _count = nullptr; }
	// void operator<=(const own<T>& obj) { reset(obj); }
	// void reset(const ref<T>& obj) { _pointer = &obj._pointer; _count = nullptr; }
	// void operator<=(const ref<T>& obj) { reset(obj); }

	ref& operator=(const ref& obj)
	{
		_cleanup();
		_pointer = obj._pointer;
		_count = obj._count;
		if (_count) (*_count)++;
		return *this;
	}

	ref& operator=(ref&& obj)
	{
		_cleanup();
		_pointer = obj._pointer;
		_count = obj._count;
		// obj._pointer  obj._count = nullptr;
		obj._pointer = nullptr;
		obj._count = nullptr;
		return *this;
	}

	// T* operator->() const { return *_pointer; }
	// T& operator*() const { return **_pointer; }
	T* operator->() const { return _pointer; }
	T& operator*() const { return *_pointer; }

	// operator bool() { return *_pointer; }
	operator bool() const { return _pointer; }

private:
	void _cleanup()
	{
		if (!_count)
			return;

		(*_count)--;
		if (*_count == 0)
		{
			// delete *_pointer;
			delete _pointer;
			delete _count;
		}
	}

	// T* const *_pointer;
	T *_pointer;
	unsigned int *_count;
};

template<typename T>
struct ref<T[]>
{
public:
	ref() : _pointer(nullptr), _count(nullptr) {}
	// ref(T *pointer) : _pointer(&pointer), _count(new unsigned int(1)) {}
	ref(T *pointer) : _pointer(pointer), _count(new unsigned int(1)) {}

	// ref(const own<T>& obj) : _pointer(&obj._pointer), _count(nullptr) {}

	// ref(const ref& obj) : _pointer(obj._pointer), _count(obj._count)
	// { if (_pointer && _count) (*_count)++; }
	ref(const ref& obj) : _pointer(obj._pointer), _count(obj._count) { if (_count) (*_count)++; }

	ref(ref&& obj) : _pointer(obj._pointer), _count(obj._count) { obj._pointer = nullptr; obj._count = nullptr; }

	~ref() { _cleanup(); }

	std::string pstr() const 
	{ return std::to_string(reinterpret_cast<long long>(_pointer)); }

	unsigned int count() const { return _count ? *_count : 0; }

	// void reset(const own<T>& obj) { _pointer = &obj._pointer; _count = nullptr; }
	// void operator<=(const own<T>& obj) { reset(obj); }
	// void reset(const ref<T>& obj) { _pointer = &obj._pointer; _count = nullptr; }
	// void operator<=(const ref<T>& obj) { reset(obj); }

	ref& operator=(const ref& obj)
	{
		_cleanup();
		_pointer = obj._pointer;
		_count = obj._count;
		if (_count) (*_count)++;
		return *this;
	}

	ref& operator=(ref&& obj)
	{
		_cleanup();
		_pointer = obj._pointer;
		_count = obj._count;
		// obj._pointer  obj._count = nullptr;
		obj._pointer = nullptr;
		obj._count = nullptr;
		return *this;
	}

	// T* operator->() const { return *_pointer; }
	// T& operator*() const { return **_pointer; }
	T* operator->() const { return _pointer; }
	T& operator*() const { return *_pointer; }

	// operator bool() { return *_pointer; }
	operator bool() const { return _pointer; }

private:
	void _cleanup()
	{
		if (!_count)
			return;

		(*_count)--;
		if (*_count == 0)
		{
			// delete *_pointer;
			delete[] _pointer;
			delete _count;
		}
	}

	// T* const *_pointer;
	T *_pointer;
	unsigned int *_count;
};
