#pragma once

#include "pch.h"

template<typename T>
struct ref;

template<typename T>
struct own
{
public:
	own() : _pointer(nullptr) {}
	own(T *pointer) : _pointer(pointer) {}

	own(own&& dyingObj) : _pointer(dyingObj._pointer) { dyingObj._pointer = nullptr; }

	~own() { _cleanup(); }

	void reset(T *pointer)
	{
		_cleanup();
		_pointer = pointer;
	}
	
	std::string pstr() const 
	{ return std::to_string(reinterpret_cast<long long>(_pointer)); }

	T* ptr() const { return _pointer; }

	void operator<=(T *pointer) { reset(pointer); }
	own& operator=(own&& dyingObj)
	{
		_cleanup();
		_pointer = dyingObj._pointer;
		dyingObj._pointer = nullptr;
	}

	operator ref<T>() const { return ref<T>(this); }

	T* operator->() { return _pointer; }
	T& operator*() { return *_pointer; }

	operator bool() { return _pointer; }

	bool operator==(const own<T> obj) { return _pointer == obj._pointer; }
	bool operator!=(const own<T> obj) { return !(this == obj); }

	// Delete copy semantics
	own(const own& obj) = delete;
	own& operator=(const own& obj) = delete;

	friend struct ref<T>;

private:
	void _cleanup() { if (_pointer) delete _pointer; _pointer = 0; }

	T *_pointer;
};

template<typename T>
struct own<T[]>
{
	own() : _pointer(nullptr) {}
	own(T *pointer) : _pointer(pointer) {}

	own(own&& dyingObj) : _pointer(dyingObj._pointer) { dyingObj._pointer = nullptr; }

	~own() { _cleanup(); }

	void reset(T *pointer)
	{
		_cleanup();
		_pointer = pointer;
	}

	void operator<=(T *pointer) { reset(pointer); }
	void operator=(own&& dyingObj)
	{
		_cleanup();
		_pointer = dyingObj._pointer;
		dyingObj._pointer = nullptr;
	}
	
	operator ref<T>() const { return ref<T>(this); }

	T* operator->() { return _pointer; }
	T& operator*() { return *_pointer; }

	operator bool() { return _pointer; }

	// Delete copy semantics
	own(const own& obj) = delete;
	own& operator=(const own& obj) = delete;

	friend struct ref<T[]>;

private:
	void _cleanup() { if (_pointer) delete[] _pointer; }

	T *_pointer;
};

