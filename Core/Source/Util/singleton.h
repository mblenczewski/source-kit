#pragma once

#include "core"
#include "debug"

template<class T>
class MxnSingleton
{
public:
	static IFN_DEBUG(inline) T* const Get()
	{
#ifdef BLD_DEBUG
		std::string name = typeid(SInstance).name();
		ASSERT(SInstance, name + " singleton hasn't been initialised");
#endif
		return SInstance;
	}

	static void Shutdown() { ASSERT(SInstance, "Tried to shutdown a singleton that has not been initialised"); delete SInstance; }

protected:
	MxnSingleton(T* data) { SInstance = data; }

	static T* SInstance;

private:
	MxnSingleton<T>(const MxnSingleton<T>&) = delete;
	MxnSingleton<T>& operator= (const MxnSingleton<T>&) = delete;
};

template<class T>
T* MxnSingleton<T>::SInstance = 0;
