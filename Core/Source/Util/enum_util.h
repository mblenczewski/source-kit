#pragma once

template<typename T, Enum... values>
struct Enum<T>
{
	T value;
	T values[] = { values };
	unsigned int size = sizeof...(values);

};

// template<typename Enum, Enum f, Enum...>
// struct __EFirst { static const Enum first = f; };

// template<typename Enum, Enum first, Enum head, Enum...>
// struct __EAdvance1
// {
// 	static void Adv(Enum& v) { if (v == head) v = first; }
// };

// template<typename Enum, Enum first, Enum head, Enum next, Enum... tail>
// struct __EAdvance2
// {
// 	static void Adv(Enum& v)
// 	{
// 		if (v == head) 
// 			v = next;
// 		else
// 			__EAdvance1<Enum, first, next, tail...>::Adv(v);
// 	}
// };

// template<typename Enum, Enum... values>
// struct __EnumValues
// {
// 	static void Advance(Enum& v) { __EAdvance2<Enum, __EFirst<Enum, values>::first, values, ...>::Adv(v); }
// };
