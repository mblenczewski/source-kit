#include "pch.h"
#include "enm_input.h"

// EInputState ToNextState(EInputState current, bool isPressed)
// {
// 	switch (current)
// 	{
// 		case EInputState::RELEASED:
// 			return isPressed ? EInputState::DOWN : current;
// 		case EInputState::DOWN:
// 			return isPressed ? EInputState::HELD	: EInputState::UP;
// 		case EInputState::HELD:
// 			return isPressed ? EInputState::HELD : EInputState::UP;
// 		case EInputState::UP:
// 			return isPressed ? EInputState::DOWN : EInputState::RELEASED;
// 	}
// 	return EInputState::RELEASED;
// }

void ToNextState(EInputState& state, bool isPressed)
{
	switch (state)
	{
		case EInputState::RELEASED:
			state = isPressed ? EInputState::DOWN : state;
			break;
		case EInputState::DOWN:
			state = isPressed ? EInputState::HELD	: EInputState::UP;
			break;
		case EInputState::HELD:
			state = isPressed ? EInputState::HELD : EInputState::UP;
			break;
		case EInputState::UP:
			state = isPressed ? EInputState::DOWN : EInputState::RELEASED;
			break;
	}
}

bool IsPressed(const EInputState& state) { return state == EInputState::DOWN || state == EInputState::HELD; }

std::ostream& operator<<(std::ostream& out, const EInputState& state) noexcept
{
	switch (state)
	{
		case EInputState::RELEASED:
			out << "Released";
			break;
		case EInputState::DOWN:
			out << "Down";
			break;
		case EInputState::HELD:
			out << "Held";
			break;
		case EInputState::UP:
			out << "Up";
			break;
	}
	return out;
}
