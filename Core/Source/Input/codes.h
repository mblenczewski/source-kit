#pragma once

// These are all GLFW key codes as we are using GLFW for all platforms
// If we start using a different windowing api, we'll have to change all
// of these
// For cross compatibility, all codes are serialised as strings

// Key Codes

#define KY_SPACE         32
#define KY_APOSTROPHE    39  /* ' */
#define KY_COMMA         44  /* , */
#define KY_MINUS         45  /* - */
#define KY_PERIOD        46  /* . */
#define KY_FSLASH         47  /* / */
#define KY_0             48
#define KY_1             49
#define KY_2             50
#define KY_3             51
#define KY_4             52
#define KY_5             53
#define KY_6             54
#define KY_7             55
#define KY_8             56
#define KY_9             57
#define KY_SEMICOLON     59  /* ; */
#define KY_EQUAL         61  /* = */
#define KY_A             65
#define KY_B             66
#define KY_C             67
#define KY_D             68
#define KY_E             69
#define KY_F             70
#define KY_G             71
#define KY_H             72
#define KY_I             73
#define KY_J             74
#define KY_K             75
#define KY_L             76
#define KY_M             77
#define KY_N             78
#define KY_O             79
#define KY_P             80
#define KY_Q             81
#define KY_R             82
#define KY_S             83
#define KY_T             84
#define KY_U             85
#define KY_V             86
#define KY_W             87
#define KY_X             88
#define KY_Y             89
#define KY_Z             90
#define KY_LBRACKET      91  /* [ */
#define KY_BSLASH        92  /* \ */
#define KY_RBRACKET      93  /* ] */
#define KY_GRAVE         96  /* ` */
#define KY_WORLD_1       161 /* non-US #1 */
#define KY_WORLD_2       162 /* non-US #2 */

/* Function keys */
#define KY_ESC           256
#define KY_ENTER         257
#define KY_TAB           258
#define KY_BACKSPACE     259
#define KY_INSERT        260
#define KY_DELETE        261
#define KY_RIGHT         262
#define KY_LEFT          263
#define KY_DOWN          264
#define KY_UP            265
#define KY_PAGE_UP       266
#define KY_PAGE_DOWN     267
#define KY_HOME          268
#define KY_END           269
#define KY_CAPS          280
#define KY_SCROLL        281
#define KY_NUM_LOCK      282
#define KY_PRINT_SCREEN  283
#define KY_PAUSE         284
#define KY_F1            290
#define KY_F2            291
#define KY_F3            292
#define KY_F4            293
#define KY_F5            294
#define KY_F6            295
#define KY_F7            296
#define KY_F8            297
#define KY_F9            298
#define KY_F10           299
#define KY_F11           300
#define KY_F12           301
#define KY_F13           302
#define KY_F14           303
#define KY_F15           304
#define KY_F16           305
#define KY_F17           306
#define KY_F18           307
#define KY_F19           308
#define KY_F20           309
#define KY_F21           310
#define KY_F22           311
#define KY_F23           312
#define KY_F24           313
#define KY_F25           314
#define KY_KP_0          320
#define KY_KP_1          321
#define KY_KP_2          322
#define KY_KP_3          323
#define KY_KP_4          324
#define KY_KP_5          325
#define KY_KP_6          326
#define KY_KP_7          327
#define KY_KP_8          328
#define KY_KP_9          329
#define KY_KP_DECIMAL    330
#define KY_KP_DIVIDE     331
#define KY_KP_MULTIPLY   332
#define KY_KP_SUBTRACT   333
#define KY_KP_ADD        334
#define KY_KP_ENTER      335
#define KY_KP_EQUAL      336
#define KY_LSHIFT        340
#define KY_LCONTROL      341
#define KY_L_ALT         342
#define KY_LSUPER        343
#define KY_RSHIFT        344
#define KY_RCONTROL      345
#define KY_R_ALT         346
#define KY_RSUPER        347
#define KY_MENU          348

#define KY_LAST          KY_MENU



#define MBC_1             0
#define MBC_2             1
#define MBC_3             2
#define MBC_4             3
#define MBC_5             4
#define MBC_6             5
#define MBC_7             6
#define MBC_8             7
#define MBC_LAST          MBC_8
#define MBC_LEFT          MBC_1
#define MBC_RIGHT         MBC_2
#define MBC_MIDDLE        MBC_3
