#pragma once

#include "core"
#include "ctw"
#include "math"

#include "enm_input.h"
#include "Util/singleton"

// enum class EInputState { RELEASED = 0, DOWN = 1, HELD = 2, UP = 3 };

// inline std::ostream& operator<<(std::ostream& out, const EInputState& state) noexcept
// {
// 	switch (state)
// 	{
// 		case EInputState::RELEASED:
// 			out << "Released";
// 			break;
// 		case EInputState::DOWN:
// 			out << "Down";
// 			break;
// 		case EInputState::HELD:
// 			out << "Held";
// 			break;
// 		case EInputState::UP:
// 			out << "Up";
// 			break;
// 	}
// 	return out;
// }


// typedef __EnumValues<EInputState, EInputState::RELEASED, EInputState::DOWN, EInputState::HELD, EInputState::UP> EVInputState;

// inline EInputState* operator++(EInputState* state)
// {
// 	EVInputState::Advance(state);
// 	return state;
// }

// bool IsDownInputState(EInputState state) { return state == EInputState::DOWN || state == EInputState::HELD; }

// class MngInput
// {
// public:
// 	static int GetKeyState(unsigned int keycode);
// 	static int GetMouseButtonState(unsigned int mouseButton);
// 	static glm::vec2 GetMousePosition();
// };

class MngInput : public MxnSingleton<MngInput>
{
public:
// 	static void Init()
// 	{
// #ifdef BLD_WING
// 	MngInput *ref = (MngInput*)CTWBridge::LoadPointer();
// 	new MngInput(ref);
// #else
// 	new MngInput;
// 	CTWBridge::SavePointer(SInstance);
// #endif

// 	}
	SiINIT(MngInput)

	static inline void Update() { return Get()->UpdateImpl(); }
	static inline EInputState GetKeyState(unsigned int keycode) { return Get()->GetKeyStateImpl(keycode); }
	static inline EInputState GetMouseButtonState(unsigned int mouseButton) { return Get()->GetMouseButtonStateImpl(mouseButton); }
	// static inline glm::vec2 GetMousePosition() { return Get()->GetMousePositionImpl(); }
	static inline const glm::vec2& GetMousePosition() { return Get()->_mousePos; }


private:
	MngInput(IF_WING(MngInput *ref));

	void UpdateImpl();
	EInputState GetKeyStateImpl(unsigned int keycode);
	EInputState GetMouseButtonStateImpl(unsigned int mouseButton);
	// inline glm::vec2 GetMousePositionImpl() const;

	glm::vec2 _mousePos;
	std::unordered_map<unsigned int, EInputState> _keyStates;
	std::unordered_map<unsigned char, EInputState> _mouseButtonStates;
};
