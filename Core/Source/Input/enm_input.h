#pragma once
#include "core"

enum class EInputState { RELEASED = 0, DOWN = 1, HELD = 2, UP = 3 };

void ToNextState(EInputState& state, bool isPressed);

std::ostream& operator<<(std::ostream& out, const EInputState& state) noexcept;

bool IsPressed(const EInputState& state);

// inline std::ostream& operator<<(std::ostream& out, const EInputState& state) noexcept
// {
// 	switch (state)
// 	{
// 		case EInputState::RELEASED:
// 			out << "Released";
// 			break;
// 		case EInputState::DOWN:
// 			out << "Down";
// 			break;
// 		case EInputState::HELD:
// 			out << "Held";
// 			break;
// 		case EInputState::UP:
// 			out << "Up";
// 			break;
// 	}
// 	return out;
// }
