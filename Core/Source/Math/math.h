#pragma once

#include <glm/glm.hpp>

template<typename OStream>
inline OStream& operator<<(OStream& os, glm::vec2& v2)
{
	return os << "(" << v2.x << ", " << v2.y << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const glm::vec2& v2)
{
	return os << "(" << v2.x << ", " << v2.y << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, glm::vec3& v3)
{
	return os << "(" << v3.x << ", " << v3.y << ", " << v3.z << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const glm::vec3& v3)
{
	return os << "(" << v3.x << ", " << v3.y << ", " << v3.z << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, glm::vec4& v4)
{
	return os << "(" << v4.x << ", " << v4.y << ", " << v4.z << ", " << v4.w << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const glm::vec4& v4)
{
	return os << "(" << v4.x << ", " << v4.y << ", " << v4.z << ", " << v4.w << ")";
}
