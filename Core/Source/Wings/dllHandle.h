#pragma once

#ifdef PLT_WINDOWS
#include <Windows.h>
#endif

class DLLHandle
{
private:
	HMODULE h;
	const char* _path;

public:
	DLLHandle(const char* filepath) : h(LoadLibrary(filepath)), _path(filepath) { }
	~DLLHandle() { FreeLibrary(h); }

	const HMODULE Get() const { return h; }
	const char* GetPath() const { return _path; }
};
