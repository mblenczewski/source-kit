#pragma once
#ifndef BLD_WING

#include "core"

#include "Util/singleton.h"
#include "wing_funcs.h"
#include "dllHandle.h"

#ifdef WING_EXTENSION
	#define TEMP "." WING_EXTENSION
	#undef WING_EXTENSION
	#define WING_EXTENSION TEMP
#else
	#define WING_EXTENSION ".wng"
#endif

class WingBeater : public MxnSingleton<WingBeater>
{
public:
	~WingBeater();
	
	static void GatherWings();
	static unsigned int WingCount() { return Get()->_wingCount; }

	static void CallStartup();
	static void CallShutdown();

private:
	DLLHandle **_wings;
	unsigned int _wingCount;

	WngStartup *WLinksStartup;
	WngShutdown *WLinksShutdown;

	// Can't forgo this
	WingBeater(DLLHandle **wings, unsigned int count
	, WngStartup *Startup
	, WngShutdown *Shutdown
);
};
#endif // not BLD_WING
