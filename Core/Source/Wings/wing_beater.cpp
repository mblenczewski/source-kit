#ifndef BLD_WING
#include "pch.h"
#include "debug"
#include "core"

#include "wing_beater.h"
#include "wing_funcs.h"

WingBeater::WingBeater(DLLHandle **wings, unsigned int count
		, WngStartup *Startup
		, WngShutdown *Shutdown
)
		: _wings(wings), _wingCount(count), MxnSingleton<WingBeater>(this)
		, WLinksStartup(Startup)
		, WLinksShutdown(Shutdown)
{ }

WingBeater::~WingBeater()
{
	// Sometimes it will crash here, other times it will not, completely random

	for (unsigned int i = 0; i < _wingCount; i--)
		delete _wings[i];
	
	delete[] _wings;
}

void WingBeater::GatherWings()
{
	PRF_FN;
	std::vector<DLLHandle*> wings;

	std::string exe_dir;

#ifdef PLT_WINDOWS
	{
		char pBuf[256];
		size_t len = sizeof(pBuf); 
		int bytes = GetModuleFileName(NULL, pBuf, len);
		std::string path = std::string(pBuf, bytes ? bytes : -1);
		exe_dir = path.substr(0, path.find_last_of('\\'));
	}
#else
	#error Can only build to windows
#endif

	std::string wing_dir = exe_dir + "/Wings";

	if (!fs::exists(wing_dir))
	{
		new WingBeater(
			nullptr, 0
			, nullptr
			, nullptr
		);
		return;
	}

	for (auto p : fs::recursive_directory_iterator(wing_dir))
	{
		if (p.is_directory() || p.path().extension().string().compare(WING_EXTENSION) != 0)
			continue;

		std::string path = p.path().string();

		// When DLLHandle goes out of scope, it frees the library
		DLLHandle *h = new DLLHandle(path.c_str());

		if (h->Get())
			wings.push_back(h);
		else
			LG_ERR("File {0} is not a hook", path);
	}

	int size = wings.size();

	WngStartup *wLinksStartup = (WngStartup*)malloc(sizeof(WngStartup*) * size);
	for (unsigned int i = 0; i < size; i++)
	{
		WngStartup pointer
				= reinterpret_cast<WngStartup>(GetProcAddress(wings[i]->Get(), "Startup"));

		if (pointer)
		{
			wLinksStartup[i] = pointer;
		}
		else
		{
			LG_ERR("Loading Startup from {0} failed", wings[i]->GetPath());
			wLinksStartup[i] = [](){};
		}
	}

	WngShutdown *wLinksShutdown = (WngShutdown*)malloc(sizeof(WngShutdown*) * size);
	for (unsigned int i = 0; i < size; i++)
	{
		WngShutdown pointer
				= reinterpret_cast<WngShutdown>(GetProcAddress(wings[i]->Get(), "Shutdown"));

		if (pointer)
		{
			wLinksShutdown[i] = pointer;
		}
		else
		{
			LG_ERR("Loading Shutdown from {0} failed", wings[i]->GetPath());
			wLinksShutdown[i] = [](){};
		}
	}

	new WingBeater(
		&wings[0], size
		, wLinksStartup
		, wLinksShutdown
	);
}

// #############################
// Calls to wing functions
// #############################

void WingBeater::CallStartup()
{
	for (unsigned int i = 0; i < Get()->WingCount(); i++)
	{
		Get()->WLinksStartup[i]();
	}
}
void WingBeater::CallShutdown()
{
	for (unsigned int i = 0; i < Get()->WingCount(); i++)
	{
		Get()->WLinksShutdown[i]();
	}
}
#endif // not BLD_WING