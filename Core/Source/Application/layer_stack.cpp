#include "pch.h"
#include "layer_stack.h"

#include "debug"

LayerStack::LayerStack()
{ }

// LayerStack::~LayerStack()
// {
// 	for (MxnLayer* layer : _layers)
// 		delete layer;
// }

void LayerStack::PushLayer(MxnLayer *layer)
{
	_layers.emplace(_layers.begin() + _iOverlaysStart, layer);
	_iOverlaysStart++;
	layer->OnAttach();
}

void LayerStack::PushOverlay(MxnLayer *overlay)
{
	_layers.emplace_back(overlay);
	overlay->OnAttach();
}

void LayerStack::PopLayer(MxnLayer *layer)
{
	auto it = std::find(_layers.begin(), _layers.begin() + _iOverlaysStart, layer);
	if (it != _layers.end())
	{
		layer->OnDetach();
		_layers.erase(it);
		_iOverlaysStart--;
	}
}

void LayerStack::PopOverlay(MxnLayer *overlay)
{
	auto it = std::find(_layers.begin() + _iOverlaysStart + 1, _layers.end(), overlay);
	if (it != _layers.end())
	{
		overlay->OnDetach();
		_layers.erase(it);
	}
}
