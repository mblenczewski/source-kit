#pragma once

#include "core"
#include "events"

class MxnLayer
{
protected:
#ifdef BLD_DEBUG
	MxnLayer(const std::string& name = "Layer");
#else
	MxnLayer();
#endif

public:
	virtual ~MxnLayer();

	virtual void OnAttach() {}
	virtual void OnDetach() {}
	virtual void OnUpdate() {}
	virtual void OnImGUIRender() {}
	virtual void OnEvent(MxnEvent& event) {}

#if BLD_DEBUG
	inline const std::string& GetName() const { return _debugName; }
protected:
	std::string _debugName;
#endif
};

