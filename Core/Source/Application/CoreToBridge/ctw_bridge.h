#pragma once

#include "core"
#include "Util/singleton.h"

#include <memory_mapped_file.hpp>
namespace mmf = memory_mapped_file;

#define MEM_FILE_PATH (fs::temp_directory_path() / "hello")

class CTWBridge : public MxnSingleton<CTWBridge>
{
public:
	~CTWBridge();
	
	static void Init();
	static void Close();

#ifdef BLD_WING
	static void* LoadPointer();
#else
	static void SavePointer(void* pointer);
#endif

private:
	CTWBridge();

#ifdef BLD_WING
	#define MMF mmf::read_only_mmf
#else
	#define MMF mmf::writable_mmf
#endif
	MMF _memFile;
	unsigned int _offsetIndex;
};

// void AppSingletons();
