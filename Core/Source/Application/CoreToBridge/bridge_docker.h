#pragma once

#include "app"
#include "input"
#include "debug"
#include "gameloop"
#include <imgui.h>

#include "ctw_bridge.h"

inline void CTWBridgeDock()
{
	PRF_FN;
	CTWBridge::Init();
	App::Init();
	MngInput::Init();
	Time::Init();

#ifdef BLD_WING
	ImGuiContext* ctx = static_cast<ImGuiContext*>(CTWBridge::LoadPointer());
	ImGui::SetCurrentContext(ctx);

	ImGuiMemAllocFunc alloc_func = (ImGuiMemAllocFunc)CTWBridge::LoadPointer();
	ImGuiMemFreeFunc free_func = (ImGuiMemFreeFunc)CTWBridge::LoadPointer();
	void* user_data = CTWBridge::LoadPointer();
	ImGui::SetAllocatorFunctions(alloc_func, free_func, user_data);
#else
	ImGuiContext* ctx = ImGui::GetCurrentContext();
	CTWBridge::SavePointer(ctx);

	// ImGuiMemAllocFunc* p_alloc_func = (ImGuiMemAllocFunc*)malloc(sizeof(ImGuiMemAllocFunc));
	// ImGuiMemFreeFunc* p_free_func = (ImGuiMemFreeFunc*)malloc(sizeof(ImGuiMemAllocFunc));
	// void** p_user_data = (void**)malloc(sizeof(ImGuiMemAllocFunc));
	ImGuiMemAllocFunc p_alloc_func = nullptr;
	ImGuiMemFreeFunc p_free_func = nullptr;
	void* p_user_data;
	ImGui::GetAllocatorFunctions(&p_alloc_func, &p_free_func, &p_user_data);
	CTWBridge::SavePointer((void*)p_alloc_func);
	CTWBridge::SavePointer((void*)p_free_func);
	CTWBridge::SavePointer(p_user_data);
#endif

	CTWBridge::Close();
}
