#include "pch.h"
#include "ctw_bridge.h"

#include "debug"

#define PTR_SIZE sizeof(void*) //+ sizeof(unsigned int)

union pointerSerialiser
{
	char bytes[sizeof(uintptr_t)];
	uintptr_t pointer;
};

CTWBridge::CTWBridge() :
		_memFile(MEM_FILE_PATH.string().c_str()
#ifndef BLD_WING
		, mmf::if_exists_truncate, mmf::if_doesnt_exist_create
#endif
		), _offsetIndex(0), MxnSingleton<CTWBridge>(this)
{
#ifdef BLD_WING
	for (unsigned int i = 0; i < _memFile.file_size() - 1; i += PTR_SIZE)
	{
		_memFile.map(i, PTR_SIZE);
		pointerSerialiser serialiser;
		// LG_TRC("{} {}", Get()->_memFile.offset(), Get()->_memFile.mapped_size());
		strcpy(serialiser.bytes, _memFile.data());
		// LG_TRC("{} {}", serialiser.pointer, serialiser.bytes);
		// LG_TRC("{} {} {}", /*serialiser.id,*/ serialiser.pointer, serialiser.bytes, _memFile.data());
	}
#endif
}

CTWBridge::~CTWBridge()
{
	_memFile.close();
	fs::remove(MEM_FILE_PATH);
}

void CTWBridge::Init() { new CTWBridge; }

void CTWBridge::Close() { Get()->_memFile.close(); }

#ifdef BLD_WING
void* CTWBridge::LoadPointer()
{
	Get()->_memFile.map(Get()->_offsetIndex, sizeof(uintptr_t));

	pointerSerialiser serialiser;
	strcpy(serialiser.bytes, Get()->_memFile.data());
	// LG_TRC("Load {} {}", serialiser.pointer, serialiser.bytes);

	Get()->_offsetIndex += PTR_SIZE;

	return reinterpret_cast<void*>(serialiser.pointer);
}
#else
void CTWBridge::SavePointer(void* pointer)
{
	Get()->_memFile.map(Get()->_offsetIndex, sizeof(uintptr_t));

	pointerSerialiser serialiser;
	serialiser.pointer = reinterpret_cast<uintptr_t>(pointer);
	// LG_TRC("Save {} {}", serialiser.pointer, serialiser.bytes);
	strcpy(Get()->_memFile.data(), serialiser.bytes);

	Get()->_offsetIndex += PTR_SIZE;
}
#endif
