#pragma once

#include "core"
#include "math"
#include "events"

struct WindowProps
{
	std::string title;
	unsigned int width, height;

	WindowProps(
		const std::string& title = PRJ_NAME,
		unsigned int width = 1280,
		unsigned int height = 720) 
		: title(title), width(width), height(height)
	{}
};

// A mixin representing a desktop system based window
class MxnWindow
{
public:
	using FnEventCallback = std::function<void(MxnEvent&)>;

	virtual ~MxnWindow() {}

	virtual void OnUpdate() = 0;

	// To be replaced with a vector2 int
	virtual unsigned int GetWidth() const = 0;
	virtual unsigned int GetHeight() const = 0;

	virtual void SetEventCallback(const FnEventCallback& callback) = 0;
	virtual void SetVSync(bool enabled) = 0;
	virtual bool IsVSync() const = 0;

	virtual void* GetNativeWindow() const = 0;

	static MxnWindow* Create(const WindowProps& props = WindowProps());

protected:
	MxnWindow() {}
};
