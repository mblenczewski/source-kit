#pragma once

#include "core"
#include "layer.h"

// Layers exist throughout the lifetime of the application
// Layers are owned by the layer stack, but if it gets removed from the layerstack
// the layer won't be destroyed when the layerstack is destroyed

// Overlays are layers that we want to ensure are rendered over every layer
// e.g. UI, debug UI
// _layerInsert is used to keep track of the top of the regular layers
// to ensure they are not placed above the overlays

class LayerStack
{
public:
	LayerStack();
	~LayerStack() {}

	void PushLayer(MxnLayer *layer);
	void PushOverlay(MxnLayer *overlay);
	
	void PopLayer(MxnLayer *layer);
	void PopOverlay(MxnLayer *layer);

	std::vector<MxnLayer*>::iterator begin() { return _layers.begin(); }
	std::vector<MxnLayer*>::iterator end() { return _layers.end(); }

private:
	std::vector<MxnLayer*> _layers;
	// unsigned int _layerInsertIndex = 0;
	unsigned int _iOverlaysStart = 0;
};
