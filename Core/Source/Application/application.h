#pragma once

#include "core"
#include "layers"
#include "ctw"
#include "debug"
#include "events"
#include "Util/singleton"

#include "Debug/imgui_layer.h"
#include "window.h"

class App : public MxnSingleton<App>
{
public:
	~App() { PRF_FN; delete _ImGUILayer; }

	inline static bool IsRunning() { return Get()->_running; }
	inline static void SetRunning(bool value) { Get()->_running = value; }

	inline static bool IsMinimised() { return Get()->_minimised; }
	inline static void SetMinimised(bool value) { Get()->_minimised = value; }

	inline static LayerStack& GetLayerStack() { return Get()->_layerStack; }

	inline static MxnWindow& GetWindow() { return *Get()->_window; }
	// inline static void SetWindow(MxnWindow* window) { Get()->_window = window; }

	inline static ImGUILayer& GetImGUILayer() { return *Get()->_ImGUILayer; }

	SiINIT(App)

private:
	App(IF_WING(App *ref))
		: _running(false), _layerStack(), _window(nullptr), _ImGUILayer(new ImGUILayer), MxnSingleton<App>(IF_WING_ELSE(ref, this))
	{
#ifndef BLD_WING
		PRF_FN;
		_window <= MxnWindow::Create();
		_layerStack.PushOverlay(_ImGUILayer);
#endif
	}
	
	bool _running;
	bool _minimised;
	LayerStack _layerStack;

	own<MxnWindow> _window;
	ImGUILayer *_ImGUILayer;
};
